# kwaeri platform

[![pipeline status](https://gitlab.com/mmod/kwaeri-platform/badges/master/pipeline.svg)](https://gitlab.com/mmod/kwaeri-platform/commits/master)  [![coverage report](https://gitlab.com/mmod/kwaeri-platform/badges/master/coverage.svg)](https://mmod.gitlab.io/kwaeri-platform/coverage/)  [![CLI Best Practices](https://bestpractices.coreinfrastructure.org/projects/1837/badge)](https://bestpractices.coreinfrastructure.org/projects/1837)

> WARNING! kwaeri platform is not ready for production yet. We have decided to publish early for testing purposes during development. You are free to use it, but please note that this project is by no means complete, nor safe, and we **DO NOT** recommend using kwaeri platform in production at this time. With that said, please feel free to check the platform out and see where it's headed!

The short version:

kwaeri platform is an 'XRM', or (X)Cross-Relational Management System; A solution for Node.js which provides a platform for development. It is a collection of components which deliver a powerful, full-featured, and extensible framework. Pre-bundled with a CMS extension that beautifully demonstrates extending the platform, users can create any application they imagine by leveraging existing extensions or creating their own.

> Please note that this project is a decendant of [nk-xrm](https://gitlab.com/mmod/nk-xrm). nk-xrm made use of [nk](https://gitlab.com/mmod/nk) and [nk-mysql](https://gitlab.com/mmod/nk-mysql) to implement an application platform of a similar caliber and with a similar purpose. Years later, we're finally sharing the results of the continued development made upon the platform for our own purposes - which has been private all this time - in a brand-new, wholly redesigned project. As a result, this project will start with the original design utlizing the original - though massively modified and full-featured - framework, and will transform into a new platform that utilizes [@kwaeri/node-kit](https://gitlab.com/mmod/kwaeri-node-kit) (nk redesigned and restructured under the [@kwaeri](https://www.npmjs.com/org/kwaeri) scope).

If you like our software, please consider making a donation. Donations help greatly to maintain the Massively Modified network and continued support of our open source offerings:

[![Donate via PayPal.com](https://gitlab.com/mmod/kwaeri-user-experience/raw/master/images/mmod-donate-btn-2.png)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=YUW4CWCAABCU2)

## Table of Contents

* [The Implementation (about kwaeri platform)](#the-implementation)
  * [Server-side](#server-side)
  * [Client-side](#client-side)
* [Developer Hype](#developer-hype)
* [It's a Template](#template-application)
* [Using kux](#using-kux)
  * [Installation](#installation)
  * [Configuration](#configuration)
  * [Getting Started](#getting-started)
* [Extending kwaeri platform](#extending-kwaeri-platform)
* [How to Contribute Code](#how-to-contribute-code)
* [Other Ways to Contribute](#other-ways-to-contribute)
  * [Bug Reports](#bug-reports)
  * [Vulnerability Reports](#vulnerability-reports)
    * [Confidential Issues](#confidential-issues)
  * [Donations](#donations)

## The Implementation

The kwaeri platform has two primary facets to it:

* Server-side
* Client-side

### Server-side

The server-side of kwaeri platform introduces a highly organized structure for designing application logic. It makes use of @kwaeri/node-kit to provide a powerful MVC framework. Read more about [@kwaeri/node-kit](https://gitlab.com/mmod/kwaeri-node-kit).

The server-side application structure is divided into two main components:

* The front-facing application
* The adminstrative application

#### The Front-Facing Application

The front-facing application is what end-users of your application see. By making use of layouts and incorporating modules to present data based upon the view and its related administrative features, it renders a user interface powered by @kwaeri/user-experience that is bound to leave a lasting impression on your visitors. Read more on [@kwaeri/user-experience](https://gitlab.com/mmod/kwaeri-user-experience).

Features - or extensions - managed by the administrative application may present data by providing any manner of module-types, and while a unique experience - the concepts driving the experience should feel familiar to anyone who has worked with theme-systems, layouts that make use of modules, and module-positions in technologies such as Wordpress and Joomla!.

#### The Aministrative Application

The administrative application is what application administrators see. It presents a task-oriented, dashboard-style interface for providing application management. From users and themes, to extension and module management - it is the one-stop-shop for growing and managing the building-blocks of your application. It too is customizable, making use of layouts - and modules (?) - to implement an intuitive, yet comfortably stunning user experience.

Built around the concepts of extensibility and modularity, the administrative application is a powerful tool - yet one with a standard, and which will feel familiar to those who have worked with other platforms providing similar features. The entire application is designed to be intuitive and easy to transition to. It plays much like lego blocks,and is just as fun. The pre-bundled CMS, for instance, is not statically bound to the platform - it is an extension, and may be removed, modified, and/or replaced altogether!

### Client-side

The client side of the application is built with [@kwaeri/user-experience](https://gitlab.com/mmod/kwaeri-user-experience). The @kwaeri/user-experience library implements - in compliment to appropriate markup in the server-side views - Google's Material Design Components for Web to deliver a feature-rich user experience that will leave a lasting impression on visitors to your application.

When users visit your application the client application is sent to their browser. Users' browsers will almost never redirect or change URLs synchronously. A kwaeri platform application is designed to entirely make use of asynchronous technologies and provide a modern, innovative user-experience through and through. @kwaeri/user-experience delivers just that kind of functionality, and makes it extremely easy to do so.

## Developer Hype

Possessing the flexibility of a full-featured MVC application framework, the speed and efficiency known only of Node.js, and empowered with Google's Material Design; @kwaeri/platform is a true example of modern innovation and represents the cutting edge of web application development. Say goodbye to bootstrap - and say hello to jaw-dropping rapid application development!

## It's a Template

kwaeri platform is a template application, or sample project, which is intended to be used as the base to any application. By downloading kwaeri platform, you are provided with the perfect starting point for your next kwaeri-powered application.

## Using kwaeri platform

After installing kwaeri-platform, you can modify the theme, layouts, and ultimately utilize it as-is for a powerful blogging application - a full-blown CMS built entirely on Node.js!

However, you can also remove the CMS extension, install and/or develop additional extensions, and build any manner of application, from a super-efficient API to a full blown SaaS application.

Here's how to get started:

### Installation

Installation can seem daunting, but its honestly very simple and doesn't take long to complete.

When kwaeri platform's ancestor, nk-xrm, was developed - the available node modules available for working with MySQL were slim pickings - so we wrote our very own native MySQL add-on in C++. The result was a very fast and powerful MySQL module with a comfortable and easy-to-use JavaScript interface. To this day, we still depend on nk-mysql, though you are free to remove it - the choice is entirely yours.

With that said, the most involved portion of the installation is with installing nk-mysql and seeding the database. However, by following the instructions provided, you'll be up and running in no time whether you're a windows or linux user (It can work for Macintosh, though we would love help in supporting Mac OS and with providing related documenation. If you would like to contribute - please check out the [How to Contribute Code](#how-to-contribute-code) and [Other Ways to Contribute](#other-ways-to-contribute) sections below.)

**NOTE**

Eventually, a migration system will make data management exteremely easy and seamless, but for now - at least in the platforms early stages - everything is manual. As with a Wordpress or Joomla! installation, there will eventually be a starter page that will allow you to seed demo data, perform a migration of a historical application, or to start fresh. The progress on this functionality can be tracked through [this issue](#).

#### Installing kwaeri platform via Git

There is an installer on its way, but for now the easiest way to get kwaeri-platform is via Git:

```bash
git clone https://gitlab.com/mmod/kwaeri-platform
```

Alternatively, you could just download a [tarball](https://gitlab.com/mmod/kwaeri-platform/-/archive/master/kwaeri-platform.tar.gz) and extract it to where you wish to keep the application.

#### Finishing the Installation

After downloading kwaeri platform, you must install its dependencies. Before that, however, you need to make sure you have some prerequisites in place for the data-tools to install correctly. Whether you wish to build or not build the data-tools, the [MMOD Wiki @ Gitlab](http://gitlab.com/mmod/documentation/wikis/home) will guide you through ensuring you meet any and all requirements. You can continue below for a brief coverage of the dependencies, or feel free to browse the wiki documentation and return here once you've finished.

As mentioned, next up is installing the dependencies for kwaeri-platform.  Assuming you have all the prerequisites met:

```bash
npm install .
```

All that's left now is to configure the application and seed the database.

### Configuration

#### Seed the Database

**NOTE**

For now, there are two sql files within the `install` directory which contain backup data from a development and production database. Until the user management is fully implemented and/or the kpm module is shipped with the platform, you'll need to go into these sql files and update the  `users`, `context` and `elevated` tables where you see the user `rik`. What you'll need to do is to update the information to that which you'll want for yourself, and replace the password hash with a hash for your own password.

However, the means to generate this hash is not yet implemented, and so if not already done - a default admin user will be added to the database for allowing anyone to download and at least try out the current functionality of the platform (I'll update this readme once it is done). Once this is completed, all that will be required is to use mysqldump to restore the two databases and update the platform's configuration to use the newly restored databases.

Before long, user management will be fully implemented (I have been focusing on the extension, theme, and module systems), and to supplement everything a new module - kwaeri platform manager (kpm) - will be added for adding a default admin, seeding the database, and performing migrations.

#### Configure the Application

To finish configuring your application, open the `config.js` file in the root of the kwaeri platform application.  In this file, notice the **url** and **server** members of both the **development** *and* **production** configuration schemas; You probably need to update at least one of them, as well as any mail config (though we can't use it yet, it's there to remind us to build that in!).  If you're developing locally and not using a host entry (meaning you're typing localhost into your browser), then the URL *should* be `http://localhost:XXXX` 'lest you want problems.

The only other changes you may need to make, are in the database sections of the configuration file.

You're all configured now, let's move on.

### Getting Started

To start the kwaeri platform application:

#### On Windows

```node
set NODE_ENV=development
node index.js
```

or if you are starting the admin application:

```node
set NODE_ENV=development
node index.js admin
```

#### On Linux/Unix

```node
NODE_ENV=development node index.js
```

or if you are starting the admin application:

```node
NODE_ENV=development node index.js admin
```

Some console output should have alerted you that the application(s) is/are running.  Try visiting `http://localhost:XXXX`; if you did not make any changes to the configuration the port would be **7719** for the *front* application, and **7717** for the *admin* application...otherwise we're guessing you know what you're doing :)

#### kwaeri-platform & Forever

Once you get use to the @kwaeri/patform application and inevitably fall in love with it, you may decide that you want to use it for just about everything.  When that time comes, you may certainly [@kwaeri/node-kit]() and the [nk-mysql]() globally, and even use supporting applications such as Forever to keep it running indefinitely. We use forever in-house to run our applications. Here's how to go about setting that up!

**NOTE**

You should definitely never have to use sudo to install global npm modules, if you find yourself having to do so, please follow our guide on how to [Properly Configure Global Modules]() with npm.

```node
npm install -g @kwaeri/node-kit nk-mysql forever
```

Once they've installed, you can remove/uninstall them from the node modules directory under your kwaeri-platform application. Here's how you'll start the application going forward:

```node
NODE_PATH=~/.npm-packages/lib/node_modules NODE_ENV=development forever start index.js
NODE_PATH=~/.npm-packages/lib/node_modules NODE_ENV=development forever start index.js admin
```

And now you're up and running system-wide.  To get a list of running applications, run:

```node
forever list
```

If you make some changes to your applications which go beyond just modifying kml or asset files in the view/public directories, you'll need to restart your application for the changes to propogate:

```node
forever restartall
```

By utilizing the multiple tabs in your terminal (linux love), you can easily monitor the logs for your applications too:

```node
tail -f /path/to/logfile/from/forever/list/command/fcEdg.log
```

The `-f` can be replaced with the number of lines to display if you don't want to follow the log live.


## Extending kwaeri platform

There will be caveats to developing over kwaeri-platform. The biggest will be understanding how the application works, so that with such understanding one will be able to make it function as they would like.

What good would a *platform* be to a developer if it wasn't extensible? While kwaeri platform brings to Node.js a truly innovative experience, it also delivers the expected: A platform with a programmable interface. Every facet of the application is provided for you, but is absolutely customizable. Use the built in facilities, modify them how you would like, or replace them altogether. The choice is yours!

### Digging Deeper

To get a feel for the platform, for the kwaeri framework and provided tools - dig through the `adm` and `app` folders.  Take note of how the controllers, models, and views are structured and where they are located. This will help you explore and learn about developing with kwaeri platform.

- An example of a website based interface is viewable from the root URL of the xrm application when it is running.
- An example of a web application 'dash'  is available by visiting `<base_url>/dash`.

***NOTE:*** *The extension system is NOT fully implemented. What is present is only a conceptual skeleton for implementation. What's missing is the system for installing an extension from a package and a management interface. In order to implement your own extensions, you'll need to build the extension into the application structure, and manually apply any initial database transactions such as table creations and seeding of data. The MMOD-CMS extension is the perfect template for this, and in a subsequent release we will release the required installation and management interfaces with appropriate documentation.*

### Important considerations

The advanced cryptography methods were stripped from this source, so you will want to modify `chosen_application_root/app/models/account` and `chosen_application_root/adm/models/account` and update the *login* members so that they implement a more secure authentication system.

**This is a new repository and we have a lot going on. Please bear with us as we update sources and documentation.**

### The Gist of the Framework

Like any MVC Framework, it's up to the developers to supply the Controllers, Models, and Views which make up the application.

#### Controllers

Controllers are where we set/get session variables, they're where we invoke models to subsequently handle data queries, and they are where we place most application or business logic specific to our application.

Properties of a controller object are considered the different *actions* the controller provides to the application; simply adding a new action to an existing controller will yield a new usable URL:  `<base_url>/<controller>/<action>`.  Adding ***Post*** to the end of a controller action name will let the framework know that it should be called when a POST is made upon that URL.

Take a look at `chosen_application_root/app/controllers/account.js` to see some examples.


***NOTE:*** *Currently there are no examples for API transactions, however -> responding to API requests will take place directly in the controller.  Example coming soon.*

#### Models

Models are where we define data queries, and data properties.

Members of a model file's object are considered *models*; You could essentially have numerous related models within a single model file. From within a model, we are able to access the database object, and are typically fed a callback to make data processing implicitly asynchronous.

Take a look at `chosen_application_root/app/models/account.js` to see an example.

#### Views

Views are where we define what the end user is going to see when the response is set (and we are returning a content type of text/html).  This involves preparation of a layout as well as content for the layout.

Each directory under `chosen_application_root/app/views` (aside from *shared*), denotes a set of views for each controller's various actions.  The *shared* directory under `chosen_application_root/app/views` allows developers to create *layouts* or *templates* which are available to the entire application.

kwaeri platform uses the nk tool kit, which has an html templating engine baked into it known as *pottr*, pronounced 'potter'. The markup is very much like JavaScript, and makes use of double square brackets `[[` and `]]` to indicate to the parser that a block of *kwaeri markup language* (or 'kml') is in need of evaluation. The views make use of `*.kml` files to provide kml-enhanced HTML for the application.

Take a look at the various files within the `chosen_application_root/app/views/` directory to see some examples of kwaeri markup.

**A little note**
*To change your favicon, just replace the favicon.ico that exists in the `path_to_application/assets/` and `path_to_application/adm/assets` directories.  Icons cache in Windows, so after dropping your new favicon you may notice that in file explorer the old icon is still displaying (you will need to restart your pc to correct this).  Rest assured that once you've deleted your browsers cache, in the browser the proper favicon will show (even before restarting).*

More documentation to come.

## How to Contribute Code

Our Open Source projects are always open to contribution. If you'd like to cocntribute, all we ask is that you follow the guidelines for contributions, which can be found at the [Massively Modified Wiki](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Contribute-Code)

There you'll find topics such as the guidelines for contributions; step-by-step walk-throughs for getting set up, [Coding Standards](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Coding-Standards), [CSS Naming Conventions](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/CSS-Naming-Conventions), and more.

## Other Ways to Contribute

There are other ways to contribute to the project other than with code. Consider [testing](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Test-Code) the software, or in case you've found an [Bug](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Bug-Reports) - please report it. You can also support the project monetarly through [donations](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Donations) via PayPal.

Regardless of how you'd like to contribute, you can also find in-depth information for how to do so at the [Massively Modified Wiki](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute)

### Bug Reports

To submit bug reports, request enhancements, and/or new features - please make use of the **issues** system baked-in to our source control project space at [Gitlab](https://gitlab.com/mmod/kwaeri-user-experience/issues)

You may optionally start an issue, track, and manage it via email by sending an email to our project's [support desk](mailto:incoming+mmod/kwaeri-user-experience@incoming.gitlab.com).

For more in-depth documentation on the process of submitting bug reports, please visit the [Massively Modified Wiki on Bug Reports](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Bug-Reports)

### Vulnerability Reports

Our Vulnerability Reporting process is very similar to Gitlab's. In fact, you could say its a *fork*.

To submit vulnerability reports, please email our [Security Group](mailto:security@mmod.co). We will try to acknowledge receipt of said vulnerability by the next business day, and to also provide regular updates about our progress. If you are curious about the status of your report feel free to email us again. If you wish to encrypt your disclosure email, like with gitlab - please email us to ask for our GPG Key.

Please refrain from requesting compensation for reporting vulnerabilities. We will publicly acknowledge your responsible disclosure, if you request us to do so. We will also try to make the confidential issue public after the vulnerability is announced.

You are not allowed, and will not be able, to search for vulnerabilities on Gitlab.com. As our software is open source, you may download a copy of the source and test against that.

#### Confidential Issues

When a vulnerability is discovered, we create a [confidential issue] to track it internally. Security patches will be pushed to private branches and eventually merged into a `security` branch. Security issues that are not vulnerabilites can be seen on our [public issue tracker](https://gitlab.com/mmod/kwaeri-user-experience/issues?label_name%5B%5D=Security).

For more in-depth information regarding vulnerability reports, confidentiality, and our practices; Please visit the [Massively Modified Wiki on Vulnerability](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Vulnerability-Reports)

### Donations

If you cannot contribute time or energy to neither the code base, documentation, nor community support; please consider making a monetary contribution which is extremely useful for maintaining the Massively Modified network and all the goodies offered free to the public.

[![Donate via PayPal.com](https://gitlab.com/mmod/kwaeri-user-experience/raw/master/images/mmod-donate-btn-2.png)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=YUW4CWCAABCU2)
