/**
 * package: mmod-xrm
 * sub-package: controllers/main
 * author: Richard B. Winters <a href="mailto:rik@mmogp.com">rik AT mmogp DOT com</a>
 * copyright: 2011-2015 Massively Modified, Inc.
 * license: See Terms and EULA at https://mmogp.com/about/terms
 */


/**
 * Constructor
 *
 * @since 0.0.1
 */
function mainController()
{
}


// HTTP GET /
mainController.prototype.index = function( request, response )
{
    // We need to collect all the data we need for rendering a layout response;
    // this includes a list of extensions, and any and all static content.
    //
    // The admin application will hardly ever have a non-ajax display, but if so
    // further nested data can be gathered via whatever controller is necessary.
    //
    // Regardless of anything, it is here we gather module data for the layout

    // Get a list of modules the current layout supports
    // Get a list of all extensions that will be producing modules content for this request
    // Pass the request to each modules rendr() method
    // proceed with the request, the nk framework will use the modules data properly
    var na = this;

    var xrmModel = require( '../models/main' ).xrm,
    model = this.modl.set( xrmModel );

    if( request.isAuthenticated )
    {
        this.klay.viewbag.token = response.session.id;
    }
    this.klay.viewbag.title = 'MMOD';
    this.klay.viewbag.pagetitle = 'Massively Modified, Massively Modern';
    this.klay.viewbag.testvar =
    [
        { 'a': '<b>A Powerful Combination</b>', 'b': '<p>This project comes prepared with the jQuery JavaScript Library, as well as the Bootstrap CSS - which includes their version of Normalize - and JavaScript Libraries. Their links:</p><p><a href="http://jquery.com/" target="_blank">The jQuery website.</a></p><p><a href="http://getbootstrap.com/" target="_blank">The GetBootstrap website.</a></p>.' },
        { 'a': '<b>MySQL</b>', 'b': '<p>This project harnesses the power of MySQL Community Server through nodamysql (nk-mysql on npm), which uses the MySQL Connector C++ to provide efficient and secure data integration. Browse the following links for specifics:</p><p><a href="http://www.mysql.com/about/legal/licensing/foss-exception/" target="_blank">Oracle\'s Free and Open Source (\"FOSS\") License</a>.</p><p><a href="http://github.com/mmod/nodamysql" target="_blank">The nodamysql GitHub.</a></p>' },
        { 'a': '<b>The</b> SJCL', 'b': '<p>This project makes use of the Stanford Javascript Crypto Library, and provides a great starting place for creating a secure and efficient user system. Here are some links to get you started:</p><p><a href="http://crypto.stanford.edu/sjcl/" target="_blank">The Stanford JavaScript Cryto Library website.<a></p><p><a href="https://jswebcrypto.azurewebsites.net/demo.html#/pbkdf2" target="_blank">Getting started with JavaScript and Web Cryptography.</a></p>', 'c': 'yea' }
    ];

    this.klay.model = xrmModel;

    // For testing
    //this.klay.modules.sysutil = true;
    //this.klay.modules.content = 1;
    //this.klay.modules.footer = 'Ofc';

    this.rendr( request, response );
    // Here we define the callback for our list method
    //var callback = function( req, res, tk )
    //{
    //    na.rendr( req, res );
    //};

    //model.turn( request, response, callback, this.klay );
};


// Export
module.exports = exports = mainController;