/**
 * package: mmod-xrm
 * sub-package: controllers/explore
 * author: Richard B. Winters <a href="mailto:rik@mmogp.com">rik AT mmogp DOT com</a>
 * copyright: 2011-2015 Massively Modified, Inc.
 * license: See Terms and EULA at https://mmogp.com/about/terms
 */


/**
 * Constructor
 *
 * @since 0.0.1
 */
function exploreController()
{
}


// HTTP GET /explore
exploreController.prototype.index = function( request, response )
{
    // We just need to display a welcome here for now
    this.klay.viewbag.title = 'MMod';
    this.klay.viewbag.pagetitle = 'MMod XRM DEMO | Home';
    this.klay.viewbag.testvar =
    [
        { 'a': '<b>Kwaeri</b>', 'b': '<p>Our work in progress; a state-of-the-art platform featuring a multi-threaded non-blocking/blocking I/O.  Coming soon.</p><p><a href="#" class="btn btn-lg btn-primary" role="button">Learn more</a></p>.', 'c': 'oh' },
        { 'a': '<b>nodakwaeri</b>', 'b': '<p>A concept, and small taste of the application framework experience baked into the up-and-coming Kwaeri platform; for Node.js.</p><p><a href="#" class="btn btn-lg btn-primary" role="button">Check it out</a></p>', 'c': 'ho' },
        { 'a': '<b>nodamysql</b>', 'b': '<p>A concept which will help us shape the future of our data integration tools for the kwaeri platform.</p><p><a href="#" class="btn btn-lg btn-primary" role="button">Learn more</a></p><p><a href="#" class="btn btn-default" role="button">On the Node</a></p>', 'c': 'yea' },
        { 'a': '<b>nk-mvc</b>', 'b': 'The project template for Node.js which makes use of nodakwaeri and nodamysql (nk and nk-mysql on npmjs.org, respectively). A conceptual preview of the experience for - and which will help shape - the Kwaeri platform.<p>.', 'c': 'nice' }
    ];

    this.rendr( request, response );
};




// HTTP GET /explore/dash
exploreController.prototype.dash = function( request, response )
{
    // We just need to display a welcome here for now
    this.klay.layout = 'dash';
    this.klay.viewbag.title = 'MMod';
    this.klay.viewbag.pagetitle = 'MMod XRM DEMO | Dashboard';

    this.rendr( request, response );
};


// Export
module.exports = exports = exploreController;