/**
 * package: mmod-xrm
 * sub-package: controllers/account
 * author: Richard B. Winters <a href="mailto:rik@mmogp.com">rik AT mmogp DOT com</a>
 * copyright: 2011-2015 Massively Modified, Inc.
 * license: See Terms and EULA at https://mmogp.com/about/terms
 */


/**
 * Constructor
 *
 * @since 0.0.1
 */
function accountController()
{
}


// HTTP GET /account
accountController.prototype.index = function( request, response )
{
    // We just need to display a message here
    this.klay.viewbag.title = 'MMod XRM: Account';
    this.klay.viewbag.pagetitle = 'Nothin to see here...';

    this.rendr( request, response );
};


// HTTP GET /account/login
accountController.prototype.login = function( request, response )
{
    // We just need to display fields for a login here
    this.klay.layout = 'login';
    this.klay.model = require( '../models/account' ).loginView;
    this.klay.viewbag.title = 'kwaeri: Login';
    this.klay.viewbag.pagetitle = 'kwaeri';

    this.rendr( request, response );
};


// HTTP POST /account/login
accountController.prototype.loginPost = function( request, response )
{
    var na = this;

    // Here we're going to want to use our database provider, when we do so
    // we'll define a callback to send along with our query
    // to support an implicit asynchronicity.
    var viewModel = require( '../models/account' ).loginView,
    loginModel = require( '../models/account' ).login,
    model = this.modl.set( loginModel );

    this.klay.model = require( '../models/account' ).loginView;
    this.klay.viewbag.title = 'MMod XRM: Account';
    this.klay.viewbag.pagetitle = 'You tried to log in.';

    // Here we define the callback for our authentication method
    var callback = function( req, res, authenticated, tk )
    {
        if( !authenticated )
        {
            console.log( 'We were unable to authenticate the user.' );
            res.session.set
            (
                'user',
                { username: 'Guest', email: 'you@example.com', name: { first: 'Guest' } },
                { secure: true }
            );

            req.session.setUserAuth( req.session.client.id, true );

            na.rendr( req, res );
        }
        else
        {
            //console.log( 'We were able to authenticate the user.' );
            // Using POST variables is quite easy as well:
            if( req.posted.rememberme )
            {
                res.session.set( 'persistence', true, { secure: true } );
            }

            // Store user information for later use
            var userdata =
            {
                id: authenticated[0].id,
                username: authenticated[0].username,
                email: authenticated[0].email,
                name: { first: authenticated[0].first, last: authenticated[0].last },
                company: { id: authenticated[0].company, name: authenticated[0].companyname }
            };

            res.session.set
            (
                'user',
                userdata,
                { secure: true }
            );

            req.session.setUserAuth( req.session.client.id, userdata );

            // Let's redirect, but remember to set the session before we do.
            res.redirect( '/' );
        }
    };

    // And here we invoke the model's authenticate method. I'm sure you can see
    // the changes you would need to make in this controller method to make things
    // synchronous instead (i.e. remove code body from callback, have it run after
    // model executes, but have model return its value to a variable within this method's
    // scope like var authenticated = mode.authenticate... callback can be left undefined.)
    model.authenticate( request, response, callback, this.klay );
};


// HTTP GET /account/manage
accountController.prototype.manage = function( request, response )
{
    // Display fields to manage account information
    this.klay.model = require( '../models/account' ).manageView;
    this.klay.viewbag.title = 'MMod XRM: Manage account';
    this.klay.viewbag.pagetitle = 'Please manage yourself.';
    this.klay.viewbag.usertypes =
    {
            'a': 'Guest',
            'b': 'Registered',
            'c': 'Moderator',
            'd': 'Administrator'
    };

    this.rendr( request, response );
};


// HTTP POST /account/manage
accountController.prototype.managePost = function( request, response )
{
    // We just need to display a message here
    this.klay.viewbag.title = 'MMod XRM: Manage account';
    this.klay.viewbag.pagetitle = 'You have tried to manage yourself.';

    this.rendr( request, response );

    var layout = this.config.view_provider;
};


// Export
module.exports = exports = accountController;