/**
 * package: mmod-xrm
 * sub-package: controllers/extensions
 * author: Richard B. Winters <a href="mailto:rik@mmogp.com">rik AT mmogp DOT com</a>
 * copyright: 2011-2015 Massively Modified, Inc.
 * license: See Terms and EULA at https://mmogp.com/about/terms
 */


/**
 * Constructor
 *
 * @since 0.0.1
 */
function extensionsController()
{
}


// HTTP GET /extensions/index
extensionsController.prototype.index = function( request, response )
{
    var reqmeth;
    if( request.method === 'POST' )
    {
        reqmeth = 'Post';
    }
    else
    {
        reqmeth = '';
    }

    // Get the path
    var path = request.requrl.pathname;

    // Remove leading slash if present, we check for one using the original path in the switch below to handle default url processing by the controller
    var tpath = path;
    if( tpath.charAt( 0 ) === '/' )
    {
        tpath = tpath.substr( 1 );
    }

    // And get the path parts
    var parts = tpath.split( '/' );
    var secondpart = "";

    // Check that we got parts
    if( parts.length > 0 )
    {
        // Great we had parts, now lets check whether there was 3 of them
        if( parts.length < 2 )
        {
            // This tells us an extension was not provided and so we can't possibly load it
            parts[1] = 'index';
        }
        else
        {
            secondpart = "/" + parts[1];
        }
    }
    else
    {
        parts[0] = tpath;
        parts[1] = 'index';
        // Impossible, something must have gone wrong.
    }

    // Here we attempt to load whatever extension is requested by name
    // if there is an error we display the 404 page.
    var isalias = false;
    var dparams = null;
    var eparts = null;

    try
    {
        var db = this.modl.dbo();
        db = db.reset();

        dparams = db
        .query( "select `mmod-cms-menu-items`.href as href, `mmod-cms-menu-items`.title as title, `mmod-cms-menu-items`.description as description, " +
                "`mmod-cms-menu-items`.menu as menu, `mmod-cms-menu-items`.parent as parent, `mmod-cms-menu-items`.options as options, " +
                "`mmod-cms-menu-item-types`.title as type, `extensions`.name as provider from `mmod-cms-menu-items` " +
                "join `mmod-cms-menu-item-types` on `mmod-cms-menu-items`.type=`mmod-cms-menu-item-types`.id " +
                "join `extensions` on `mmod-cms-menu-item-types`.parent=`extensions`.id " +
                "where href='" + parts[0] + secondpart + "' and `mmod-cms-menu-items`.enabled=2;" )
        .execute();

        if( dparams )
        {
            console.log( "DPARAMS: " );
            console.log( dparams );
            // Now we need to set the path to the type, which will be the extension path
            // and append it with the controller that handles
            eparts = dparams[0].provider.split( '-' );
            if( eparts.length > 0 )
            {
                if( eparts.length < 2 )
                {
                    eparts[1] = "";
                }
            }
            else
            {
                eparts = [ "", "" ];

                // Not a valid extension namespace
                console.log( "Error: not a valid extension namespace" );
            }

            etype = require( this.dextensions + '/' + eparts[0] + '/' + eparts[0] + '_' + eparts[1] + '/controllers/' + parts[0].toLowerCase() );
        }

        var dextension = new etype();

        dextension.isExtension = true;
        dextension.isModule = false;
        dextension.extData =
        {
            vendor: eparts[0],
            extension: eparts[0] + '_' + eparts[1],
            params: dparams
        }
        dextension.dextensions = this.dextensions;
        dextension.config = this.config;
        dextension.modl = this.modl;
        dextension.pottr = this.pottr;
        dextension.promoter = this.promoter;
        dextension.setUserAuth = this.setUserAuth;

        // Tac ?layout=0 on to the url in order to avoid wrapping the response with the layout/template
        if( Object.prototype.hasOwnProperty.call( request.requrl.query, 'layout' ) )
        {
            if( request.requrl.query.layout == 0 )
            {
                dextension.klay =
                {
                    controller: parts[0],
                    view: parts[1] || "",
                    layoutOverride: true,
                    viewbag: this.klay.viewbag
                };

                //console.log( 'VIEW: ' + dextension.klay.view );
            }
        }
        else
        {
            dextension.klay =
            {
                controller: parts[0],
                view: parts[1] || "",
                viewbag: this.klay.viewbag
            };
        }

        dextension.rendr = this.rendr;

        // The requested action determines the view, ensure the view action specified exists and that its a function, otherwise
        // we'll set Index as the action/view - and if that's not found then a great big 404 will display :)
        if( toString.call( dextension[parts[1] + reqmeth] ) !== '[object Function]' )
        {
            parts[1] = 'index';
        }

        //console.log( dextension );
        //console.log( 'Invoking method: ' + parts[4] + reqmeth + ': ' + parts[0] + ', ' + parts[1] + ', ' + parts[2] + ', ' + parts[3] );
        // Require the controller, and use the action term within the parts array to invoke the proper controller method
        dextension[parts[1] + reqmeth]( request, response );
        //console.log( 'done invoking method: ' + parts[4] + reqmeth );
    }
    catch( error )
    {
        // And log the error ofc
        console.error( 'Extension error: ' + error + ' Derived path: ' + path + 'line: ', /\(file:[\w\d/.-]+:([\d]+)/.exec( error.stack ) );

        // If the controller can't be loaded for some reason, handle the exception by showing a 404
        require( './../../../node_modules/nk/library/controller/404' ).get( request, response );
    }
};


// Export
module.exports = exports = extensionsController;