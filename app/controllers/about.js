/**
 * package: mmod-xrm
 * sub-package: controllers/about
 * author: Richard B. Winters <a href="mailto:rik@mmogp.com">rik AT mmogp DOT com</a>
 * copyright: 2011-2015 Massively Modified, Inc.
 * license: See Terms and EULA at https://mmogp.com/about/terms
 */


/**
 * Constructor
 *
 * @since 0.0.1
 */
function aboutController()
{
}


// HTTP GET /about
aboutController.prototype.index = function( request, response )
{
    // We just need to display a welcome here for now
    this.klay.viewbag.title = 'MMod';
    this.klay.viewbag.pagetitle = 'What About Us?';

    this.rendr( request, response );
};


// HTTP GET /explore/privacy
aboutController.prototype.terms = function( request, response )
{
  // We just need to display a welcome here for now
  this.klay.viewbag.title = 'MMod';
  this.klay.viewbag.pagetitle = 'What About Terms?';

  this.rendr( request, response );
};


// HTTP GET /explore/privacy
aboutController.prototype.privacy = function( request, response )
{
    // We just need to display a welcome here for now
    this.klay.viewbag.title = 'MMod';
    this.klay.viewbag.pagetitle = 'What About Privacy?';

    this.rendr( request, response );
};


// Export
module.exports = exports = aboutController;