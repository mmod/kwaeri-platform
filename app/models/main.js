/**
 * package: mmod-xrm
 * sub-package: models/main
 * author: Richard B. Winters <a href="mailto:rik@mmogp.com">rik AT mmogp DOT com</a>
 * copyright: 2011-2015 Massively Modified, Inc.
 * license: See Terms and EULA at https://mmogp.com/about/terms
 */


// Deps
var nk = require( 'nk' ),
nk = new nk();  // When we do not pass an argument to the constructor, we
                // get only the core facilities ( .type, .extend, .each,
                // .hash, etc )


var mainModel =
{
    xrm:
    {
        turn: function( request, response, callback, klay )
        {
            // It is here for the front end where we will query the content
            // from all modules that are listed as to be rendered for the
            // requested facility.

            // If layout is requested, we'll include all the modules

            // If no layout requested, its AJAX, and we won't query for
            // any module content, leave that up to the facility in question



            // And invoke the callback, passing the result of our query
            if( nk.type( callback ) === 'function' )
            { // Asynchronous
                callback( request, response, klay );
            }
            else
            { // Synchronous
                return true;
            }
        }
    }
};


//Export
module.exports = mainModel;