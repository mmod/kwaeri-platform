/*-----------------------------------------------------------------------------
 * @package:    Kwaeri platform
 * @author:     Richard B Winters
 * @copyright:  2015-2018 Massively Modified, Inc.
 * @license:    Apache-2.0
 * @version:    0.1.0
 *---------------------------------------------------------------------------*/


// INCLUDES
import merge from 'webpack-merge';
import baseConfig from './webpack.config';
import uglifyJsPlugin from 'uglifyjs-webpack-plugin';


module.exports = merge
(
    baseConfig,
    {
        devtool: 'source-map',
        plugins:
        [
            new uglifyJsPlugin
            (
                {
                    sourceMap: true
                }
            )
        ]
    }
);