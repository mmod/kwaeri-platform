/*-----------------------------------------------------------------------------
 * @package:    Kwaeri platform
 * @author:     Richard B Winters
 * @copyright:  2015-2018 Massively Modified, Inc.
 * @license:    Apache-2.0
 * @version:    0.1.0
 *---------------------------------------------------------------------------*/


module.exports =
{
    output:
    {
      filename: 'kwaeri-platform-bundle.min.js',
      sourceMapFilename: 'kwaeri-platform-bundle.min.js.map'
    },
    module:
    {
      rules:
      [
        {
          test: /\.(js|jsx)$/,
          exclude: /(node_modules)/,
          loader: 'babel-loader',
          query:
          {
            presets:
            [
              ['env'],
            ],
          },
        },
      ],
    }
};