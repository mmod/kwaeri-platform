CREATE SCHEMA IF NOT EXISTS `mmdev` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ;
CREATE USER 'mmdadm'@'localhost' IDENTIFIED BY '^DevPass777$';
CREATE USER 'mmdadm'@'%' IDENTIFIED BY '^DevPass777$';
GRANT ALL ON `mmdev`.* TO 'mmdadm'@'localhost';
GRANT ALL ON `mmdev`.* TO 'mmdadm'@'%';

CREATE TABLE IF NOT EXISTS `mmdev`.`users`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `type` INT NULL,
  `acl` INT NULL,
  `ts` DATETIME NULL,
  `luts` DATETIME NULL,
  `luby` INT NULL,
  `username` VARCHAR(255) NULL,
  `password` TEXT NULL,
  `first` VARCHAR(100) NULL,
  `middle` VARCHAR(45) NULL,
  `last` VARCHAR(100) NULL,
  `email` TEXT NULL,
  `context` INT NULL,
  PRIMARY KEY (`id`)
) ENGINE=NDBCLUSTER;

INSERT INTO `mmdev`.`users` 
( `type`, `acl`, `ts`, `luts`, `username`, `password`, `first`, `last`, `email`, `context` )
VALUES
( 1, 3, '2014-11-30 23:38:13', '2014-11-30 23:38:13', 'rik', '3f40e560164e9e602652e517117ac97421d81a9742318ba5c60c4698ae94ee49', 'Richard', 'Winters', 'rik@mmogp.com', 1 ),
( 1, 3, '2014-11-30 23:38:13', '2014-11-30 23:38:13', 'ravi', '42df385d8574ca064666c822918913e0e9f615965523fea1c93e33da276e69c3', 'Ravi', 'Soni', 'ravi@mmogp.com', 3 ),
( 1, 1, '2014-11-30 23:38:13', '2014-11-30 23:38:13', 'dmrls', '3f81820227da4c4047986679df146539e419d779835577b8db9a51877a122fdc', 'Dave', 'Morales', 'dave@mmogp.com', 4),
( 1, 1, '2014-11-30 23:38:13', '2014-11-30 23:38:13', 'ahren', '5b19781846be6a7adacec671c5eed36d4cb3c3902fe21a46a8b3748a34227ad4', 'Ahren', 'Edison', 'edisoncomputers@gmail.com', 5 ),
( 1, 1, '2014-11-30 23:38:13', '2014-11-30 23:38:13', 'dana', '502a1a79e74a2f368646d77edbb67aad5e43012db8e8eb10267151b00b6f9ac2', 'Jordana', 'Taynor', 'speedietaynor24@gmail.com', 6 ),
( 1, 1, '2014-11-30 23:38:13', '2014-11-30 23:38:13', 'shanna', '41ae0defc0c6ffe9ba252ed80a4c443434b74b9434838729fd9071aaf47c68cf', 'Shanna', 'Taynor', 'toonietaynor@gmail.com', 7 ),
( 1, 3, '2014-11-30 23:38:13', '2014-11-30 23:38:13', 'kan', 'a8e7b190bf2cad5a438f8da19f0093b1291dba59ee0bcecaf112aad00e7e3d4c', 'Kanhaiya', 'Jha', 'kan@mmogp.com', 8 );

CREATE TABLE IF NOT EXISTS `mmdev`.`acls`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NULL,
  `description` TEXT NULL,
  PRIMARY KEY (`id`)
) ENGINE=NDBCLUSTER;

INSERT INTO `mmdev`.`acls`
( `name`, `description` )
VALUES
( 'Guest', 'Default access level for non-registered users.' ),
( 'Registered', 'Access level for registered users.' ),
( 'Moderator', 'Access level for registered moderators.' ),
( 'Administrator', 'Access level for registered admin users.' );

CREATE TABLE IF NOT EXISTS `mmdev`.`elevated`
(
    `id` INT NOT NULL AUTO_INCREMENT,
    `user` INT NOT NULL,
    `secure` TEXT NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=NDBCLUSTER;

INSERT INTO `mmdev`.`elevated`
( `user`, `secure` )
VALUES
( 1, '3f40e560164e9e602652e517117ac97421d81a9742318ba5c60c4698ae94ee49' ),
( 2, '42df385d8574ca064666c822918913e0e9f615965523fea1c93e33da276e69c3' );

CREATE TABLE IF NOT EXISTS `mmdev`.`contexts`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `type` INT NULL,
  `acl` INT NULL,
  `ts` DATETIME NULL,
  `owner` int NULL,
  `company` int NULL,
  `location` int NULL,
  `luts` DATETIME NULL,
  `luby` INT NULL,
  `add1` TEXT NULL,
  `add2` TEXT NULL,
  `city` VARCHAR(255) NULL,
  `state` VARCHAR(255) NULL,
  `country` VARCHAR(255) NULL,
  `zip` VARCHAR(45) NULL,
  `email` TEXT NULL,
  `phone` VARCHAR(45) NULL,
  `fax` VARCHAR(45) NULL,
  `ext` VARCHAR(10),
  PRIMARY KEY (`id`)
) ENGINE=NDBCLUSTER;

INSERT INTO `mmdev`.`contexts` 
( `type`, `acl`, `ts`, `owner`, `luts`, `add1`, `add2`, `city`, `state`, `country`, `zip`, `email`, `phone` )
VALUES
( 0, 3, NOW(), 1, NOW(), '40 Morgan Avenue', 'Apt 1', 'Oneonta', 'NY', 'USA', '13820-1246', 'rik@mmogp.com', '607-267-1424' ),
( 0, 3, NOW(), 1, NOW(), '5006 NY-23', '', 'Oneonta', 'NY', 'USA', '13820-1246', 'rik@mmogp.com', '607-431-8726' ),
( 0, 3, NOW(), 2, NOW(), '', '', 'Jaipur', '', 'India', '', 'ravi@mmogp.com', '' ),
( 0, 1, NOW(), 3, NOW(), '', '', '', 'Ca', 'USA', '', 'dave@mmogp.com', '' ),
( 0, 1, NOW(), 4, NOW(), '', '', 'Oneonta', 'New York', 'USA', '13820', 'edisoncomputers@gmail.com', '607-437-4766' ),
( 0, 1, NOW(), 5, NOW(), '1672 State Highway 7', '', 'Unadilla', 'NY', 'USA', '13849', 'speedietaynor24@gmail.com', '' ),
( 0, 1, NOW(), 6, NOW(), '1672 State Highway 7', '', 'Unadilla', 'NY', 'USA', '13849', 'tooneytaynor@gmail.com', '' ),
( 0, 3, NOW(), 7, NOW(), 'Sushila Sadan, Jamtalla', 'Uttarmath, Hatiara', 'JKolkata', 'West Bengal', 'India', '700157', 'kan@mmogp.com', '' );

CREATE TABLE IF NOT EXISTS `mmdev`.`companies`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `type` INT NULL,
  `ts` DATETIME NULL,
  `owner` int NULL,
  `luts` DATETIME NULL,
  `luby` INT NULL,
  `name` TEXT NULL,
  `description` TEXT NULL,
  PRIMARY KEY (`id`)
) ENGINE=NDBCLUSTER;

INSERT INTO `mmdev`.`companies` 
( `ts`, `owner`, `luts`, `name`, `description` )
VALUES
( NOW(), 1, NOW(), 'Massively Modified, Inc.', 'Custom computer programming services. Game and Simulation specialization.' );

CREATE TABLE IF NOT EXISTS `mmdev`.`locations`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `type` INT NULL,
  `ts` DATETIME NULL,
  `company` int NULL,
  `luts` DATETIME NULL,
  `luby` INT NULL,
  `name` TEXT NULL,
  `description` TEXT NULL,
  `add1` TEXT NULL,
  `add2` TEXT NULL,
  `city` VARCHAR(255) NULL,
  `state` VARCHAR(255) NULL,
  `country` VARCHAR(255) NULL,
  `zip` VARCHAR(45) NULL,
  `email` TEXT NULL,
  `phone` VARCHAR(45) NULL,
  `ext` VARCHAR(10),
  PRIMARY KEY (`id`)
) ENGINE=NDBCLUSTER;

INSERT INTO `mmdev`.`locations` 
( `type`,  `ts`, `company`, `luts`, `name`, `description`, `add1`, `add2`, `city`, `state`, `country`, `zip`, `email`, `phone` )
VALUES
( 0, NOW(), 1, NOW(), 'Headquarters', 'Corporate office in Oneonta, NY.', '40 Morgan Avenue', 'Unit 1', 'Oneonta', 'NY', 'USA', '13820-1246', 'support@mmogp.com', '607-441-3319' );

INSERT INTO `mmdev`.`contexts` 
( `type`, `acl`, `ts`, `owner`, `company`, `location`, `luts`, `email`, `phone`, `fax` )
VALUES
( 1, 2, NOW(), 1, 1, 1, NOW(), 'rik@mmogp.com', '607-441-3319', '877-267-0412' );

CREATE TABLE IF NOT EXISTS `mmdev`.`extensions`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `type` INT NULL,
  `ts` DATETIME NULL,
  `luts` DATETIME NULL,
  `luby` INT NULL,
  `name` TEXT NULL,
  `fancy_name` TEXT NULL,
  `description` TEXT NULL,
  `version` VARCHAR(255) NULL,
  `author` VARCHAR(255) NULL,
  `website` VARCHAR(255) NULL,
  `token` TEXT NULL,
  `enabled` INT NULL,
  PRIMARY KEY (`id`)
) ENGINE=NDBCLUSTER;

CREATE TABLE IF NOT EXISTS `mmdev`.`extensions-elevated`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `extension` INT NULL,
  `token` TEXT NULL,
  PRIMARY KEY (`id`)
) ENGINE=NDBCLUSTER;

INSERT INTO `mmdev`.`extensions`
( `type`, `ts`, `luts`, `luby`, `name`, `fancy_name`, `description`, `version`, `author`, `website`, `enabled` ) 
VALUES
( '3', NOW(), NOW(), 0, 'mmod-cms', 'MMod CMS', 'The proprietary CMS extension for mmod-xrm', '0.0.1', 'Richard B. Winters <rik@mmogp.com>', 'http://mmogp.com/', 1 ),
( '3', NOW(), NOW(), 0, 'mmod-utilities', 'MMod Utilities', 'The proprietary Utility extension for mmod-xrm', '0.0.1', 'Richard B. Winters <rik@mmogp.com>', 'http://mmogp.com/', 1 );

CREATE TABLE IF NOT EXISTS `mmdev`.`themes`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `type` INT NULL,
  `ts` DATETIME NULL,
  `luts` DATETIME NULL,
  `luby` INT NULL,
  `name` TEXT NULL,
  `fancy_name` TEXT NULL,
  `description` TEXT NULL,
  `version` VARCHAR(255) NULL,
  `author` VARCHAR(255) NULL,
  `license` VARCHAR(255) NULL,
  `website` VARCHAR(255) NULL,
  `token` TEXT NULL,
  `enabled` INT NULL,
  `settings` TEXT NULL,
  PRIMARY KEY (`id`)
) ENGINE=NDBCLUSTER;

CREATE TABLE IF NOT EXISTS `mmdev`.`themes-elevated`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `extension` INT NULL,
  `token` TEXT NULL,
  PRIMARY KEY (`id`)
) ENGINE=NDBCLUSTER;

INSERT INTO `mmdev`.`themes`
( `type`, `ts`, `luts`, `luby`, `name`, `fancy_name`, `description`, `version`, `author`, `license`, `website`, `enabled`, `settings` ) 
VALUES
( '1', NOW(), NOW(), 0, 'mmxrm-admin', 'MMod XRM Admin Theme', 'The default admin layout for mmod-xrm', '1.0.0', 'Richard B. Winters <rik@mmogp.com>', 'Apache-2.0', 'http://mmogp.com/', 1, '{ "namespace": "mmod", "module_positions": [ "menu", "content", "sidebar-left", "sidebar-right" ], "update-url": "https://code.mmogp.com/mmod/mmod-xrm-layouts/tree/master/mmod/mmxrm-admin" }' ),
( '2', NOW(), NOW(), 0, 'mmxrm', 'MMod XRM Theme', 'The default layout for mmod-xrm', '1.0.0', 'Richard B. Winters <rik@mmogp.com>', 'Apache-2.0', 'http://mmogp.com/', 1, '{ "namespace": "mmod", "module_positions": [ "sysutil", "menu", "breadcrumbs", "content", "footer" ], "update-url": "https://code.mmogp.com/mmod/mmod-xrm-layouts/tree/master/mmod/mmxrm" }' );

CREATE TABLE IF NOT EXISTS `mmdev`.`module-types`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `provider` INT NOT NULL,
  `name` TEXT NULL,
  `description` TEXT NULL,
  PRIMARY KEY (`id`)
) ENGINE=NDBCLUSTER;

INSERT INTO `mmdev`.`module-types`
( `provider`, `name`, `description` ) 
VALUES
( '1', 'Menu', 'A module for rendering a navigation menu' ),
( '1', 'Breadcrumb', 'A module for rendering breadcrumb links.' ),
( '1', 'Featured', 'A module for rendering excerpts of Featured content on the main page of the application.' ),
( '1', 'Fora', 'A module for rendering fora content in various styles.' ),
( '1', 'Footer', 'A module for rendering footer content.' ),
( '2', 'Utility', 'A module for rendering various utilities.' );

CREATE TABLE IF NOT EXISTS `mmdev`.`modules`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `provider` INT NOT NULL,
  `type` INT NOT NULL,
  `ts` DATETIME NULL,
  `luts` DATETIME NULL,
  `luby` INT NULL,
  `name` TEXT NULL,
  `description` TEXT NULL,
  `position` VARCHAR(255) NULL,
  `token` TEXT NULL,
  `enabled` INT NULL,
  `params` TEXT NULL,
  PRIMARY KEY (`id`)
) ENGINE=NDBCLUSTER;

CREATE TABLE IF NOT EXISTS `mmdev`.`modules-elevated`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `module` INT NULL,
  `token` TEXT NULL,
  PRIMARY KEY (`id`)
) ENGINE=NDBCLUSTER;

INSERT INTO `mmdev`.`modules`
( `provider`, `type`, `ts`, `luts`, `luby`, `name`, `description`, `position`, `enabled`, `params` ) 
VALUES
( '2', '6',  NOW(), NOW(), 0, 'Lytebox', 'The application lytebox module', 'utility', '1', '{ "utility_options": { "type": "lytebox" } }' ),
( '2', '6',  NOW(), NOW(), 0, 'Kwaeri Chat', 'The application Kwaeri Chat module', 'utility', '1', '{ "utility_options": { "type": "kwaerichat" } }' ),
( '1', '1',  NOW(), NOW(), 0, 'Main Menu', 'The application menu module', 'menu', '1', '{ "menu_options": { "style": "modern" } }' ),
( '1', '5',  NOW(), NOW(), 0, 'App Footer', 'The application footer module', 'footer', '1', '{ "footer_options": { "style": "modern" } }' );

CREATE SCHEMA IF NOT EXISTS `mmpro` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ;
CREATE USER 'mmpadm'@'localhost' IDENTIFIED BY '^ProPass777$';
CREATE USER 'mmpadm'@'%' IDENTIFIED BY '^ProPass777$';
GRANT ALL ON `mmpro`.* TO 'mmpadm'@'localhost';
GRANT ALL ON `mmpro`.* TO 'mmpadm'@'%';

CREATE TABLE `mmpro`.`users` LIKE `mmdev`.`users`;
INSERT INTO `mmpro`.`users` SELECT * FROM `mmdev`.`users`;

CREATE TABLE `mmpro`.`acls` LIKE `mmdev`.`acls`;
INSERT INTO `mmpro`.`acls` SELECT * FROM `mmdev`.`acls`;

CREATE TABLE `mmpro`.`elevated` LIKE `mmdev`.`elevated`;
INSERT INTO `mmpro`.`elevated` SELECT * FROM `mmdev`.`elevated`;

CREATE TABLE `mmpro`.`contexts` LIKE `mmdev`.`contexts`;
INSERT INTO `mmpro`.`contexts` SELECT * FROM `mmdev`.`contexts`;

CREATE TABLE `mmpro`.`companies` LIKE `mmdev`.`companies`;
INSERT INTO `mmpro`.`companies` SELECT * FROM `mmdev`.`companies`;

CREATE TABLE `mmpro`.`locations` LIKE `mmdev`.`locations`;
INSERT INTO `mmpro`.`locations` SELECT * FROM `mmdev`.`locations`;

CREATE TABLE `mmpro`.`extensions` LIKE `mmdev`.`extensions`;
INSERT INTO `mmpro`.`extensions` SELECT * FROM `mmdev`.`extensions`;

CREATE TABLE `mmpro`.`extensions-elevated` LIKE `mmdev`.`extensions-elevated`;
INSERT INTO `mmpro`.`extensions-elevated` SELECT * FROM `mmdev`.`extensions-elevated`;

CREATE TABLE `mmpro`.`module-types` LIKE `mmdev`.`module-types`;
INSERT INTO `mmpro`.`module-types` SELECT * FROM `mmdev`.`module-types`;

CREATE TABLE `mmpro`.`modules` LIKE `mmdev`.`modules`;
INSERT INTO `mmpro`.`modules` SELECT * FROM `mmdev`.`modules`;

CREATE TABLE `mmpro`.`modules-elevated` LIKE `mmdev`.`modules-elevated`;
INSERT INTO `mmpro`.`modules-elevated` SELECT * FROM `mmdev`.`modules-elevated`;

CREATE TABLE `mmpro`.`themes` LIKE `mmdev`.`themes`;
INSERT INTO `mmpro`.`themes` SELECT * FROM `mmdev`.`themes`;

CREATE TABLE `mmpro`.`themes-elevated` LIKE `mmdev`.`themes-elevated`;
INSERT INTO `mmpro`.`themes-elevated` SELECT * FROM `mmdev`.`themes-elevated`;
