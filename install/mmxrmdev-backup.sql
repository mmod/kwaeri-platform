-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: localhost    Database: mmxrmdev
-- ------------------------------------------------------
-- Server version	5.7.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `acls`
--

DROP TABLE IF EXISTS `acls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) DEFAULT NULL,
  `description` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acls`
--

LOCK TABLES `acls` WRITE;
/*!40000 ALTER TABLE `acls` DISABLE KEYS */;
INSERT INTO `acls` VALUES (1,'Guest','Default access level for non-registered users.'),(2,'Registered','Access level for registered users.'),(3,'Moderator','Access level for registered moderators.'),(4,'Administrator','Access level for registered admin users.');
/*!40000 ALTER TABLE `acls` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `companies`
--

DROP TABLE IF EXISTS `companies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT NULL,
  `ts` datetime DEFAULT NULL,
  `owner` int(11) DEFAULT NULL,
  `luts` datetime DEFAULT NULL,
  `luby` int(11) DEFAULT NULL,
  `name` mediumtext,
  `description` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `companies`
--

LOCK TABLES `companies` WRITE;
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;
INSERT INTO `companies` VALUES (1,NULL,'2015-04-02 19:14:33',1,'2015-04-02 19:14:33',NULL,'Massively Modified, Inc.','Custom computer programming services. Game and Simulation specialization.');
/*!40000 ALTER TABLE `companies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contexts`
--

DROP TABLE IF EXISTS `contexts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contexts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT NULL,
  `acl` int(11) DEFAULT NULL,
  `ts` datetime DEFAULT NULL,
  `owner` int(11) DEFAULT NULL,
  `company` int(11) DEFAULT NULL,
  `location` int(11) DEFAULT NULL,
  `luts` datetime DEFAULT NULL,
  `luby` int(11) DEFAULT NULL,
  `add1` mediumtext,
  `add2` mediumtext,
  `city` varchar(191) DEFAULT NULL,
  `state` varchar(191) DEFAULT NULL,
  `country` varchar(191) DEFAULT NULL,
  `zip` varchar(65) DEFAULT NULL,
  `email` mediumtext,
  `phone` varchar(65) DEFAULT NULL,
  `fax` varchar(65) DEFAULT NULL,
  `ext` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contexts`
--

LOCK TABLES `contexts` WRITE;
/*!40000 ALTER TABLE `contexts` DISABLE KEYS */;
INSERT INTO `contexts` VALUES (1,0,2,'2015-04-02 19:14:32',1,NULL,NULL,'2015-04-02 19:14:32',NULL,'40 Morgan Avenue','Apt 1','Oneonta','NY','USA','13820-1246','rik@mmogp.com','607-267-1424',NULL,NULL),(2,1,2,'2015-04-02 19:14:34',1,1,1,'2015-04-02 19:14:34',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'rik@mmogp.com','607-441-3319','877-267-0412',NULL),(3,0,3,'2015-10-18 20:31:39',2,NULL,NULL,'2015-10-18 20:31:39',NULL,'','','Jaipur','','India','','ravi@mmogp.com','',NULL,NULL),(4,0,1,'2015-10-18 20:31:39',3,NULL,NULL,'2015-10-18 20:31:39',NULL,'','','','Ca','USA','','dave@mmogp.com','',NULL,NULL),(5,0,1,'2015-10-18 20:31:39',4,NULL,NULL,'2015-10-18 20:31:39',NULL,'','','Oneonta','New York','USA','13820','edisoncomputers@gmail.com','607-437-4766',NULL,NULL),(6,0,1,'2015-10-18 20:31:39',5,NULL,NULL,'2015-10-18 20:31:39',NULL,'1672 State Highway 7','','Unadilla','NY','USA','13849','speedietaynor24@gmail.com','',NULL,NULL),(7,0,1,'2015-10-18 20:31:39',6,NULL,NULL,'2015-10-18 20:31:39',NULL,'1672 State Highway 7','','Unadilla','NY','USA','13849','tooneytaynor@gmail.com','',NULL,NULL),(8,0,3,'2016-01-25 02:23:25',7,NULL,NULL,'2016-01-25 02:23:25',NULL,'Sushila Sadan, Jamtalla','Uttarmath, Hatiara','JKolkata','West Bengal','India','700157','kan@mmogp.com','',NULL,NULL);
/*!40000 ALTER TABLE `contexts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elevated`
--

DROP TABLE IF EXISTS `elevated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elevated` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `secure` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elevated`
--

LOCK TABLES `elevated` WRITE;
/*!40000 ALTER TABLE `elevated` DISABLE KEYS */;
INSERT INTO `elevated` VALUES (1,1,'3f40e560164e9e602652e517117ac97421d81a9742318ba5c60c4698ae94ee49'),(2,2,'42df385d8574ca064666c822918913e0e9f615965523fea1c93e33da276e69c3'),(3,7,'a8e7b190bf2cad5a438f8da19f0093b1291dba59ee0bcecaf112aad00e7e3d4c');
/*!40000 ALTER TABLE `elevated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `extensions`
--

DROP TABLE IF EXISTS `extensions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `extensions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT NULL,
  `ts` datetime DEFAULT NULL,
  `luts` datetime DEFAULT NULL,
  `luby` int(11) DEFAULT NULL,
  `name` mediumtext,
  `fancy_name` mediumtext,
  `description` mediumtext,
  `version` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `token` mediumtext,
  `enabled` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `extensions`
--

LOCK TABLES `extensions` WRITE;
/*!40000 ALTER TABLE `extensions` DISABLE KEYS */;
INSERT INTO `extensions` VALUES (1,NULL,'2015-05-13 18:43:11','2015-05-13 18:43:11',0,'mmod-cms','MMod CMS','The proprietary CMS extension for mmod-xrm','0.0.1','Richard B. Winters <rik@mmogp.com>','http://mmogp.com/',NULL,1),(2,3,'2015-12-27 06:30:52','2015-12-27 06:30:52',0,'mmod-utilities','MMod Utilities','The proprietary Utility extension for mmod-xrm','0.0.1','Richard B. Winters <rik@mmogp.com>','http://mmogp.com/',NULL,1);
/*!40000 ALTER TABLE `extensions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `extensions-elevated`
--

DROP TABLE IF EXISTS `extensions-elevated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `extensions-elevated` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `extension` int(11) DEFAULT NULL,
  `token` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `extensions-elevated`
--

LOCK TABLES `extensions-elevated` WRITE;
/*!40000 ALTER TABLE `extensions-elevated` DISABLE KEYS */;
/*!40000 ALTER TABLE `extensions-elevated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locations`
--

DROP TABLE IF EXISTS `locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT NULL,
  `ts` datetime DEFAULT NULL,
  `company` int(11) DEFAULT NULL,
  `luts` datetime DEFAULT NULL,
  `luby` int(11) DEFAULT NULL,
  `name` mediumtext,
  `description` mediumtext,
  `add1` mediumtext,
  `add2` mediumtext,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `zip` varchar(45) DEFAULT NULL,
  `email` mediumtext,
  `phone` varchar(45) DEFAULT NULL,
  `ext` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locations`
--

LOCK TABLES `locations` WRITE;
/*!40000 ALTER TABLE `locations` DISABLE KEYS */;
INSERT INTO `locations` VALUES (1,0,'2015-04-02 19:14:34',1,'2015-04-02 19:14:34',NULL,'Headquarters','Corporate office in Oneonta, NY.','40 Morgan Avenue','Unit 1','Oneonta','NY','USA','13820-1246','support@mmogp.com','607-441-3319',NULL);
/*!40000 ALTER TABLE `locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mmod-cms-article-types`
--

DROP TABLE IF EXISTS `mmod-cms-article-types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mmod-cms-article-types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `acl` int(11) DEFAULT NULL,
  `ts` datetime DEFAULT NULL,
  `luts` datetime DEFAULT NULL,
  `luby` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `parent` int(11) DEFAULT NULL,
  `enabled` int(11) DEFAULT NULL,
  `options` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mmod-cms-article-types`
--

LOCK TABLES `mmod-cms-article-types` WRITE;
/*!40000 ALTER TABLE `mmod-cms-article-types` DISABLE KEYS */;
INSERT INTO `mmod-cms-article-types` VALUES (1,1,'2015-05-13 17:37:13','2015-05-13 17:37:13',0,'Article','A type which can be utilized for many purposes. A generalized page artifact.',0,1,'{}'),(2,1,'2015-05-13 17:37:13','2015-05-13 17:37:13',0,'Blog','A type which can be utilized for blogging purposes. A generalized blog artifact.',0,1,'{}');
/*!40000 ALTER TABLE `mmod-cms-article-types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mmod-cms-articles`
--

DROP TABLE IF EXISTS `mmod-cms-articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mmod-cms-articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT NULL,
  `published` int(11) DEFAULT NULL,
  `acl` int(11) DEFAULT NULL,
  `ts` datetime DEFAULT NULL,
  `owner` int(11) DEFAULT NULL,
  `luts` datetime DEFAULT NULL,
  `luby` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  `tags` text,
  `title` varchar(255) DEFAULT NULL,
  `alias` text,
  `description` text,
  `content` text,
  `keywords` text,
  `options` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mmod-cms-articles`
--

LOCK TABLES `mmod-cms-articles` WRITE;
/*!40000 ALTER TABLE `mmod-cms-articles` DISABLE KEYS */;
INSERT INTO `mmod-cms-articles` VALUES (1,1,1,1,'2015-05-13 17:36:23',1,'2016-05-22 23:48:05',1,2,'[\"portfolio\",\"showcase\",\"mmod\"]','Portfolio','portfolio','The MMOD Portfolio','<div id=\"portfolio\" data-grid=\"modastic\"></div>','[\"portfolio\",\"showcase\"]','{\"allow_comments\":false,\"social_forwarding\":{\"enabled\":false}}'),(2,2,1,1,'2015-05-13 17:36:23',1,'2016-01-21 21:25:38',1,3,'[\"site blog\",\"hot topic\"]','The Massively Modified Blog!','the-massively-modified-blog','Our very first blog, that\'s hot!','<p>MMOD CMS is here and it\'s packed full of features for you to enjoy. Use MMOD CMS to manage all aspects of your front-facing application:</p><p><ul><li>Application menus</li><li>Page content (Articles)</li><li>Fora content (Blogs, Discussions)</li><li>Categories</li><li>Modules for customizing the rendering of content within your application</li></ul></p><p></p><p><br /></p><p>This is your new blog, so why not dress it up a bit?&nbsp;</p><p>When you\'re ready to get started: Log in to the administrative application, visit the Extensions section, and select MMOD CMS from the list of extensions.</p>','[\"first blog\",\"first\",\"blog\",\"mmod-cms\",\"cms\",\"mmod-xrm\",\"xrm\",\"mmod\"]','{\"allow_comments\":false,\"social_forwarding\":{\"enabled\":false}}'),(3,1,1,1,'2015-05-13 17:36:23',1,'2016-05-22 23:02:51',1,1,'[\"support\",\"help desk\",\"contact us\"]','About Us','about','About us.','<p>Massively Modified, &nbsp;Inc. is a software publisher dedicated to the Open Source communities, ever moving forward with a mission to innovate - and to bring about real change in - real-life through the use of technology.<br /><br />Our online presence is a work-in-progress, and we plan to expand our domain and implement new features in real-time; meaning that this application is - as well as all applications within the mmogp.com domain - a live beta.<br /><br />Everyone is welcome to join us on freenode: #mmod</p><p>Alternatively, inquiries may also be sent to&nbsp;<a href=\"mailto:support@mmogp.com\">support</a></p>','[\"contact us\",\"contact\",\"us\",\"support\",\"help desk\",\"help\",\"mmod\"]','{\"allow_comments\":false,\"social_forwarding\":{\"enabled\":false}}'),(19,2,1,1,'2015-05-16 00:44:30',1,'2016-01-20 20:19:49',1,3,'[\"test content\",\"test article\"]','mmod-cms: The content extension for nk-xrm','mmod-cms-the-content-extension-for-nk-xrm','Testing blog entries, will update!','Testing the ability to create new articles, hence the need for this new article!<div><br /></div><div>This is the first edit :)</div>','[\"test article\",\"test\",\"article\"]','{\"allow_comments\":false,\"social_forwarding\":{\"enabled\":false}}'),(20,1,NULL,1,'2015-05-16 13:16:25',1,'2015-05-16 13:22:44',1,1,'[\"test content\",\"test article\"]','Article Test 2','','Continued test of the ability to add and edit articles','Here we go again!<div><br /></div><div>With an edit!</div><div><br /></div><div>And again!</div><div><br /></div><div>And even another!</div><div><br /></div><div>And yet another!</div>','[\"test article\",\"test\",\"article\"]','{\"allow_comments\":false,\"social_forwarding\":{\"enabled\":false}}'),(21,1,NULL,1,'2015-05-16 13:23:50',1,'2015-05-16 13:24:49',1,1,'[\"test content\",\"test article\"]','Article Test 3','','Further continued testing of the ability to add and edit articles','Testing once more, to ensure desired functionality and/or behavior.<div><br /></div><div>Making an edit to this</div>','[\"test article\",\"test\",\"article\"]','{\"allow_comments\":false,\"social_forwarding\":{\"enabled\":false}}'),(22,1,NULL,1,'2015-05-16 13:24:05',1,'2015-05-16 13:25:54',1,1,'[\"test content\",\"test article\"]','Article Test 4','','Even more testing of the ability to add and edit articles','Testing once more, to ensure desired functionality and/or behavior.<div><br /></div><div>Making an edit to Article Test 3 resulted in this here new article!?</div>','[\"test article\",\"test\",\"article\"]','{\"allow_comments\":false,\"social_forwarding\":{\"enabled\":false}}'),(23,1,1,1,'2015-05-16 18:37:14',1,'2015-05-31 23:19:50',1,1,'[\"test content\",\"test article\"]','Article Test 5','article-test-5','Testing articles again!','Another test article :)<span class=\"Apple-tab-span\" style=\"white-space:pre\">		</span>Tabs!','[\"test article\",\"test\",\"article\"]','{\"allow_comments\":false,\"social_forwarding\":{\"enabled\":false}}'),(24,1,NULL,1,'2015-05-16 18:40:39',1,'2015-05-16 18:40:39',1,1,'[\"test content\",\"test article\"]','Test Article 6','test-article-6','Testing articles with code and special characters','\0\0\0\0\0\0\0\0nna test an article which contains code and, subsequently, special characters. Previously we had issues submitting such data as our non-prepared queries were being escaped, etc.<div><br /></div><div>Now we try again, only using prepared statements to ensure integrity of data as well as intent! ;-)</div><div><br /></div><div><br /></div><div><span style=\"vertical-align: sub; font-weight: bold;\">// An example function</span></div><div><span style=\"vertical-align: sub; font-weight: bold;\">if( Object.hasOwnProperty( \'property\' ) )</span></div><div><span style=\"vertical-align: sub; font-weight: bold;\">{</span></div><div><span style=\"vertical-align: sub; font-weight: bold;\"><span class=\"Apple-tab-span\" style=\"white-space:pre\">	</span>return true;</span></div><div><span style=\"vertical-align: sub; font-weight: bold;\">}</span></div><div><span style=\"vertical-align: sub; font-weight: bold;\">else</span></div><div><span style=\"vertical-align: sub; font-weight: bold;\">{</span></div><div><span style=\"vertical-align: sub; font-weight: bold;\"><span class=\"Apple-tab-span\" style=\"white-space:pre\">	</span>return false;</span></div><div><span style=\"vertical-align: sub; font-weight: bold;\">}</span></div>','[\"test article\",\"test\",\"article\"]','{\"allow_comments\":false,\"social_forwarding\":{\"enabled\":false}}'),(25,1,NULL,1,'2015-05-16 19:07:42',1,'2015-05-21 10:20:09',1,1,'[\"test content\",\"test article\"]','test','test','Another test article','Special Chars?<div><div><br /></div><div><div><div>`1234567890-=[]\\;/?&gt;&lt;:|}{+_)(*&amp;^%$#@!~\'\"</div><div><br /></div><div>Code?</div><div><br /></div><div>if( you === true )</div><div>{</div><div>&nbsp; &nbsp; console.log( \'It was true!\' );</div><div>&nbsp; &nbsp; return true;</div><div>}</div><div>else</div><div>{</div>&nbsp; &nbsp; console.log( \'It was false!\' );<div>&nbsp; &nbsp; return false;</div><div>}</div><div><br /></div>Hope it works this time!</div></div></div>','[\"test article\",\"test\",\"article\"]','{\"allow_comments\":false,\"social_forwarding\":{\"enabled\":false}}'),(26,1,NULL,1,'2015-05-18 06:01:40',1,'2015-05-18 06:01:40',1,1,'[]','Over 10 Test','','A test for over 10 articles using datatables','Another quick tes','[]','{\"allow_comments\":false,\"social_forwarding\":{\"enabled\":false}}'),(27,1,NULL,1,'2015-05-18 06:03:10',1,'2015-05-18 06:03:10',1,1,'[]','Over 10 Test 2','','Yet another over 10 test','Yet another over 10 te','[]','{\"allow_comments\":false,\"social_forwarding\":{\"enabled\":false}}'),(28,1,1,1,'2015-05-18 06:03:37',1,'2015-12-11 09:38:22',1,1,'[]','Over 10 Test 3','','Yet another over 10 test','Yet another over 10 te','[]','{\"allow_comments\":false,\"social_forwarding\":{\"enabled\":false}}'),(29,1,NULL,1,'2015-05-18 06:07:10',1,'2015-05-18 06:07:10',1,1,'[]','Over 10 Test 4','','Yet another over 10 test','Yet another over 10 te','[]','{\"allow_comments\":false,\"social_forwarding\":{\"enabled\":false}}'),(33,1,NULL,1,'2015-05-21 08:04:51',1,'2015-05-21 08:04:51',1,1,'[]','Over 10 Test 5','','Yet another over 10 test','Yet another over 10 test','[]','{\"allow_comments\":false,\"social_forwarding\":{\"enabled\":false}}'),(34,1,NULL,1,'2015-05-21 08:14:36',1,'2015-05-21 08:14:36',1,1,'[]','Over 10 Test 6','','Yet another over 10 test','Yet another over 10 test','[]','{\"allow_comments\":false,\"social_forwarding\":{\"enabled\":false}}'),(35,1,NULL,1,'2015-05-21 08:15:24',1,'2015-05-21 09:59:31',1,1,'[\"test content\",\"test article\"]','Over 10 Test 7','','Yet another over 10 test','Yet another over 10 test','[]','{\"allow_comments\":false,\"social_forwarding\":{\"enabled\":false}}'),(36,1,NULL,1,'2015-05-21 08:19:05',1,'2015-05-21 09:58:57',1,1,'[\"test content\",\"test article\"]','Over 10 Test 8','','Yet another over 10 test','Yet another over 10 test','[\"test article\",\"test\",\"article\"]','{\"allow_comments\":false,\"social_forwarding\":{\"enabled\":false}}'),(37,1,NULL,1,'2015-05-21 08:23:39',1,'2015-05-21 09:54:10',1,1,'[\"test content\",\"test article\"]','Over 10 Test 9','','Yet another over 10 test','Yet another over 10 test','[\"test article\",\"test\",\"article\"]','{\"allow_comments\":false,\"social_forwarding\":{\"enabled\":false}}'),(51,1,NULL,1,'2015-05-21 21:52:15',1,'2015-05-21 21:52:15',1,1,'[]','Over 20 Test','','Over 20 test article','Over 20 test article','[]','{\"allow_comments\":false,\"social_forwarding\":{\"enabled\":false}}'),(55,1,1,1,'2015-05-28 03:50:24',1,'2015-05-28 03:50:24',1,1,'[\"test content\",\"test article\"]','Over 20 Test','','Over 20 Test Article','Over 20 test article','[\"test article\",\"test\",\"article\"]','{\"allow_comments\":false,\"social_forwarding\":{\"enabled\":false}}'),(56,1,1,1,'2015-05-28 03:51:08',1,'2015-05-28 03:51:08',1,1,'[\"test content\",\"test article\"]','Over 20 Test 2','','Over 20 Test 2 Article','Over 20 test 2 article','[\"test article\",\"test\",\"article\"]','{\"allow_comments\":false,\"social_forwarding\":{\"enabled\":false}}'),(57,1,1,1,'2015-05-28 03:53:36',1,'2015-07-07 18:08:46',1,1,'[\"test content\",\"test article\"]','Over 20 Test 3','','Over 20 Test 3 Article','Over 20 test 3 article test 1000','[\"test article\",\"test\",\"article\"]','{\"allow_comments\":false,\"social_forwarding\":{\"enabled\":false}}'),(58,1,1,1,'2016-05-23 06:31:19',1,'2016-05-23 06:31:19',1,1,'[\"terms\"]','Terms of Use','terms','Application Terms of Use','Testing','[\"Terms\",\"EULA\"]','{\"allow_comments\":false,\"social_forwarding\":{\"enabled\":false}}');
/*!40000 ALTER TABLE `mmod-cms-articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mmod-cms-categories`
--

DROP TABLE IF EXISTS `mmod-cms-categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mmod-cms-categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `acl` int(11) DEFAULT NULL,
  `ts` datetime DEFAULT NULL,
  `luts` datetime DEFAULT NULL,
  `luby` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `parent` int(11) DEFAULT NULL,
  `enabled` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mmod-cms-categories`
--

LOCK TABLES `mmod-cms-categories` WRITE;
/*!40000 ALTER TABLE `mmod-cms-categories` DISABLE KEYS */;
INSERT INTO `mmod-cms-categories` VALUES (1,0,'2015-05-13 17:37:30','2015-05-13 17:37:30',0,'uncategorized','Category for uncategorized articles',0,2),(2,0,'2015-05-13 17:37:30','2015-05-13 17:37:30',0,'news','Category for news',0,2),(3,1,'2015-05-13 17:37:30','2015-07-25 21:35:55',1,'blog','Category for blogs',0,2),(9,1,'2015-06-03 20:42:17','2015-06-18 07:22:50',1,'Test 1','Test 1',0,1);
/*!40000 ALTER TABLE `mmod-cms-categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mmod-cms-menu-item-types`
--

DROP TABLE IF EXISTS `mmod-cms-menu-item-types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mmod-cms-menu-item-types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `acl` int(11) DEFAULT NULL,
  `ts` datetime DEFAULT NULL,
  `luts` datetime DEFAULT NULL,
  `luby` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `parent` int(11) DEFAULT NULL,
  `enabled` int(11) DEFAULT NULL,
  `options` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mmod-cms-menu-item-types`
--

LOCK TABLES `mmod-cms-menu-item-types` WRITE;
/*!40000 ALTER TABLE `mmod-cms-menu-item-types` DISABLE KEYS */;
INSERT INTO `mmod-cms-menu-item-types` VALUES (1,0,'2015-05-28 14:03:20','2015-05-28 14:03:20',0,'Alias','A menu type which provides a link to another location (i.e. to another alias, or any URL).',1,1,'{}'),(2,0,'2015-05-28 14:03:20','2015-05-28 14:03:20',0,'Fora','A menu type which can be utilized for rendering fora content.',1,1,'{}'),(3,0,'2016-01-04 01:58:27','2016-01-04 01:58:27',0,'View','A menu type which can be utilized for rendering article content.',1,1,'{}');
/*!40000 ALTER TABLE `mmod-cms-menu-item-types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mmod-cms-menu-items`
--

DROP TABLE IF EXISTS `mmod-cms-menu-items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mmod-cms-menu-items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT NULL,
  `acl` int(11) DEFAULT NULL,
  `ts` datetime DEFAULT NULL,
  `luts` datetime DEFAULT NULL,
  `luby` int(11) DEFAULT NULL,
  `href` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `menu` int(11) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `enabled` int(11) DEFAULT NULL,
  `options` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mmod-cms-menu-items`
--

LOCK TABLES `mmod-cms-menu-items` WRITE;
/*!40000 ALTER TABLE `mmod-cms-menu-items` DISABLE KEYS */;
INSERT INTO `mmod-cms-menu-items` VALUES (1,1,1,'2015-05-28 14:11:27','2015-06-07 10:29:34',1,NULL,'Home','Alias for the base domain.',1,0,1,'{}'),(2,3,1,'2015-05-28 14:11:27','2016-01-22 07:11:18',1,'view/about','About','About menu and link to about page',1,0,2,'{\"content_type\":\"article\",\"article_alias\":\"about\"}'),(3,3,1,'2015-05-28 14:11:27','2016-05-22 00:08:10',1,'view/portfolio','Portfolio','Services menu and link to services page.',1,0,2,'{\"content_type\":\"article\",\"article_alias\":\"portfolio\"}'),(4,1,0,'2015-05-28 14:11:27','2015-05-28 14:11:27',0,NULL,'Documentation','Documentation menu and link to documentation page.',1,0,1,'{\"content_type\":1}'),(5,2,1,'2015-05-28 14:11:27','2016-01-04 06:58:57',1,'fora/blog','Fora','Fora menu and link to the fora page.',1,0,2,'{ \"show\": \"all\", \"category\": false, \"blog\": false }'),(6,3,1,'2015-06-02 06:21:24','2016-05-23 06:45:58',1,NULL,'Privacy','Privacy Policy',1,2,2,'{}'),(23,3,1,'2015-06-02 20:03:41','2016-01-22 08:27:46',1,NULL,'Contact Us','Contact us menu item and link to Contact us page',1,2,2,'{}'),(24,3,1,'2016-05-23 06:39:25','2016-05-23 06:39:25',1,NULL,'Terms of Use','Application Terms of Use',1,2,2,'{\"content_type\":\"\"}');
/*!40000 ALTER TABLE `mmod-cms-menu-items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mmod-cms-menus`
--

DROP TABLE IF EXISTS `mmod-cms-menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mmod-cms-menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ts` datetime DEFAULT NULL,
  `luts` datetime DEFAULT NULL,
  `luby` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `enabled` int(11) DEFAULT NULL,
  `options` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mmod-cms-menus`
--

LOCK TABLES `mmod-cms-menus` WRITE;
/*!40000 ALTER TABLE `mmod-cms-menus` DISABLE KEYS */;
INSERT INTO `mmod-cms-menus` VALUES (1,'2015-05-28 14:13:22','2015-06-03 20:43:14',1,'Main','The application menu',2,'{}'),(4,'2015-06-02 06:14:00','2015-07-25 21:36:52',1,'Menu 2','Test menu',2,'{}');
/*!40000 ALTER TABLE `mmod-cms-menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mmod-cms-settings`
--

DROP TABLE IF EXISTS `mmod-cms-settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mmod-cms-settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ts` datetime DEFAULT NULL,
  `luts` datetime DEFAULT NULL,
  `luby` int(11) DEFAULT NULL,
  `article_settings` text NOT NULL,
  `blog_settings` text NOT NULL,
  `social_settings` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mmod-cms-settings`
--

LOCK TABLES `mmod-cms-settings` WRITE;
/*!40000 ALTER TABLE `mmod-cms-settings` DISABLE KEYS */;
INSERT INTO `mmod-cms-settings` VALUES (1,'2015-05-13 17:30:20','2015-05-13 17:30:20',0,'{\"tags_enabled\":true,\"comments_enabled\":true}','{\"tags_enabled\":true,\"comments_enabled\":true}','{}');
/*!40000 ALTER TABLE `mmod-cms-settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module-types`
--

DROP TABLE IF EXISTS `module-types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module-types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `provider` int(11) NOT NULL,
  `name` text,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module-types`
--

LOCK TABLES `module-types` WRITE;
/*!40000 ALTER TABLE `module-types` DISABLE KEYS */;
INSERT INTO `module-types` VALUES (1,1,'Menu','A module for rendering a navigation menu'),(2,1,'Breadcrumb','A module for rendering breadcrumb links.'),(3,1,'Featured','A module for rendering excerpts of Featured content on the main page of the application.'),(4,1,'Fora','A module for rendering fora content in various styles.'),(5,1,'Footer','A module for rendering footer content.'),(6,2,'Utility','A module for rendering various utilities.'),(7,1,'View','A module for rendering page content.');
/*!40000 ALTER TABLE `module-types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modules`
--

DROP TABLE IF EXISTS `modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `provider` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `ts` datetime DEFAULT NULL,
  `luts` datetime DEFAULT NULL,
  `luby` int(11) DEFAULT NULL,
  `name` text,
  `description` text,
  `position` varchar(255) DEFAULT NULL,
  `token` text,
  `enabled` int(11) DEFAULT NULL,
  `params` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modules`
--

LOCK TABLES `modules` WRITE;
/*!40000 ALTER TABLE `modules` DISABLE KEYS */;
INSERT INTO `modules` VALUES (1,1,1,'2015-12-05 05:02:37','2015-12-05 05:02:37',0,'Main Menu','The application menu module','menu',NULL,1,'{ \"menu\": \"1\", \"style\": \"modern\", \"dropdownEnabled\": true, \"authDropdownMenu\": [ { \"type\": \"link\", \"href\": \"view/about\", \"description\": \"Product & service documentation site\", \"title\": \"Documentation\", \"iconClass\": \"flaticon-book203\" }, { \"type\": \"link\", \"href\": \"view/about\", \"description\": \"Community support site\", \"title\": \"Support\", \"iconClass\": \"flaticon-help17\" }, { \"type\": \"link\", \"href\": \"account/preferences\", \"description\": \"Manage user preferences\", \"title\": \"Preferences\", \"iconClass\": \"flaticon-wrench72\" } ], \"dropdownMenu\": [ { \"type\": \"link\", \"href\": \"view/about\", \"description\": \"Community support site\", \"title\": \"Support\", \"iconClass\": \"flaticon-help17\" } ], \"authLinks\": [ { \"type\": \"link\", \"href\": \"view/about\", \"description\": \"Community support site\", \"title\": \"Help\", \"iconClass\": \"flaticon-help17\" } ], \"links\": [ { \"type\": \"link\", \"href\": \"account/login\", \"description\": \"Sign in to mmogp.com\", \"title\": \"Sign In\", \"iconClass\": false } ] }'),(2,1,5,'2015-12-07 12:24:21','2015-12-07 12:24:21',0,'App Footer','The application footer module','footer',NULL,1,'{ \"style\": \"modern\", \"copyrights\": [ { \"enabled\": true, \"date\": true, \"range\": false, \"dateStart\": \"2011\", \"entity\": \"Massively Modified, Inc\" } ], \"links\": [ { \"type\": \"link\", \"href\": \"about/terms\", \"description\": \"View terms and conditions of use\", \"title\": \"Terms\", \"iconClass\": \"flaticon-list81 mpbf\" }, { \"type\": \"link\", \"href\": \"about/privacy\", \"description\": \"View our privacy policy\", \"title\": \"Privacy Policy\", \"iconClass\": \"flaticon-invisible3 mpbf\" } ], \"icons\": [ { \"name\": \"icon-logo-badge-small\" } ], \"socials\": [ { \"provider\": \"linkedin\", \"description\": \"View our LinkedIn profile\", \"href\": \"www.linkedin.com/company/massively-modified-inc-\", \"target\": \"_blank\", \"style\": \"padding: 0 20px 0 10px; font-size: 33px; font-weight: bold; color: #666;\", \"iconClass\": \"\", \"text\": \"in\" }, { \"provider\": \"facebook\", \"description\": \"Visit our facebook page\", \"href\": \"www.facebook.com/massivelymodified\", \"style\": \"\", \"iconClass\": \"flaticon-facebook55\", \"text\": \"\" }, { \"provider\": \"twitter\", \"description\": \"Check out our tweets\", \"href\": \"www.twitter.com/mmogp\", \"style\": \"\", \"iconClass\": \"flaticon-twitter45 icon-label\", \"text\": \"\" } ] }'),(3,2,6,'2015-12-27 06:22:56','2015-12-27 06:22:56',0,'Lytebox','The application lytebox module','utility',NULL,1,'{ \"utility_options\": { \"type\": 1 } }'),(4,2,6,'2015-12-27 06:23:35','2015-12-27 06:23:35',0,'Kwaeri Chat','The application Kwaeri Chat module','utility',NULL,1,'{ \"utility_options\": { \"type\": 2 } }');
/*!40000 ALTER TABLE `modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modules-elevated`
--

DROP TABLE IF EXISTS `modules-elevated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modules-elevated` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module` int(11) DEFAULT NULL,
  `token` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modules-elevated`
--

LOCK TABLES `modules-elevated` WRITE;
/*!40000 ALTER TABLE `modules-elevated` DISABLE KEYS */;
/*!40000 ALTER TABLE `modules-elevated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `themes`
--

DROP TABLE IF EXISTS `themes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `themes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT NULL,
  `ts` datetime DEFAULT NULL,
  `luts` datetime DEFAULT NULL,
  `luby` int(11) DEFAULT NULL,
  `name` text,
  `fancy_name` text,
  `description` text,
  `version` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `license` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `token` text,
  `enabled` int(11) DEFAULT NULL,
  `settings` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `themes`
--

LOCK TABLES `themes` WRITE;
/*!40000 ALTER TABLE `themes` DISABLE KEYS */;
INSERT INTO `themes` VALUES (1,1,'2015-12-05 01:01:37','2015-12-05 01:01:37',0,'mmxrm-admin','MMod XRM Admin Theme','The default admin layout for mmod-xrm','1.0.0','Richard B. Winters <rik@mmogp.com>','Apache-2.0','http://mmogp.com/',NULL,1,'{ \"namespace\": \"mmod\", \"module_positions\": [ \"menu\", \"content\", \"sidebar-left\", \"sidebar-right\" ], \"update-url\": \"https://code.mmogp.com/mmod/mmod-xrm-layouts/tree/master/mmod/mmxrm-admin\" }'),(2,2,'2015-12-05 01:01:37','2015-12-05 01:01:37',0,'mmxrm','MMod XRM Theme','The default layout for mmod-xrm','1.0.0','Richard B. Winters <rik@mmogp.com>','Apache-2.0','http://mmogp.com/',NULL,1,'{ \"namespace\": \"mmod\", \"module_positions\": [ \"utility\", \"menu\", \"breadcrumbs\", \"content\", \"footer\" ], \"update-url\": \"https://code.mmogp.com/mmod/mmod-xrm-layouts/tree/master/mmod/mmxrm\" }');
/*!40000 ALTER TABLE `themes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `themes-elevated`
--

DROP TABLE IF EXISTS `themes-elevated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `themes-elevated` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `extension` int(11) DEFAULT NULL,
  `token` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `themes-elevated`
--

LOCK TABLES `themes-elevated` WRITE;
/*!40000 ALTER TABLE `themes-elevated` DISABLE KEYS */;
/*!40000 ALTER TABLE `themes-elevated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT NULL,
  `acl` int(11) DEFAULT NULL,
  `ts` datetime DEFAULT NULL,
  `luts` datetime DEFAULT NULL,
  `luby` int(11) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` mediumtext,
  `first` varchar(100) DEFAULT NULL,
  `middle` varchar(45) DEFAULT NULL,
  `last` varchar(100) DEFAULT NULL,
  `email` mediumtext,
  `context` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,2,4,'2014-11-30 23:38:13','2015-06-08 10:25:22',1,'rik','3f40e560164e9e602652e517117ac97421d81a9742318ba5c60c4698ae94ee49','Richard','B','Winters','rik@mmogp.com',1),(2,1,3,'2014-11-30 23:38:13','2014-11-30 23:38:13',NULL,'ravi','42df385d8574ca064666c822918913e0e9f615965523fea1c93e33da276e69c3','Ravi',NULL,'Soni','ravi@mmogp.com',3),(3,1,1,'2014-11-30 23:38:13','2014-11-30 23:38:13',NULL,'dmrls','3f81820227da4c4047986679df146539e419d779835577b8db9a51877a122fdc','Dave',NULL,'Morales','dave@mmogp.com',4),(4,1,1,'2014-11-30 23:38:13','2014-11-30 23:38:13',NULL,'ahren','5b19781846be6a7adacec671c5eed36d4cb3c3902fe21a46a8b3748a34227ad4','Ahren',NULL,'Edison','edisoncomputers@gmail.com',5),(5,1,1,'2014-11-30 23:38:13','2014-11-30 23:38:13',NULL,'dana','502a1a79e74a2f368646d77edbb67aad5e43012db8e8eb10267151b00b6f9ac2','Jordana',NULL,'Taynor','speedietaynor24@gmail.com',6),(6,1,1,'2014-11-30 23:38:13','2014-11-30 23:38:13',NULL,'shanna','41ae0defc0c6ffe9ba252ed80a4c443434b74b9434838729fd9071aaf47c68cf','Shanna',NULL,'Taynor','toonietaynor@gmail.com',7),(7,1,3,'2014-11-30 23:38:13','2014-11-30 23:38:13',NULL,'kan','a8e7b190bf2cad5a438f8da19f0093b1291dba59ee0bcecaf112aad00e7e3d4c','Kanhaiya',NULL,'Jha','kan@mmogp.com',8);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-11 11:40:42
