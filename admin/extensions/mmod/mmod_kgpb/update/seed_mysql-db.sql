CREATE TABLE IF NOT EXISTS `mmdev`.`mmod-kgpb-settings`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `ts` DATETIME NULL,
  `luts` DATETIME NULL,
  `luby` INT NULL,
  `kgp_settings` TEXT NOT NULL,
  `social_settings` TEXT NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=NDBCLUSTER;

INSERT INTO `mmdev`.`mmod-kgpb-settings`
( `ts`, `luts`, `luby`, `kgp_settings`, `social_settings` )
VALUES
( NOW(), NOW(), 0, '{ "tags_enabled":true, "comments_enabled":true }', '{}' );


CREATE TABLE `mmpro`.`mmod-kgpb-settings` LIKE `mmdev`.`mmod-kgpb-settings`;
INSERT INTO `mmpro`.`mmod-kgpb-settings` SELECT * FROM `mmdev`.`mmod-kgpb-settings`;
