/**
 * package: mmod-cms
 * sub-package: controllers/dash
 * author: Richard B. Winters <a href="mailto:rik@mmogp.com">rik AT mmogp DOT com</a>
 * copyright: 2011-2015 Massively Modified, Inc.
 * license: See Terms and EULA at https://mmogp.com/about/terms
 */


/**
 * Constructor
 *
 * @since 0.0.1
 */
function dashController()
{
}


// HTTP GET /extensions/manage/mmod-cms/dash
dashController.prototype.index = function( request, response )
{
    // We just need to display a message here
    this.klay.viewbag.title = 'Extension Dashboard';
    this.klay.viewbag.pagetitle = 'MMod-CMS';

    this.rendr( request, response );
};


// Export
module.exports = exports = dashController;