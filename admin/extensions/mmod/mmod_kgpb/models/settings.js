/**
 * package: mmod-cms
 * sub-package: models/settings
 * author: Richard B. Winters <a href="mailto:rik@mmogp.com">rik AT mmogp DOT com</a>
 * copyright: 2011-2015 Massively Modified, Inc.
 * license: See Terms and EULA at https://mmogp.com/about/terms
 */


// Deps
var nk = require( 'nk' ),
nk = new nk();  // When we do not pass an argument to the constructor, we
                // get only the core facilities ( .type, .extend, .each,
                // .hash, etc )


/**
 * Defines the settings model
 *
 * @since 0.0.1
 */
var settingsModel =
{
    settings:
    {
        listAll: function( request, response, callback, klay )
        {
            // Prep the database object
            var db = this.dbo();

            // Rid the DBO of any models currently set.
            db.reset();

            // Get a list of all settings
            var settings = db
            .query( "select * from `mmod-cms-settings`" )
            .execute();

            if( nk.type( settings ) === 'array' )
            {
                // Great, we have an array which should be full of setting records
                //console.log( settings );
                klay.viewbag.settings = settings;
            }
            else
            {
                console.log( 'Failed to get a list of settings mang...' );
                klay.viewbag.settings = {};
            }

            // And invoke the callback, passing the result of our query
            if( nk.type( callback ) === 'function' )
            { // Asynchronous
                callback( request, response, settings, klay );
            }
            else
            { // Synchronous
                return settings;
            }
        },
        saveAll: function()
        {

        },
        schema:
        {
            /*
             * settings Model
             */
            id: [ true, 'int', ''],
            ts: [ true, 'text', 'Created' ],
            luts: [ true, 'text', 'Updated' ],
            luby: [ true, 'text', 'Updated by' ],
            article_settings: [ true, 'text', 'Article' ],
            blog_settings: [ true, 'text', 'Blog' ],
            social_settings: [ true, 'text', 'Social' ]
        }
    }
};


// Export
module.exports = settingsModel;