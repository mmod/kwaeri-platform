/**
 * package: mmod-cms
 * sub-package: controllers/menus
 * author: Richard B. Winters <a href="mailto:rik@mmogp.com">rik AT mmogp DOT com</a>
 * copyright: 2011-2015 Massively Modified, Inc.
 * license: See Terms and EULA at https://mmogp.com/about/terms
 */


// Deps
var nk = require( 'nk' ),
nk = new nk();  // When we do not pass an argument to the constructor, we
                // get only the core facilities ( .type, .extend, .each,
                // .hash, etc )


/**
 * Constructor
 *
 * @since 0.0.1
 */
function menusController()
{
}

// HTTP GET /extensions/mmod-cms/menus
menusController.prototype.index = function( request, response )
{
    var na = this;

    var menusModel = require( '../models/menus' ).menus,
    model = this.modl.set( menusModel );

    this.klay.model = require( '../models/menus' ).listView;

    // We just need to display a message here
    this.klay.viewbag.pagetitle = 'Menus';

    // Here we define the callback for our list method
    var callback = function( req, res, tk )
    {
        na.rendr( req, res );
    };

    model.listAll( request, response, callback, this.klay );
};


//HTTP GET /extensions/mmod-cms/menus/add
menusController.prototype.add = function( request, response )
{
    var na = this;

    var menusModel = require( '../models/menus' ).addView,
    model = this.modl.set( menusModel );

    this.klay.model = menusModel;
    this.klay.viewbag.pagetitle = 'Add a New Menu';
    this.klay.viewbag.menu = { id: '', title: '', description: '', enabled: '', options: '' };

    // Here we define the callback for our method
    var callback = function( req, res, query_good, tk )
    {
        if( !query_good )
        {
            console.log( "Menu input field options were not returned" );
        }

        na.rendr( req, res );
    };

    model.getMenuInputFieldOptions( request, response, callback, this.klay );
};


//HTTP POST /extensions/mmod-cms/menuss/add
menusController.prototype.addPost = function( request, response )
{
    var na = this;

    var menusModel = require( '../models/menus' ).addView,
    model = this.modl.set( menusModel );

    this.klay.model = menusModel;
    this.klay.viewbag.pagetitle = 'Add a New Menu';
    this.klay.viewbag.menu = { id: '', title: '', description: '', enabled: '', options: '' };

    // Here we define the callback for our list method
    var callback = function( req, res, tk )
    {
        na.rendr( req, res );
    };

    model.addMenu( request, response, callback, this.klay );
};


//HTTP GET /extensions/mmod-cms/menus/edit
menusController.prototype.edit = function( request, response )
{
    var na = this;

    var menusModel = require( '../models/menus' ).editView,
    model = this.modl.set( menusModel );

    this.klay.model = menusModel;

    // We just need to display a message here
    this.klay.viewbag.pagetitle = 'Menus';

     // Here we define the callback for our list method
     var callback = function( req, res, tk )
     {
         if( tk.viewbag.menu && !nk.isEmptyObject( tk.viewbag.menu ) )
         {
             var tmp = '';
             var options = JSON.parse( tk.viewbag.menu.options );
             for( var option in options )
             {
                 tmp += '[' + option + ': ' + options[option] + '] ';
             }
             tk.viewbag.menu.options = tmp;

             // There can only be one menu to edit at a time ofc...
             na.klay.viewbag.menu = tk.viewbag.menu;
        }
        else
        {
            console.log( "Menu not returned" );
            na.klay.viewbag.menu = [];
        }

         na.rendr( req, res );
    };

    model.getMenu( request, response, callback, this.klay );
};


//HTTP POST /extensions/mmod-cms/menus/edit
menusController.prototype.editPost = function( request, response )
{
    var na = this;

    var menusModel = require( '../models/menus' ).editView,
    model = this.modl.set( menusModel );

    this.klay.model = menusModel;

    // We just need to display a message here
    this.klay.viewbag.pagetitle = 'Menus';

     // Here we define the callback for our list method
     var callback = function( req, res, tk )
     {
         if( tk.viewbag.menu && !nk.isEmptyObject( tk.viewbag.menu ) )
         {
             var tmp = '';
             if( tk.viewbag.menu.options )
             {
                 var options = JSON.parse( tk.viewbag.menu.options );
                 for( var option in options )
                 {
                     tmp += '[' + option + ': ' + options[option] + '] ';
                 }
                 tk.viewbag.menu.options = tmp;
             }

             na.klay.viewbag.menu = tk.viewbag.menu;
        }
        else
        {
            console.log( 'Menu not returned' );
        }

        na.rendr( req, res );
    };

    model.editMenu( request, response, callback, this.klay );
};


//HTTP POST /extensions/mmod-cms/menus/copy
menusController.prototype.copyPost = function( request, response )
{
    var na = this;

    var menusModel = require( '../models/menus' ).copyView,
    model = this.modl.set( menusModel );

    this.klay.model = menusModel;

    // We just need to display a message here
    this.klay.viewbag.pagetitle = 'Menus';

    // Here we define the callback for our list method
    var callback = function( req, res, tk )
    {
        na.rendr( req, res );
    };

    model.copyMenu( request, response, callback, this.klay );
};


//HTTP POST /extensions/mmod-cms/menus/delete
menusController.prototype.deletePost = function( request, response )
{
    var na = this;

    var menusModel = require( '../models/menus' ).deleteView,
    model = this.modl.set( menusModel );

    this.klay.model = menusModel;

    // We just need to display a message here
    this.klay.viewbag.pagetitle = 'Menus';

    // Here we define the callback for our list method
    var callback = function( req, res, tk )
    {
        na.rendr( req, res );
    };

    model.deleteMenu( request, response, callback, this.klay );
};


// Export
module.exports = exports = menusController;