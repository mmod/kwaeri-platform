/**
 * package: mmod-cms
 * sub-package: controllers/settings
 * author: Richard B. Winters <a href="mailto:rik@mmogp.com">rik AT mmogp DOT com</a>
 * copyright: 2011-2015 Massively Modified, Inc.
 * license: See Terms and EULA at https://mmogp.com/about/terms
 */


/**
 * Constructor
 *
 * @since 0.0.1
 */
function settingsController()
{
}

//HTTP GET /extensions/mmod-cms/settings
settingsController.prototype.index = function( request, response )
{
  var na = this;

  // Here we're going to want to use our database provider, when we do so
  // we'll define a callback to send along with our query
  // to support an implicit asynchronicity.
  var settingsModel = require( '../models/settings' ).settings,
  model = this.modl.set( settingsModel );

  this.klay.model = settingsModel;

  // We just need to display a message here
  this.klay.viewbag.pagetitle = 'settings';

  // Here we define the callback for our list method
  var callback = function( req, res, settings, tk )
  {
      if( settings )
       {
          if( !settings.length > 0 )
          {
              console.log( 'We were unable to get a list of settings.' );
              na.rendr( req, res );
          }
          else
          {
              //console.log( 'We were able to get a list of settings.' );
              //console.log( settings );
              na.klay.viewbag.settings = settings;

              na.rendr( req, res );
          }
      }
      else
      {
          console.log( "Settings not returned" );
          na.klay.viewbag.settings = [];

          na.rendr( req, res );
      }
  };

  model.listAll( request, response, callback, this.klay );
};


//HTTP POST /extensions/mmod-cms/settings
settingsController.prototype.indexPOST = function( request, response )
{
    var na = this;

    // Here we're going to want to use our database provider, when we do so
    // we'll define a callback to send along with our query
    // to support an implicit asynchronicity.
    var settingsModel = require( '../models/settings' ).settings,
    model = this.modl.set( settingsModel );

    this.klay.model = settingsModel;

    // We just need to display a message here
    this.klay.viewbag.pagetitle = 'settings';

    // Here we define the callback for our list method
    var callback = function( req, res, settings, tk )
    {
        if( settings )
         {
            if( !settings.length > 0 )
            {
                console.log( 'We were unable to get a list of settings.' );
                na.rendr( req, res );
            }
            else
            {
                //console.log( 'We were able to get a list of settings.' );
                //console.log( settings );
                na.klay.viewbag.settings = settings;

                na.rendr( req, res );
            }
        }
        else
        {
            console.log( "Settings not returned" );
            na.klay.viewbag.settings = [];

            na.rendr( req, res );
        }
    };

    model.saveAll( request, response, callback, this.klay );
};

// Export
module.exports = exports = settingsController;