/**
 * package: mmod-cms
 * sub-package: controllers/menuItems
 * author: Richard B. Winters <a href="mailto:rik@mmogp.com">rik AT mmogp DOT com</a>
 * copyright: 2011-2015 Massively Modified, Inc.
 * license: See Terms and EULA at https://mmogp.com/about/terms
 */


// Deps
var nk = require( 'nk' ),
nk = new nk();  // When we do not pass an argument to the constructor, we
                // get only the core facilities ( .type, .extend, .each,
                // .hash, etc )


/**
 * Constructor
 *
 * @since 0.0.1
 */
function menuItemsController()
{
}

// HTTP GET /extensions/mmod-cms/menu-items
menuItemsController.prototype.index = function( request, response )
{
    var na = this;

    var menuItemsModel = require( '../models/menu-items' ).menuItems,
    model = this.modl.set( menuItemsModel );

    this.klay.model = require( '../models/menu-items' ).listView;

    // We just need to display a message here
    this.klay.viewbag.pagetitle = 'Menu Items';

    // Here we define the callback for our list method
    var callback = function( req, res, tk )
    {
        na.rendr( req, res );
    };

    model.listAll( request, response, callback, this.klay );
};


//HTTP GET /extensions/mmod-cms/menus/add
menuItemsController.prototype.add = function( request, response )
{
    var na = this;

    var menuItemsModel = require( '../models/menu-items' ).addView,
    model = this.modl.set( menuItemsModel );

    this.klay.model = menuItemsModel;
    this.klay.viewbag.pagetitle = 'Add a New Menu Item';
    this.klay.viewbag.menuItem = { id: '', type: 1, acl: 1, title: '', description: '', parent: 1, enabled: '', options: '' };

    // Here we define the callback for our method
    var callback = function( req, res, query_good, tk )
    {
        if( !query_good )
        {
            console.log( "Menu item input field options were not returned" );
        }

        na.rendr( req, res );
    };

    model.getMenuItemInputFieldOptions( request, response, callback, this.klay );
};


//HTTP POST /extensions/mmod-cms/menu-items/add
menuItemsController.prototype.addPost = function( request, response )
{
    var na = this;

    var menuItemsModel = require( '../models/menu-items' ).addView,
    model = this.modl.set( menuItemsModel );

    this.klay.model = menuItemsModel;
    this.klay.viewbag.pagetitle = 'Add a New Menu Item';
    this.klay.viewbag.menuItem = { id: '', type: 1, acl: 1, title: '', description: '', parent: 1, enabled: 1, options: '{}' };

    // Here we define the callback for our list method
    var callback = function( req, res, tk )
    {
        na.rendr( req, res );
    };

    model.addMenuItem( request, response, callback, this.klay );
};


//HTTP GET /extensions/mmod-cms/menu-items/edit


//HTTP GET /extensions/mmod-cms/menu-items/edit
menuItemsController.prototype.edit = function( request, response )
{
    var na = this;

    var menuItemsModel = require( '../models/menu-items' ).editView,
    model = this.modl.set( menuItemsModel );

    this.klay.model = menuItemsModel;

    // We just need to display a message here
    this.klay.viewbag.pagetitle = 'Menu Items';

     // Here we define the callback for our list method
     var callback = function( req, res, tk )
     {
         if( tk.viewbag.menuItem && !nk.isEmptyObject( tk.viewbag.menuItem ) )
         {
             var tmp = '';
             var options = JSON.parse( tk.viewbag.menuItem.options );
             for( var option in options )
             {
                 tmp += '[' + option + ': ' + options[option] + '] ';
             }
             tk.viewbag.menuItem.options = tmp;

             // There can only be one menu to edit at a time ofc...
             na.klay.viewbag.menuItem = tk.viewbag.menuItem;
        }
        else
        {
            console.log( "Menu not returned" );
            na.klay.viewbag.menuItem = [];
        }

         na.rendr( req, res );
    };

    model.getMenuItem( request, response, callback, this.klay );
};


//HTTP POST /extensions/mmod-cms/menu-items/edit
menuItemsController.prototype.editPost = function( request, response )
{
    var na = this;

    var menuItemsModel = require( '../models/menu-items' ).editView,
    model = this.modl.set( menuItemsModel );

    this.klay.model = menuItemsModel;

    // We just need to display a message here
    this.klay.viewbag.pagetitle = 'Menu Items';

     // Here we define the callback for our list method
     var callback = function( req, res, tk )
     {
         if( tk.viewbag.menuItem && !nk.isEmptyObject( tk.viewbag.menuItem ) )
         {
             var tmp = '';
             if( tk.viewbag.menuItem.options )
             {
                 var options = JSON.parse( tk.viewbag.menuItem.options );
                 for( var option in options )
                 {
                     tmp += '[' + option + ': ' + options[option] + '] ';
                 }
                 tk.viewbag.menuItem.options = tmp;
             }

             na.klay.viewbag.menuItem = tk.viewbag.menuItem;
        }
        else
        {
            console.log( 'Menu Item not returned' );
        }

        na.rendr( req, res );
    };

    model.editMenuItem( request, response, callback, this.klay );
};


//HTTP POST /extensions/mmod-cms/menu-items/copy


//HTTP POST /extensions/mmod-cms/menu-items/copy
menuItemsController.prototype.copyPost = function( request, response )
{
    var na = this;

    var menuItemsModel = require( '../models/menu-items' ).copyView,
    model = this.modl.set( menuItemsModel );

    this.klay.model = menuItemsModel;

    // We just need to display a message here
    this.klay.viewbag.pagetitle = 'Menu Items';

    // Here we define the callback for our list method
    var callback = function( req, res, tk )
    {
        na.rendr( req, res );
    };

    model.copyMenuItem( request, response, callback, this.klay );
};


//HTTP POST /extensions/mmod-cms/menu-items/delete


//HTTP POST /extensions/mmod-cms/menu-items/delete


//HTTP POST /extensions/mmod-cms/menu-items/delete
menuItemsController.prototype.deletePost = function( request, response )
{
    var na = this;

    var menuItemsModel = require( '../models/menu-items' ).deleteView,
    model = this.modl.set( menuItemsModel );

    this.klay.model = menuItemsModel;

    // We just need to display a message here
    this.klay.viewbag.pagetitle = 'Menu Items';

    // Here we define the callback for our list method
    var callback = function( req, res, tk )
    {
        na.rendr( req, res );
    };

    model.deleteMenuItem( request, response, callback, this.klay );
};


// Export
module.exports = exports = menuItemsController;