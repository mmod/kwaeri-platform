/**
 * package: mmod-cms
 * sub-package: controllers/articles
 * author: Richard B. Winters <a href="mailto:rik@mmogp.com">rik AT mmogp DOT com</a>
 * copyright: 2011-2015 Massively Modified, Inc.
 * license: See Terms and EULA at https://mmogp.com/about/terms
 */


// Deps
var nk = require( 'nk' ),
nk = new nk();  // When we do not pass an argument to the constructor, we
                // get only the core facilities ( .type, .extend, .each,
                // .hash, etc )


/**
 * Constructor
 *
 * @since 0.0.1
 */
function articlesController()
{
}

// HTTP GET /extensions/mmod-cms/articles
articlesController.prototype.index = function( request, response )
{
    var na = this;

    var articlesModel = require( '../models/articles' ).articles,
    model = this.modl.set( articlesModel );

    this.klay.model = require( '../models/articles' ).listView;

    // We just need to display a message here
    this.klay.viewbag.pagetitle = 'Articles';

    // Here we define the callback for our list method
    var callback = function( req, res, articles, tk )
    {
        na.rendr( req, res );
    };

    model.listAll( request, response, callback, this.klay );
};


//HTTP GET /extensions/mmod-cms/articles/add
articlesController.prototype.add = function( request, response )
{
    var na = this;

    var articlesModel = require( '../models/articles' ).addView,
    model = this.modl.set( articlesModel );

    this.klay.model = articlesModel;
    this.klay.viewbag.pagetitle = 'Add a New Article';
    this.klay.viewbag.article = { id: '', type: '', acl: '', category: '', tags: '', title: '', alias: '', description: '', content: '', keywords: '', options: '' };

    // Here we define the callback for our method
    var callback = function( req, res, query_good, tk )
    {
        if( !query_good )
        {
            console.log( "Article input field options were not returned" );
        }

        na.rendr( req, res );
    };

    model.getArticleInputFieldOptions( request, response, callback, this.klay );
};


//HTTP POST /extensions/mmod-cms/articles/add
articlesController.prototype.addPost = function( request, response )
{
    var na = this;

    var articlesModel = require( '../models/articles' ).addView,
    model = this.modl.set( articlesModel );

    this.klay.model = articlesModel;
    this.klay.viewbag.pagetitle = 'Add a New Article';
    this.klay.viewbag.article = { id: '', type: '', acl: '', category: '', tags: '', title: '', alias: '', description: '', content: '', keywords: '', options: '' };

    // Here we define the callback for our list method
    var callback = function( req, res, tk )
    {
        na.rendr( req, res );
    };

    model.addArticle( request, response, callback, this.klay );
};


//HTTP GET /extensions/mmod-cms/articles/edit
articlesController.prototype.edit = function( request, response )
{
    var na = this;

    var articlesModel = require( '../models/articles' ).editView,
    model = this.modl.set( articlesModel );

    this.klay.model = articlesModel;

    // We just need to display a message here
    this.klay.viewbag.pagetitle = 'Articles';

     // Here we define the callback for our list method
     var callback = function( req, res, tk )
     {
         if( tk.viewbag.article && !nk.isEmptyObject( tk.viewbag.article ) )
         {
             var tmp = '';
             var tags = JSON.parse( tk.viewbag.article.tags );
             for( var tag in tags )
             {
                 if( tag == tags.length - 1 )
                 {
                     tmp += tags[tag];
                 }
                 else
                 {
                     tmp += tags[tag] + ', ';
                 }
             }
             tk.viewbag.article.tags = tmp;

             tmp = '';
             var keywords = JSON.parse( tk.viewbag.article.keywords );
             for( var keyword in keywords )
             {
                 if( keyword == keywords.length - 1 )
                 {
                     tmp += keywords[keyword];
                 }
                 else
                 {
                     tmp += keywords[keyword] + ', ';
                 }
             }
             tk.viewbag.article.keywords = tmp;

             tmp = '';
             var options = JSON.parse( tk.viewbag.article.options );
             for( var option in options )
             {
                 tmp += '[' + option + ': ' + options[option] + '] ';
             }
             tk.viewbag.article.options = tmp;

             // There can only be one article to edit at a time ofc...
             na.klay.viewbag.article = tk.viewbag.article;
        }
        else
        {
            console.log( "Article not returned" );
            na.klay.viewbag.article = [];
        }

         na.rendr( req, res );
    };

    model.getArticle( request, response, callback, this.klay );
};


//HTTP POST /extensions/mmod-cms/articles/edit
articlesController.prototype.editPost = function( request, response )
{
    var na = this;

    var articlesModel = require( '../models/articles' ).editView,
    model = this.modl.set( articlesModel );

    this.klay.model = articlesModel;

    // We just need to display a message here
    this.klay.viewbag.pagetitle = 'Articles';

     // Here we define the callback for our list method
     var callback = function( req, res, tk )
     {
         if( tk.viewbag.article && !nk.isEmptyObject( tk.viewbag.article ) )
         {
             var tmp = '';
             if( tk.viewbag.article.tags )
             {
                 var tags = JSON.parse( tk.viewbag.article.tags );
                 for( var tag in tags )
                 {
                     if( tag == tags.length - 1 )
                     {
                         tmp += tags[tag];
                     }
                     else
                     {
                         tmp += tags[tag] + ', ';
                     }
                 }
                 tk.viewbag.article.tags = tmp;
             }

             tmp = '';
             if( tk.viewbag.article.keywords )
             {
                 var keywords = JSON.parse( tk.viewbag.article.keywords );
                 for( var keyword in keywords )
                 {
                     if( keyword == keywords.length - 1 )
                     {
                         tmp += keywords[keyword];
                     }
                     else
                     {
                         tmp += keywords[keyword] + ', ';
                     }
                 }
                 tk.viewbag.article.keywords = tmp;
             }

             tmp = '';
             if( tk.viewbag.article.options )
             {
                 var options = JSON.parse( tk.viewbag.article.options );
                 for( var option in options )
                 {
                     tmp += '[' + option + ': ' + options[option] + '] ';
                 }
                 tk.viewbag.article.options = tmp;
             }

             na.klay.viewbag.article = tk.viewbag.article;
        }
        else
        {
            console.log( 'Article not returned' );
        }

        na.rendr( req, res );
    };

    model.editArticle( request, response, callback, this.klay );
};


//HTTP POST /extensions/mmod-cms/articles/copy
articlesController.prototype.copyPost = function( request, response )
{
    var na = this;

    var articlesModel = require( '../models/articles' ).copyView,
    model = this.modl.set( articlesModel );

    this.klay.model = articlesModel;

    // We just need to display a message here
    this.klay.viewbag.pagetitle = 'Articles';

    // Here we define the callback for our list method
    var callback = function( req, res, tk )
    {
        na.rendr( req, res );
    };

    model.copyArticle( request, response, callback, this.klay );
};


//HTTP POST /extensions/mmod-cms/articles/delete
articlesController.prototype.deletePost = function( request, response )
{
    var na = this;

    var articlesModel = require( '../models/articles' ).deleteView,
    model = this.modl.set( articlesModel );

    this.klay.model = articlesModel;

    // We just need to display a message here
    this.klay.viewbag.pagetitle = 'Articles';

    // Here we define the callback for our list method
    var callback = function( req, res, tk )
    {
        na.rendr( req, res );
    };

    model.deleteArticle( request, response, callback, this.klay );
};


// Export
module.exports = exports = articlesController;