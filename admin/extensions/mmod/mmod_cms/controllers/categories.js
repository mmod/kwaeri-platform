/**
 * package: mmod-cms
 * sub-package: controllers/categories
 * author: Richard B. Winters <a href="mailto:rik@mmogp.com">rik AT mmogp DOT com</a>
 * copyright: 2011-2015 Massively Modified, Inc.
 * license: See Terms and EULA at https://mmogp.com/about/terms
 */


/**
 * Constructor
 *
 * @since 0.0.1
 */
function categoriesController()
{
}

// HTTP GET /extensions/mmod-cms/categories
categoriesController.prototype.index = function( request, response )
{
    var na = this;

    var categoriesModel = require( '../models/categories' ).categories,
    model = this.modl.set( categoriesModel );

    this.klay.model = require( '../models/categories' ).listView;

    // We just need to display a message here
    this.klay.viewbag.pagetitle = 'Categories';

    // Here we define the callback for our list method
    var callback = function( req, res, tk )
    {
        na.rendr( req, res );
    };

    model.listAll( request, response, callback, this.klay );
};


//HTTP GET /extensions/mmod-cms/categories/add
categoriesController.prototype.add = function( request, response )
{
    var na = this;

    var categoriesModel = require( '../models/categories' ).addView,
    model = this.modl.set( categoriesModel );

    this.klay.model = categoriesModel;

    // We just need to display a message here
    this.klay.viewbag.pagetitle = 'Add a New category';
    this.klay.viewbag.category = {};

    // Here we define the callback for our list method
    var callback = function( req, res, query_good, tk )
    {
        if( !query_good )
        {
            console.log( "Menu input field options were not returned" );
        }
        na.rendr( req, res );
    };

    model.getCategoryInputFieldOptions( request, response, callback, this.klay );
};


//HTTP POST /extensions/mmod-cms/categories/add
categoriesController.prototype.addPost = function( request, response )
{
    var na = this;

    var categoriesModel = require( '../models/categories' ).addView,
    model = this.modl.set( categoriesModel );

    this.klay.model = categoriesModel;

    // We just need to display a message here
    this.klay.viewbag.pagetitle = 'Add a New category';
    this.klay.viewbag.category = {};

    // Here we define the callback for our list method
    var callback = function( req, res, tk )
    {
        na.rendr( req, res );
    };

    model.addCategory( request, response, callback, this.klay );
};


//HTTP GET /extensions/mmod-cms/categories/edit
categoriesController.prototype.edit = function( request, response )
{
    var na = this;

    var categoriesModel = require( '../models/categories' ).editView,
    model = this.modl.set( categoriesModel );

    this.klay.model = categoriesModel;

    // We just need to display a message here
    this.klay.viewbag.pagetitle = 'Categories';

    // Here we define the callback for our list method
    var callback = function( req, res, tk )
    {
        na.rendr( req, res );
    };

    model.getCategory( request, response, callback, this.klay );
};


//HTTP POST /extensions/mmod-cms/categories/edit
categoriesController.prototype.editPost = function( request, response )
{
    var na = this;

    var categoriesModel = require( '../models/categories' ).editView,
    model = this.modl.set( categoriesModel );

    this.klay.model = categoriesModel;

    // We just need to display a message here
    this.klay.viewbag.pagetitle = 'Categories';

    // Here we define the callback for our edit method
    var callback = function( req, res, tk )
    {
        na.rendr( req, res );
    };

    model.editCategory( request, response, callback, this.klay );
};


//HTTP POST /extensions/mmod-cms/categories/copy
categoriesController.prototype.copyPost = function( request, response )
{
    var na = this;

    var categoriesModel = require( '../models/categories' ).copyView,
    model = this.modl.set( categoriesModel );

    this.klay.model = categoriesModel;

    // We just need to display a message here
    this.klay.viewbag.pagetitle = 'Categories';

    // Here we define the callback for our list method
    var callback = function( req, res, tk )
    {
        na.rendr( req, res );
    };

    model.copyCategory( request, response, callback, this.klay );
};


//HTTP POST /extensions/mmod-cms/categories/delete
categoriesController.prototype.deletePost = function( request, response )
{
    var na = this;

    var categoriesModel = require( '../models/categories' ).deleteView,
    model = this.modl.set( categoriesModel );

    this.klay.model = categoriesModel;

    // We just need to display a message here
    this.klay.viewbag.pagetitle = 'Categories';

    // Here we define the callback for our list method
    var callback = function( req, res, tk )
    {
        na.rendr( req, res );
    };

    model.deleteCategory( request, response, callback, this.klay );
};


// Export
module.exports = exports = categoriesController;