CREATE TABLE IF NOT EXISTS `mmdev`.`mmod-cms-settings`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `ts` DATETIME NULL,
  `luts` DATETIME NULL,
  `luby` INT NULL,
  `article_settings` TEXT NOT NULL,
  `blog_settings` TEXT NOT NULL,
  `social_settings` TEXT NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=NDBCLUSTER;

INSERT INTO `mmdev`.`mmod-cms-settings`
( `ts`, `luts`, `luby`, `article_settings`, `blog_settings`, `social_settings` )
VALUES
( NOW(), NOW(), 0, '{"tags_enabled":true,"comments_enabled":true}', '{"tags_enabled":true,"comments_enabled":true}', '{}' );


CREATE TABLE IF NOT EXISTS `mmdev`.`mmod-cms-articles`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `type` INT NULL,
  `published` INT NULL,
  `acl` INT NULL,
  `ts` DATETIME NULL,
  `owner` INT NULL,
  `luts` DATETIME NULL,
  `luby` INT NULL,
  `category` INT NULL,
  `tags` TEXT NULL,
  `title` VARCHAR(255) NULL,
  `alias` TEXT NULL,
  `description` TEXT NULL,
  `content` TEXT NULL,
  `keywords` TEXT NULL,
  `options` TEXT NULL,
  PRIMARY KEY (`id`)
) ENGINE=NDBCLUSTER;

INSERT INTO `mmdev`.`mmod-cms-articles`
( `type`, `published`, `acl`, `ts`, `owner`, `luts`, `luby`, `category`, `tags`, `title`, `alias`,
  `description`,
  `content`,
  `keywords`, `options` )
VALUES
( 
  0, 1, 0, NOW(), 0, NOW(), 0, 1, '["site news","featured"]', 'MMod CMS for MMod XRM Has Arrived!', 'mmod-cms-for-mmod-xrm-has-arrived',
  'Your very first article, and marked featured at that!',
  'MMod CMS is here and its packed full of features for you to enjoy. Manage all aspects of your front-facing application, from this to that, and more!',
  '["first article","first","article","mmod-cms","cms","mmod-xrm","xrm","mmod,"]', '{"allow_comments":true}'
),
( 
  0, 1, 0, NOW(), 0, NOW(), 0, 2, '["site blog","hot topic"]', 'Check out our Massively Modified Blog!', 'check-out-our-massively-modified-blog',
  'Our very first blog, that&apos;s hot!',
  '<p>MMod CMS is here and it&apos;s packed full of features for you to enjoy. Manage all aspects of your front-facing application, from this to that, and more!</p><p></p><p>This is your new blog, why not dress it up a bit?</p>',
  '["first blog","first","blog","mmod-cms","cms","mmod-xrm","xrm","mmod,"]', '{"allow_comments":true,"social_forwarding":{"enabled":true}}'
),
( 
  0, 1, 0, NOW(), 0, NOW(), 0, 0, '["support","help desk","contact us"]', 'Contact Us', 'contact-us',
  'Contact Us',
  '<p>We work hard at taking - and viewing - consideration of your feedback very carefully, and/or importantly, respectively. Please use on of the options below should you need to get in touch with us!</p><p></p><p>IRC: #mmod on freenode.net.</p>',
  '["contact us","contact","us","support","help desk","help","mmod,"]', '{"allow_comments":false}'
);


CREATE TABLE IF NOT EXISTS `mmdev`.`mmod-cms-article-types`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `acl` INT NULL,
  `ts` DATETIME NULL,
  `luts` DATETIME NULL,
  `luby` INT NULL,
  `title` VARCHAR(255) NULL,
  `description` TEXT NULL,
  `parent` INT NULL,
  `enabled` INT NULL,
  `options` TEXT NULL,
  PRIMARY KEY (`id`)
) ENGINE=NDBCLUSTER;

INSERT INTO `mmdev`.`mmod-cms-article-types`
( `acl`, `ts`, `luts`, `luby`, `title`, `description`, `parent`, `enabled`, `options` )
VALUES
( 0, NOW(), NOW(), 0, 'Article', 'A type which can be utilized for many purposes. A generalized page artifact.', 0, 1, '{}' ),
( 0, NOW(), NOW(), 0, 'Fora', 'A type which can be utilized for building locked and/or open communities in blog or forum fashion.', 0, 1, '{}' );


CREATE TABLE IF NOT EXISTS `mmdev`.`mmod-cms-categories`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `acl` INT NULL,
  `ts` DATETIME NULL,
  `luts` DATETIME NULL,
  `luby` INT NULL,
  `title` VARCHAR(255) NULL,
  `description` TEXT NULL,
  `parent` INT NULL,
  `enabled` INT NULL,
  PRIMARY KEY (`id`)
) ENGINE=NDBCLUSTER;

INSERT INTO `mmdev`.`mmod-cms-categories`
( `acl`, `ts`, `luts`, `luby`, `title`, `description`, `parent` )
VALUES
( 0, NOW(), NOW(), 0, '', '', 0 ),
( 0, NOW(), NOW(), 0, 'news', 'Category for news', 0 ),
( 0, NOW(), NOW(), 0, 'fora', 'Category for fora', 0 );


CREATE TABLE IF NOT EXISTS `mmdev`.`mmod-cms-menus`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `ts` DATETIME NULL,
  `luts` DATETIME NULL,
  `luby` INT NULL,
  `title` VARCHAR(255) NULL,
  `description` TEXT NULL,
  `enabled` INT NULL,
  `options` TEXT NULL,
  PRIMARY KEY (`id`)
) ENGINE=NDBCLUSTER;

INSERT INTO `mmdev`.`mmod-cms-menus`
( `ts`, `luts`, `luby`, `title`, `description`, `enabled`, `options` )
VALUES
( NOW(), NOW(), 0, 'Main', 'The application menu', 1, '{}' );


CREATE TABLE IF NOT EXISTS `mmdev`.`mmod-cms-menu-item-types`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `acl` INT NULL,
  `ts` DATETIME NULL,
  `luts` DATETIME NULL,
  `luby` INT NULL,
  `title` VARCHAR(255) NULL,
  `description` TEXT NULL,
  `parent` INT NULL,
  `enabled` INT NULL,
  `options` TEXT NULL,
  PRIMARY KEY (`id`)
) ENGINE=NDBCLUSTER;

INSERT INTO `mmdev`.`mmod-cms-menu-item-types`
( `acl`, `ts`, `luts`, `luby`, `title`, `description`, `parent`, `enabled`, `options` )
VALUES
( 0, NOW(), NOW(), 0, 'Alias', 'A menu type which provides a link to another location (i.e. to another alias, or any URL).', 0, 1, '{}' ),
( 0, NOW(), NOW(), 0, 'Fora', 'A menu type which can be utilized for rendering fora content.', 0, 1, '{}' );
( 0, NOW(), NOW(), 0, 'Page', 'A menu type which can be utilized for rendering article content.', 0, 1, '{}' );


CREATE TABLE IF NOT EXISTS `mmdev`.`mmod-cms-menu-items`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `type` INT NULL,
  `acl` INT NULL,
  `ts` DATETIME NULL,
  `luts` DATETIME NULL,
  `luby` INT NULL,
  `title` VARCHAR(255) NULL,
  `description` TEXT NULL,
  `menu` INT NULL,
  `parent` INT NULL,
  `enabled` INT NULL,
  `options` TEXT NULL,
  PRIMARY KEY (`id`)
) ENGINE=NDBCLUSTER;

INSERT INTO `mmdev`.`mmod-cms-menu-items`
( `type`, `acl`, `ts`, `luts`, `luby`, `title`, `description`, `parent`, `enabled`, `options` )
VALUES
( 1, 0, NOW(), NOW(), 0, 'Home', 'Alias for the base domain.', 1, 1, '{}' ),
( 1, 0, NOW(), NOW(), 0, 'Products', 'Products menu and link to products page.', 1, 1, '{"content_type":"article"}' ),
( 1, 0, NOW(), NOW(), 0, 'Services', 'Services menu and link to services page.', 1, 1, '{"content_type":"article"}' ),
( 1, 0, NOW(), NOW(), 0, 'Documentation', 'Documentation menu and link to documentation page.', 1, 1, '{"content_type":1}' ),
( 1, 0, NOW(), NOW(), 0, 'Fora', 'Fora menu and link to fora page.', 1, 1, '{"content_type":2}' );


CREATE TABLE `mmpro`.`mmod-cms-settings` LIKE `mmdev`.`mmod-cms-settings`;
INSERT INTO `mmpro`.`mmod-cms-settings` SELECT * FROM `mmdev`.`mmod-cms-settings`;

CREATE TABLE `mmpro`.`mmod-cms-articles` LIKE `mmdev`.`mmod-cms-articles`;
INSERT INTO `mmpro`.`mmod-cms-articles` SELECT * FROM `mmdev`.`mmod-cms-articles`;

CREATE TABLE `mmpro`.`mmod-cms-article-types` LIKE `mmdev`.`mmod-cms-article-types`;
INSERT INTO `mmpro`.`mmod-cms-article-types` SELECT * FROM `mmdev`.`mmod-cms-article-types`;

CREATE TABLE `mmpro`.`mmod-cms-categories` LIKE `mmdev`.`mmod-cms-categories`;
INSERT INTO `mmpro`.`mmod-cms-categories` SELECT * FROM `mmdev`.`mmod-cms-categories`;

CREATE TABLE `mmpro`.`mmod-cms-menus` LIKE `mmdev`.`mmod-cms-menus`;
INSERT INTO `mmpro`.`mmod-cms-menus` SELECT * FROM `mmdev`.`mmod-cms-menus`;

CREATE TABLE `mmpro`.`mmod-cms-menu-item-types` LIKE `mmdev`.`mmod-cms-menu-item-types`;
INSERT INTO `mmpro`.`mmod-cms-menu-item-types` SELECT * FROM `mmdev`.`mmod-cms-menu-item-types`;

CREATE TABLE `mmpro`.`mmod-cms-menu-items` LIKE `mmdev`.`mmod-cms-menu-items`;
INSERT INTO `mmpro`.`mmod-cms-menu-items` SELECT * FROM `mmdev`.`mmod-cms-menu-items`;
