/**
 * package: mmod-cms
 * sub-package: models/articles
 * author: Richard B. Winters <a href="mailto:rik@mmogp.com">rik AT mmogp DOT com</a>
 * copyright: 2011-2015 Massively Modified, Inc.
 * license: See Terms and EULA at https://mmogp.com/about/terms
 */


// Deps
var nk = require( 'nk' ),
nk = new nk();  // When we do not pass an argument to the constructor, we
                // get only the core facilities ( .type, .extend, .each,
                // .hash, etc )


/**
 * Defines the context model
 *
 * @since 0.0.1
 */
var articlesModel =
{
    articles:
    {
        listAll: function( request, response, callback, klay )
        {
            // Prep the database object
            var db = this.dbo();

            // Rid the DBO of any stored data.
            var model = klay.model.schema;
            db.reset( model );

            // Get a list of all articles using a prepared statement
            var articles = db
            .select( "`mmod-cms-articles`" )
            .execute();

            if( nk.type( articles ) === 'array' )
            {
                // Great, we have an array which should be full of user records
                klay.viewbag.publishedTypes = [ 'Yes', 'No' ];
                for( var article in articles )
                {
                    articles[article].published = klay.viewbag.publishedTypes[articles[article].published];
                    var tmp = '';
                    var tags = JSON.parse( articles[article].tags );
                    for( var tag in tags )
                    {
                        tmp += '<span class="badge tag">' + tags[tag] + '</span> ';
                    }
                    articles[article].tags = tmp;
                }
                klay.viewbag.articles = articles;
            }
            else
            {
                console.log( 'Failed to get a list of articles...' );
                klay.viewbag.articles = {};
            }

            // And invoke the callback, passing the result of our query
            if( nk.type( callback ) === 'function' )
            { // Asynchronous
                callback( request, response, articles, klay );
            }
            else
            { // Synchronous
                return articles;
            }
        },
        schema:
        {
            /*
             * Article Model
             */
            id: [ true, 'int', ''],
            type: [ true, 'int', 'Type' ],
            published: [ true, 'int', 'Published' ],
            acl: [ true, 'int', 'ACL' ],
            ts: [ true, 'text', 'Created' ],
            owner: [ true, 'int', 'Owner' ],
            luts: [ true, 'text', 'Last Updated' ],
            luby: [ true, 'text', 'Last Updated by' ],
            category: [ true, 'int', 'Category' ],
            tags: [true, 'text', 'Tags' ],
            title: [ true, 'text', 'Title' ],
            alias: [ true, 'text', 'Alias' ],
            description: [ true, 'text', 'Description' ],
            content: [ true, 'text', 'Article Content' ],
            keywords: [ true, 'text', 'Keywords' ],
            options: [ true, 'text', 'Options' ]
        }
    },
    listView:
    {
        schema:
        {
            /*
             * Article/list View model
             */
            id: [ true, 'int', ''],
            type: [ true, 'int', 'Type' ],
            published: [ true, 'int', 'Published' ],
            acl: [ true, 'int', 'ACL' ],
            ts: [ true, 'text', 'Created' ],
            owner: [ true, 'int', 'Author' ],
            luts: [ true, 'text', 'Last Updated' ],
            luby: [ true, 'text', 'Last Updated by' ],
            category: [ true, 'int', 'Category' ],
            tags: [true, 'text', 'Tags' ],
            title: [ true, 'text', 'Title' ],
            alias: [ true, 'text', 'Alias' ],
            description: [ true, 'text', 'Description' ],
            content: [ true, 'text', 'Article Content' ],
            keywords: [ true, 'text', 'Keywords' ],
            options: [ true, 'text', 'Options' ]
        }
    },
    addView:
    {
        getArticleInputFieldOptions: function( request, response, callback, klay )
        {
            var query_good = true;

            // Prep the database object
            var db = this.dbo();

            // Prep the model for the database object
            var model = false;
            db.reset( model );

            var articleTypes = false;
            articleTypes = db
            .select( "* from `mmod-cms-article-types`" )
            .execute();

            model = require( './categories' ).addView.schema;
            db.reset( model );

            var categories = false;
            categories = db
            .select( "`mmod-cms-categories`" )
            .execute();

            model = false;
            db.reset( model );

            var acls = false;
            acls = db
            .select( "* from acls" )
            .execute();

            db.reset( model );

            if( nk.type( articleTypes ) === 'array' )
            {
                var tmpArticleTypes = [];
                for( var i = 0; i < articleTypes.length; i++ )
                {
                    tmpArticleTypes[articleTypes[i].id] = articleTypes[i].title;
                }

                klay.viewbag.articleTypes = tmpArticleTypes;
            }
            else
            {
                query_good = false;
                console.log( 'Failed to get the article types...' );
                klay.viewbag.articleTypes = {};
            }

            klay.viewbag.publishedTypes = [ klay.viewbag.article.published, 'Yes', 'No' ];

            if( nk.type( categories ) === 'array' )
            {
                var tmp = [];
                for( var i = 0; i < categories.length; i++ )
                {
                    tmp[categories[i].id] = categories[i].title;
                }

                klay.viewbag.categories = tmp;
            }
            else
            {
                query_good = false;
                console.log( 'Failed to get the categories...' );
                klay.viewbag.categories = {};
            }

            if( nk.type( acls ) === 'array' )
            {
                var tmp = [];
                for( var i = 0; i < acls.length; i++ )
                {
                    tmp[acls[i].id] = acls[i].name;
                }

                klay.viewbag.articleACLs = tmp;
            }
            else
            {
                query_good = false;
                console.log( 'Failed to get the acls...' );
                klay.viewbag.articleACLs = {};
            }

            // And invoke the callback, passing the result of our query
            if( nk.type( callback ) === 'function' )
            { // Asynchronous
                callback( request, response, query_good, klay );
            }
            else
            { // Synchronous
                return query_good;
            }
        },
        addArticle: function( request, response, callback, klay )
        {
            var mdata;
            if( request.posted.mdata )
            {
                // Store data so it can be manipulated more easily by other components which will
                // experience higher usage/traffic than this here article creation process (i.e.
                // the traffic of end users reading the articles). We'll only do this where applicable.
                mdata = JSON.parse( request.posted.mdata );
                if( !mdata.tags || mdata.tags === '' )
                {
                    mdata.tags = '[]';
                }
                else
                {
                    mdata.tags = JSON.stringify( nk.split( mdata.tags ) );
                }
                if( !mdata.keywords || mdata.keywords === '' )
                {
                    mdata.keywords = '[]';
                }
                else
                {
                    mdata.keywords = JSON.stringify( nk.split( mdata.keywords ) );
                }
                if( !mdata.options || mdata.options === '' )
                {
                    mdata.options = '{"allow_comments":false,"social_forwarding":{"enabled":false}}';
                }
            }

            // We need to reference the user with this one.
            var user = klay.viewbag.user;

            // We also want to display the article edit view after adding this here new article
            var article = false;

            // Prep the database object
            var db = this.dbo();

            // Prep the model for the database object
            var model = false;
            db.reset( false );

            // We store the user by Id, not by name
            var whereVals =
            {
                username: [ '=', user.username ],
                email: [ '=', user.email ]
            };

            var userId = db
            .select( "id from users" )
            .where( whereVals )
            .execute();

            // Extract the user's Id
            if( nk.type( userId ) === 'array' )
            {
                userId = userId[0].id;
            }

            model = klay.model.schema;
            delete model.id;
            db.reset( model );

            // Add our article now that we have the information to store it correctly
            var values =
            {
                type: mdata.type,
                published: mdata.published,
                acl: mdata.acl,
                ts: new Date().toISOString().slice(0, 19).replace('T', ' '),
                owner: userId,
                luts: new Date().toISOString().slice(0, 19).replace('T', ' '),
                luby: userId,
                category: mdata.category,
                tags: mdata.tags,
                title: mdata.title,
                alias: mdata.alias,
                description: mdata.description,
                content: mdata.content,
                keywords: mdata.keywords,
                options: mdata.options
            };

            var added = db
            .insert( "`mmod-cms-articles`" )     // Records contains the number of rows affected.
            .values( values )
            .execute();

            model = klay.model.schema
            db.reset( model );

            if( added )
            {
                if( added > 0 )
                {
                    klay.viewbag.sysmsg = '<span class="xrm-xalert success">Article "' + mdata.title + '" added successfully.</span>';
                }
                else
                {
                    klay.viewbag.sysmsg = '<span class="xrm-xalert error">Article "' + mdata.title + '" not saved.</span>';
                }
            }
            else
            {
                klay.viewbag.sysmsg = '<span class="xrm-xalert error">Whoopsie! Something went wrong; Article "' + mdata.title + '" not saved.</span>';
            }

            // And invoke the callback, passing the result of our query
            if( nk.type( callback ) === 'function' )
            { // Asynchronous
                callback( request, response, klay );
            }
            else
            { // Synchronous
                return klay.viewbag.sysmsg;
            }
        },
        schema:
        {
            /*
             * Article/add View model
             */
            id: [ true, 'int', ''],
            type: [ true, 'int', 'Type' ],
            published: [ true, 'int', 'Published' ],
            acl: [ true, 'int', 'ACL' ],
            ts: [ true, 'text', 'Created' ],
            owner: [ true, 'int', 'Owner' ],
            luts: [ true, 'text', 'Last Updated' ],
            luby: [ true, 'text', 'Last Updated by' ],
            category: [ true, 'int', 'Category' ],
            tags: [true, 'text', 'Tags' ],
            title: [ true, 'text', 'Title' ],
            alias: [ true, 'text', 'Alias' ],
            description: [ true, 'text', 'Description' ],
            content: [ true, 'text', 'Article Content' ],
            keywords: [ true, 'text', 'Keywords' ],
            options: [ true, 'text', 'Options' ]
        }
    },
    editView:
    {
        getArticle: function( request, response, callback, klay )
        {
            var articleId = false;
            if( Object.prototype.hasOwnProperty.call( request.requrl.query, 'articleId' ) )
            {
                articleId = request.requrl.query.articleId;
            }
            else
            {
                if( Object.prototype.hasOwnProperty.call( request.requrl.query, 'recordId' ) )
                {
                    // User came here from the universal edit mechanism  on the list page
                    articleId = request.requrl.query.recordId;
                }
                else
                {
                    console.log( 'Article/Record ID not found in query string!' );
                }
            }

            var article = false;

            if( articleId )
            {
                // Prep the database object
                var db = this.dbo();

                // Prepare the model for the database
                var model = klay.model.schema;
                db.reset( model );

                // Get a list of all articles
                article = db
                .select( "`mmod-cms-articles`" )
                .where( { id: [ '=', articleId ] } )
                .execute();

                model = false;
                db.reset( false );

                var articleTypes = false;
                articleTypes = db
                .select( "* from `mmod-cms-article-types`" )
                .execute();

                model = require( './categories' ).categories.schema;
                db.reset( model );

                var categories = false;
                categories = db
                .select( "`mmod-cms-categories`" )
                .execute();

                model = false;
                db.reset( model );

                var acls = false;
                acls = db
                .select( "* from acls" )
                .execute();

                db.reset( model );
            }

            if( nk.type( article ) === 'array' && article.length > -1 )
            {
                // Great, we have an array which should be full of user records
                for( var record in article )
                {
                    article[record].content = article[record].content;
                }

                klay.viewbag.article = article[0];
                klay.viewbag.article.id = articleId;
            }
            else
            {
                console.log( 'Failed to get the article...' );
                klay.viewbag.article = {};
                klay.viewbag.article.id = articleId;
            }

            if( nk.type( articleTypes ) === 'array' && articleTypes.length > -1 )
            {
                var tmp = [];
                for( var i = 0; i < articleTypes.length; i++ )
                {
                    tmp[articleTypes[i].id] = articleTypes[i].title;
                }

                // Set the 'selected' value
                tmp[0] = tmp[klay.viewbag.article.type || 1];

                klay.viewbag.articleTypes = tmp;
            }
            else
            {
                console.log( 'Failed to get the article types...' );
                klay.viewbag.articleTypes = {};
            }

            klay.viewbag.publishedTypes = [ klay.viewbag.article.published, 'Yes', 'No' ];

            if( nk.type( categories ) === 'array' && categories.length > -1 )
            {
                var tmp = [];
                for( var i = 0; i < categories.length; i++ )
                {
                    tmp[categories[i].id] = categories[i].title;
                }

                // Set the 'selected' value
                tmp[0] = tmp[klay.viewbag.article.category];

                klay.viewbag.categories = tmp;
            }
            else
            {
                console.log( 'Failed to get the categories...' );
                klay.viewbag.categories = {};
            }

            if( nk.type( acls ) === 'array' && acls.length > -1 )
            {
                var tmp = [];
                for( var i = 0; i < acls.length; i++ )
                {
                    tmp[acls[i].id] = acls[i].name;
                }

                // Set the 'selected' value
                tmp[0] = tmp[klay.viewbag.article.acl];

                klay.viewbag.articleACLs = tmp;
            }
            else
            {
                console.log( 'Failed to get the acls...' );
                klay.viewbag.articleACLs = {};
            }

            // And invoke the callback, passing the result of our query
            if( nk.type( callback ) === 'function' )
            { // Asynchronous
                callback( request, response, klay );
            }
            else
            { // Synchronous
                return;
            }
        },
        editArticle: function( request, response, callback, klay )
        {
            var mdata = null;
            if( request.posted.mdata )
            {
                // Store data so it can be manipulated more easily by other components which will
                // experience higher usage/traffic than this here article creation process (i.e.
                // the traffic of end users reading the articles). We'll only do this where applicable.
                mdata = JSON.parse( request.posted.mdata );
                if( !mdata.tags || mdata.tags === '' )
                {
                    mdata.tags = '[]';
                }
                else
                {
                    mdata.tags = JSON.stringify( nk.split( mdata.tags ) );
                }

                if( !mdata.keywords || mdata.keywords === '' )
                {
                    mdata.keywords = '[]';
                }
                else
                {
                    mdata.keywords = JSON.stringify( nk.split( mdata.keywords ) );
                }

                if( !mdata.options || mdata.options === '' )
                {
                    mdata.options = '{"allow_comments":false,"social_forwarding":{"enabled":false}}';
                }
            }

            // We need to reference the user with this one.
            var user = klay.viewbag.user;

            // We also want to display the article edit view after adding this here existing article
            var article = false;

            // Prep the database object
            var db = this.dbo();

            // Prep the model for the database object
            var model = false;
            db.reset( model );

            var whereVals =
            {
                username: [ '=', user.username ],
                email: [ '=', user.email ]
            };

            // We store the user by Id, not by name
            var userId = db
            .select( "id from users" )
            .where( whereVals )
            .execute();

            // Extract the user's Id
            if( nk.type( userId ) === 'array' )
            {
                userId = userId[0].id;
            }

            model = klay.model.schema;
            delete model.id;
            delete model.ts;
            db.reset( model );

            whereVals =
            {
                id: [ '=', mdata.id ]
            };

            // Add our article now that we have the information to store it correctly
            var values =
            {
                type: mdata.type,
                published: mdata.published,
                acl: mdata.acl,
                owner: userId,
                luts: new Date().toISOString().slice( 0, 19 ).replace( 'T', ' ' ),
                luby: userId,
                category: mdata.category,
                tags: mdata.tags,
                title: mdata.title,
                alias: mdata.alias,
                description: mdata.description,
                content: mdata.content,
                keywords: mdata.keywords,
                options: mdata.options
            };

            var updated = false;
            updated = db
            .update( "`mmod-cms-articles`" )
            .values( values )
            .where( whereVals )
            .execute();

            model = require( './articles' ).editView.schema;
            db.reset( model );

            if( updated && updated >= 0 )
            {
                klay.viewbag.sysmsg = '<span class="xrm-xalert success">Article "' + mdata.id + '", "' + mdata.title + '", was updated successfully.</span>';

                article = db
                .select( "`mmod-cms-articles`" )
                .where ( { id: [ '=', mdata.id ] } )
                .execute();

                if( nk.type( article ) === 'array' && article.length > -1 )
                {
                    // Great, we have an array which should be full of user records
                    for( var record in article )
                    {
                        article[record].content = article[record].content;
                    }
                    klay.viewbag.article = article[0];
                }
                else
                {
                    console.log( 'Failed to fetch the article...' );
                    klay.viewbag.article = {};
                }
            }
            else
            {
                console.log( 'Something went wrong when updating the article...');
                klay.viewbag.article = {};
                klay.viewbag.sysmsg = '<span class="xrm-xalert error">Article "' + mdata.id + '", "' + mdata.title + '", was not updated successfully.</span>';
            }

            model = false;
            db.reset( model );

            var articleTypes = false;
            articleTypes = db
            .select( "* from `mmod-cms-article-types`" )
            .execute();

            model = require( './categories' ).categories.schema;
            db.reset( model );

            var categories = false;
            categories = db
            .select( "`mmod-cms-categories`" )
            .execute();

            model = false;
            db.reset( model );

            var acls = false;
            acls = db
            .select( "* from acls" )
            .execute();

            db.reset( model );

            if( nk.type( articleTypes ) === 'array' && articleTypes.length > -1 )
            {
                var tmp = [];
                for( var i = 0; i < articleTypes.length; i++ )
                {
                    tmp[articleTypes[i].id] = articleTypes[i].title;
                }

                // Set the 'selected' value
                tmp[0] = tmp[klay.viewbag.article.type || 1];

                klay.viewbag.articleTypes = tmp;
            }
            else
            {
                console.log( 'Failed to get the article types...' );
                klay.viewbag.articleTypes = {};
            }

            klay.viewbag.publishedTypes = [ klay.viewbag.article.published, 'Yes', 'No' ];

            if( nk.type( categories ) === 'array' && categories.length > -1 )
            {
                var tmp = [];
                for( var i = 0; i < categories.length; i++ )
                {
                    tmp[categories[i].id] = categories[i].title;
                }

                // Set the 'selected' value
                tmp[0] = tmp[klay.viewbag.article.category || 1];

                klay.viewbag.categories = tmp;
            }
            else
            {
                console.log( 'Failed to get the categories...' );
                klay.viewbag.categories = {};
            }

            if( nk.type( acls ) === 'array' && acls.length > -1 )
            {
                var tmp = [];
                for( var i = 0; i < acls.length; i++ )
                {
                    tmp[acls[i].id] = acls[i].name;
                }

                // Set the 'selected' value
                tmp[0] = tmp[klay.viewbag.article.acl || 1];

                klay.viewbag.articleACLs = tmp;
            }
            else
            {
                console.log( 'Failed to get the acls...' );
                klay.viewbag.articleACLs = {};
            }

            // Lastly, we need to set the article id to redisplay the form properly
            klay.viewbag.article.id = mdata.id;
            if( klay.viewbag.article.id === '[object Object]'  )
            {
                console.log( 'There was an issue setting the article id.')
            }

            // And invoke the callback, passing the result of our query
            if( nk.type( callback ) === 'function' )
            { // Asynchronous
                callback( request, response, klay );
            }
            else
            { // Synchronous
                return klay.viewbag.article;
            }
        },
        schema:
        {
            /*
             * Article/edit View model
             */
            id: [ true, 'int', ''],
            type: [ true, 'int', 'Type' ],
            published: [ true, 'int', 'Published' ],
            acl: [ true, 'int', 'ACL' ],
            ts: [ true, 'text', 'Created' ],
            owner: [ true, 'int', 'Owner' ],
            luts: [ true, 'text', 'Last Updated' ],
            luby: [ true, 'text', 'Last Updated by' ],
            category: [ true, 'int', 'Category' ],
            tags: [true, 'text', 'Tags' ],
            title: [ true, 'text', 'Title' ],
            alias: [ true, 'text', 'Alias' ],
            description: [ true, 'text', 'Description' ],
            content: [ true, 'text', 'Article Content' ],
            keywords: [ true, 'text', 'Keywords' ],
            options: [ true, 'text', 'Options' ]
        }
    },
    copyView:
    {
        copyArticle: function( request, response, callback, klay )
        {
            var mdata = null;
            if( request.posted.mdata )
            {
                mdata = JSON.parse( request.posted.mdata );
            }

            // We need to reference the user with this one.
            var user = klay.viewbag.user;

            // Prep the database object
            var db = this.dbo();

            // Prep the model for the database object
            var model = false;
            db.reset( model );

            var whereVals =
            {
                username: [ '=', user.username ],
                email: [ '=', user.email ]
            };

            // We store the user by Id, not by name
            var userId = db
            .select( "id from users" )
            .where( whereVals )
            .execute();

            // Extract the user's Id
            if( nk.type( userId ) === 'array' )
            {
                userId = userId[0].id;
            }

            // Prep the model for the database object
            db.reset( model );

            whereVals =
            {
                id: [ '=', mdata.recordId ]
            };

            // We're gonna select first, and make some alterations to what
            // will be inserted back into the table
            var article = db
            .select( "* from `mmod-cms-articles`" )
            .where( whereVals )
            .execute();

            // Now lets do the deed
            var values = {};

            if( nk.type( article ) === 'array' && article.length > 0 )
            {
                values.type = article[0].type;
                values.published = article[0].published;
                values.acl = article[0].acl;
                values.ts = new Date().toISOString().slice( 0, 19 ).replace( 'T', ' ' );
                values.owner = userId;
                values.luts = new Date().toISOString().slice( 0, 19 ).replace( 'T', ' ' );
                values.luby = userId;
                values.category = article[0].category;
                values.tags = article[0].tags;
                values.title = article[0].title + ' (Copy)';
                values.alias = article[0].alias;
                values.description = article[0].description;
                values.content = article[0].content;
                values.keywords = article[0].keywords;
                values.options = article[0].options;
            }

            // Prep the model for the database object
            model = klay.model.schema;
            delete model.id;
            db.reset( model );

            var inserted = false;

            inserted = db
            .insert( "`mmod-cms-articles`" )
            .values( values )
            .execute();

            if( inserted )
            {
                if( inserted > 0 )
                {
                    klay.viewbag.sysmsg = '<span class="xrm-xalert success">Article "' + mdata.recordId + '" copied successfully.</span>';
                }
                else
                {
                    klay.viewbag.sysmsg = '<span class="xrm-xalert error">Article "' + mdata.recordId + '" not found/copied.</span>';
                }
            }
            else
            {
                klay.viewbag.sysmsg = '<span class="xrm-xalert error">Whoopsie! Something went wrong; Article "' + mdata.recordId + '" not found/copied.';
            }

            // And invoke the callback, passing the result of our query
            if( nk.type( callback ) === 'function' )
            { // Asynchronous
                callback( request, response, klay );
            }
            else
            { // Synchronous
                return klay.viewbag.sysmsg;
            }
        },
        schema:
        {
            /*
             * Article/delete Action model
             */
            id: [ true, 'int', ''],
            type: [ true, 'int', 'Type' ],
            published: [ true, 'int', 'Published' ],
            acl: [ true, 'int', 'ACL' ],
            ts: [ true, 'text', 'Created' ],
            owner: [ true, 'int', 'Owner' ],
            luts: [ true, 'text', 'Last Updated' ],
            luby: [ true, 'text', 'Last Updated by' ],
            category: [ true, 'int', 'Category' ],
            tags: [true, 'text', 'Tags' ],
            title: [ true, 'text', 'Title' ],
            alias: [ true, 'text', 'Alias' ],
            description: [ true, 'text', 'Description' ],
            content: [ true, 'text', 'Article Content' ],
            keywords: [ true, 'text', 'Keywords' ],
            options: [ true, 'text', 'Options' ]
        }
    },
    deleteView:
    {
        deleteArticle: function( request, response, callback, klay )
        {
            var mdata = null;
            if( request.posted.mdata )
            {
                mdata = JSON.parse( request.posted.mdata );
            }

            // Prep the database object
            var db = this.dbo();

            // Prep the model for the database object
            var model = klay.model.schema;
            db.reset( model );

            var whereVals =
            {
                id: [ '=', mdata.id ]
            };

            var deleted = false;

            deleted = db
            .delete( "`mmod-cms-articles`" )
            .where( whereVals )
            .execute();

            if( deleted )
            {
                if( deleted > 0 )
                {
                    klay.viewbag.sysmsg = '<span class="xrm-xalert success">Article "' + mdata.id + '" deleted successfully.</span>';
                }
                else
                {
                    klay.viewbag.sysmsg = '<span class="xrm-xalert error">Article "' + mdata.id + '" not found/deleted.</span>';
                }
            }
            else
            {
                klay.viewbag.sysmsg = '<span class="xrm-xalert error">Whoopsie! Something went wrong; Article(s) "' + mdata.id + '" not found/deleted.</span>';
            }

            // And invoke the callback, passing the result of our query
            if( nk.type( callback ) === 'function' )
            { // Asynchronous
                callback( request, response, klay );
            }
            else
            { // Synchronous
                return klay.viewbag.sysmsg;
            }
        },
        schema:
        {
            /*
             * Article/delete Action model
             */
            id: [ true, 'int', '']
        }
    }
};


// Export
module.exports = articlesModel;