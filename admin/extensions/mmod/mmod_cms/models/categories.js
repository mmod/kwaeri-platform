/**
 * package: mmod-cms
 * sub-package: models/categories
 * author: Richard B. Winters <a href="mailto:rik@mmogp.com">rik AT mmogp DOT com</a>
 * copyright: 2011-2015 Massively Modified, Inc.
 * license: See Terms and EULA at https://mmogp.com/about/terms
 */


// Deps
var nk = require( 'nk' ),
nk = new nk();  // When we do not pass an argument to the constructor, we
                // get only the core facilities ( .type, .extend, .each,
                // .hash, etc )


/**
 * Defines the category model
 *
 * @since 0.0.1
 */
var categoriesModel =
{
    categories:
    {
        listAll: function( request, response, callback, klay )
        {
            // Prep the database object
            var db = this.dbo();

            // Rid the DBO of any models currently set.
            db.reset();

            // Get a list of all categories
            var categories = db
            .query( "select * from `mmod-cms-categories`" )
            .execute();

            if( nk.type( categories ) === 'array' && categories.length > -1 )
            {
                // Great, we have an array which should be full of category records
                klay.viewbag.enabledTypes = [ 'No', 'No', 'Yes' ];
                for( var category in categories )
                {
                    categories[category].enabled = klay.viewbag.enabledTypes[categories[category].enabled];
                }
                klay.viewbag.categories = categories;
            }
            else
            {
                console.log( 'Failed to get a list of categories...' );
                klay.viewbag.categories = {};
            }

            // And invoke the callback, passing the result of our query
            if( nk.type( callback ) === 'function' )
            { // Asynchronous
                callback( request, response, klay );
            }
            else
            { // Synchronous
                return categories;
            }
        },
        schema:
        {
            /*
             * category Model
             */
            id: [ true, 'int', ''],
            acl: [ true, 'int', 'ACL' ],
            ts: [ true, 'text', 'Created' ],
            luts: [ true, 'text', 'Last Updated' ],
            luby: [ true, 'text', 'Last Updated by' ],
            title: [ true, 'text', 'Title' ],
            description: [ true, 'text', 'Description' ],
            parent: [ true, 'int', 'Parent' ],
            enabled: [ true, 'int', 'Enabled' ]
        }
    },
    listView:
    {
        schema:
        {
            /*
             * Category/List View Model
             */
            id: [ true, 'int', ''],
            acl: [ true, 'int', 'ACL' ],
            ts: [ true, 'text', 'Created' ],
            luts: [ true, 'text', 'Last Updated' ],
            luby: [ true, 'text', 'Last Updated by' ],
            title: [ true, 'text', 'Title' ],
            description: [ true, 'text', 'Description' ],
            parent: [ true, 'int', 'Parent' ],
            enabled: [ true, 'int', 'Enabled' ]
        }
    },
    addView:
    {
        getCategoryInputFieldOptions: function( request, response, callback, klay )
        {
            var query_good = true;

            klay.viewbag.enabledTypes = [ 'No', 'No', 'Yes' ];

            // Prep the database object
            var db = this.dbo();
            var model = false;
            db.reset( model );

            var acls = false;
            acls = db
            .select( "* from acls" )
            .execute();

            if( nk.type( acls ) === 'array' && acls.length > -1 )
            {
                var tmp = [];
                for( var i = 0; i < acls.length; i++ )
                {
                    tmp[acls[i].id] = acls[i].name;
                }

                tmp[0] = tmp[1];
                klay.viewbag.categoryACLs = tmp;
            }
            else
            {
                query_good = false;
                console.log( 'Failed to get the acls...' );
                klay.viewbag.categoryACLs = {};
            }

            model = false;
            db.reset( model );

            var parents = false;
            parents = db
            .select( "* from `mmod-cms-categories`" )
            .execute();

            if( nk.type( parents ) === 'array' && parents.length > -1 )
            {
                var tmp = [];
                tmp[1] = 'None';
                for( var i = 0; i < parents.length; i++ )
                {
                    tmp[(parents[i].id + 1)] = parents[i].title;
                }

                tmp[0] = tmp[1];

                klay.viewbag.categoryParents = tmp;
            }
            else
            {
                query_good = false;
                console.log( 'Failed to get a list of eligible parents...' );
                klay.viewbag.categoryParents = {};
            }

            // And invoke the callback, passing the result of our query
            if( nk.type( callback ) === 'function' )
            { // Asynchronous
                callback( request, response, query_good, klay );
            }
            else
            { // Synchronous
                return query_good;
            }
        },
        addCategory: function( request, response, callback, klay )
        {
            var mdata;
            if( request.posted.mdata )
            {
                mdata = JSON.parse( request.posted.mdata );
            }

            // We need to reference the user with this one.
            var user = klay.viewbag.user;

            // Prep the database object
            var db = this.dbo();

            // Prep the model for the database object
            var model = false;
            db.reset( false );

            // We store the user by Id, not by name
            var whereVals =
            {
                username: [ '=', user.username ],
                email: [ '=', user.email ]
            };

            var userId = db
            .select( "id from users" )
            .where( whereVals )
            .execute();

            // Extract the user's Id
            if( nk.type( userId ) === 'array' )
            {
                userId = userId[0].id;
            }

            model = klay.model.schema;
            delete model.id;
            db.reset( model );

            // Add the category now that we have the data to store it properly
            var values =
            {
                acl: mdata.acl,
                ts: new Date().toISOString().slice(0, 19).replace('T', ' '),
                luts: new Date().toISOString().slice(0, 19).replace('T', ' '),
                luby: userId,
                title: mdata.title,
                description: mdata.description,
                parent: mdata.parent - 1,
                enabled: mdata.enabled
            };

            var added = db
            .insert( "`mmod-cms-categories`" )     // Records contains the number of rows affected.
            .values( values )
            .execute();

            model = false;
            db.reset( model );

            if( added )
            {
                if( added > 0 )
                {
                    klay.viewbag.sysmsg = '<span class="xrm-xalert success">Category "' + mdata.title + '" added successfully.</span>';
                }
                else
                {
                    klay.viewbag.sysmsg = '<span class="xrm-xalert error">Category "' + mdata.title + '" not saved.</span>';
                }
            }
            else
            {
                klay.viewbag.sysmsg = '<span class="xrm-xalert error">Whoopsie! Something went wrong; Category "' + mdata.title + '" not saved.</span>';
            }

            // And invoke the callback, passing the result of our query
            if( nk.type( callback ) === 'function' )
            { // Asynchronous
                callback( request, response, klay );
            }
            else
            { // Synchronous
                return klay.viewbag.sysmsg;
            }
        },
        schema:
        {
            /*
             * Category/Add Action Model
             */
            id: [ true, 'int', ''],
            acl: [ true, 'int', 'ACL' ],
            ts: [ true, 'text', 'Created' ],
            luts: [ true, 'text', 'Last Updated' ],
            luby: [ true, 'text', 'Last Updated by' ],
            title: [ true, 'text', 'Title' ],
            description: [ true, 'text', 'Description' ],
            parent: [ true, 'int', 'Parent' ],
            enabled: [ true, 'int', 'Enabled' ]
        }
    },
    editView:
    {
        getCategory: function( request, response, callback, klay )
        {
            var categoryId = false;
            if( Object.prototype.hasOwnProperty.call( request.requrl.query, 'categoryId' ) )
            {
                categoryId = request.requrl.query.categoryId;
            }
            else
            {
                if( Object.prototype.hasOwnProperty.call( request.requrl.query, 'recordId' ) )
                {
                    categoryId = request.requrl.query.recordId;
                }
                else
                {
                    console.log( 'Category/Record ID not found in query string!' );
                }
            }

            var category = false;

            if( categoryId )
            {
                // Prep the database object
                var db = this.dbo();

                // Prepare the model for the database
                var model = klay.model.schema;
                db.reset( model );

                // Get a list of all categories
                category = db
                .select( "`mmod-cms-categories`" )
                .where( { id: [ '=', categoryId ] } )
                .execute();

                model = false;
                db.reset( false );
            }

            if( nk.type( category ) === 'array' && category.length > -1 )
            {
                // Great, we have an array which should be full of category records
                klay.viewbag.category = category[0];
                klay.viewbag.category.id = categoryId;
                klay.viewbag.enabledTypes = [ 'No', 'No', 'Yes' ];
                klay.viewbag.enabledTypes[0] = klay.viewbag.enabledTypes[klay.viewbag.category.enabled];
            }
            else
            {
                console.log( 'Failed to get the category...' );
                klay.viewbag.category = {};
                klay.viewbag.category.id = categoryId;
                klay.viewbag.enabledTypes = [ 'No', 'No', 'Yes' ];
            }

            var acls = false;
            acls = db
            .select( "* from acls" )
            .execute();

            if( nk.type( acls ) === 'array' && acls.length > -1 )
            {
                var tmp = [];
                for( var i = 0; i < acls.length; i++ )
                {
                    tmp[acls[i].id] = acls[i].name;
                }

                tmp[0] = tmp[klay.viewbag.category.acl];
                klay.viewbag.categoryACLs = tmp;
            }
            else
            {
                console.log( 'Failed to get the acls...' );
                klay.viewbag.categoryACLs = {};
            }

            model = false;
            db.reset( model );

            var parents = false;
            parents = db
            .select( "* from `mmod-cms-categories`" )
            .execute();

            if( nk.type( parents ) === 'array' && parents.length > -1 )
            {
                var tmp = [];
                tmp[1] = 'None';
                for( var i = 0; i < parents.length; i++ )
                {
                    tmp[(parents[i].id + 1)] = parents[i].title;
                }

                tmp[0] = tmp[(klay.viewbag.category.parent + 1)];

                klay.viewbag.categoryParents = tmp;
            }
            else
            {
                query_good = false;
                console.log( 'Failed to get a list of eligible parents...' );
                klay.viewbag.categoryParents = {};
            }

            // And invoke the callback, passing the result of our query
            if( nk.type( callback ) === 'function' )
            { // Asynchronous
                callback( request, response, klay );
            }
            else
            { // Synchronous
                return category;
            }
        },
        editCategory: function( request, response, callback, klay )
        {
            var mdata;
            if( request.posted.mdata )
            {
                mdata = JSON.parse( request.posted.mdata );
            }

            // We need to reference the user with this one.
            var user = klay.viewbag.user;

            // We also want to display the category edit view after adding this here existing menu
            var category = false;

            // Prep the database object
            var db = this.dbo();

            // Prep the model for the database object
            var model = false;
            db.reset( model );

            var whereVals =
            {
                username: [ '=', user.username ],
                email: [ '=', user.email ]
            };

            // We store the user by Id, not by name
            var userId = db
            .select( "id from users" )
            .where( whereVals )
            .execute();

            // Extract the user's Id
            if( nk.type( userId ) === 'array' )
            {
                userId = userId[0].id;
            }

            model = klay.model.schema;
            delete model.id;
            delete model.ts;
            db.reset( model );

            whereVals =
            {
                id: [ '=', mdata.id ]
            };

            // Update our menu-item now that we have the information to store it correctly
            var values =
            {
                acl: mdata.acl,
                luts: new Date().toISOString().slice( 0, 19 ).replace( 'T', ' ' ),
                luby: userId,
                title: mdata.title,
                description: mdata.description,
                parent: mdata.parent - 1,
                enabled: mdata.enabled
            };

            var updated = false;
            updated = db
            .update( "`mmod-cms-categories`" )
            .values( values )
            .where( whereVals )
            .execute();

            model = require( './categories' ).editView.schema;
            db.reset( model );

            if( updated && updated >= 0 )
            {
                klay.viewbag.sysmsg = '<span class="xrm-xalert success">Category "' + mdata.id + '", "' + mdata.title + '", was updated successfully.</span>';

                category = db
                .select( "`mmod-cms-categories`" )
                .where ( { id: [ '=', mdata.id ] } )
                .execute();

                if( nk.type( category ) === 'array' && category.length > -1 )
                {
                    klay.viewbag.category = category[0];
                    klay.viewbag.category.id = mdata.id;
                }
                else
                {
                    console.log( 'Failed to get the category...' );
                    klay.viewbag.category = {};
                    klay.viewbag.category.id = mdata.id;
                }
            }
            else
            {
                console.log( 'Something went wrong when updating the category...');
                klay.viewbag.category = {};
                klay.viewbag.category.id = mdata.id;
                klay.viewbag.sysmsg = '<span class="xrm-xalert error">Category "' + mdata.id + '", "' + mdata.title + '", was not updated successfully.</span>';
            }

            var enabledTypes = [ 'No', 'No', 'Yes' ];
            klay.viewbag.enabledTypes = [ enabledTypes[klay.viewbag.category.enabled], 'No', 'Yes' ];

            db.reset( model );

            var parents = false;
            parents = db
            .select( "`mmod-cms-categories`" )
            .execute();

            if( nk.type( parents ) === 'array' && parents.length > -1 )
            {
                var tmp = [];
                tmp[1] = 'None';
                for( var i = 0; i < parents.length; i++ )
                {
                    tmp[(parents[i].id + 1)] = parents[i].title;
                }

                tmp[0] = tmp[(klay.viewbag.category.parent + 1)];

                klay.viewbag.categoryParents = tmp;
            }
            else
            {
                console.log( 'Failed to get a list of eligible parents...' );
                klay.viewbag.categoryParents = {};
            }

            model = false;
            db.reset( model );

            var acls = db
            .select( "* from acls" )
            .execute();

            if( nk.type( acls ) === 'array' && acls.length > -1 )
            {
                var tmp = [];
                for( var i = 0; i < acls.length; i++ )
                {
                    tmp[acls[i].id] = acls[i].name;
                }

                tmp[0] = tmp[klay.viewbag.category.acl];
                klay.viewbag.categoryACLs = tmp;
            }
            else
            {
                console.log( 'Failed to get the acls...' );
                klay.viewbag.categoryACLs = {};
            }

            model = false;
            db.reset( model );

            // And invoke the callback, passing the result of our query
            if( nk.type( callback ) === 'function' )
            { // Asynchronous
                callback( request, response, klay );
            }
            else
            { // Synchronous
                return category;
            }
        },
        schema:
        {
            /*
             * Category/Edit Action/View Model
             */
            id: [ true, 'int', ''],
            acl: [ true, 'int', 'ACL' ],
            ts: [ true, 'text', 'Created' ],
            luts: [ true, 'text', 'Last Updated' ],
            luby: [ true, 'text', 'Last Updated by' ],
            title: [ true, 'text', 'Title' ],
            description: [ true, 'text', 'Description' ],
            parent: [ true, 'int', 'Parent' ],
            enabled: [ true, 'int', 'Enabled' ]
        }
    },
    copyView:
    {
        copyCategory: function( request, response, callback, klay )
        {
            var mdata = null;
            if( request.posted.mdata )
            {
                mdata = JSON.parse( request.posted.mdata );
            }

            // We need to reference the user with this one.
            var user = klay.viewbag.user;

            // Prep the database object
            var db = this.dbo();

            // Prep the model for the database object
            var model = false;
            db.reset( model );

            var whereVals =
            {
                username: [ '=', user.username ],
                email: [ '=', user.email ]
            };

            // We store the user by Id, not by name
            var userId = db
            .select( "id from users" )
            .where( whereVals )
            .execute();

            // Extract the user's Id
            if( nk.type( userId ) === 'array' )
            {
                userId = userId[0].id;
            }

            // Prep the model for the database object
            db.reset( model );

            whereVals =
            {
                id: [ '=', mdata.recordId ]
            };

            // We're gonna select first, and make some alterations to what
            // will be inserted back into the table
            var category = db
            .select( "* from `mmod-cms-categories`" )
            .where( whereVals )
            .execute();

            // Now lets do the deed
            var values = {};

            if( nk.type( category ) === 'array' && category.length > 0 )
            {
                values.acl = category[0].acl;
                values.ts = new Date().toISOString().slice( 0, 19 ).replace( 'T', ' ' );
                values.luts = new Date().toISOString().slice( 0, 19 ).replace( 'T', ' ' );
                values.luby = userId;
                values.title = category[0].title + ' (Copy)';
                values.description = category[0].description;
                values.parent = category[0].parent;
                values.enabled = category[0].enabled;
            }

            // Prep the model for the database object
            model = klay.model.schema;
            delete model.id;
            db.reset( model );

            var inserted = false;

            inserted = db
            .insert( "`mmod-cms-categories`" )
            .values( values )
            .execute();

            if( inserted )
            {
                if( inserted > 0 )
                {
                    klay.viewbag.sysmsg = '<span class="xrm-xalert success">Category "' + mdata.recordId + '" copied successfully.</span>';
                }
                else
                {
                    klay.viewbag.sysmsg = '<span class="xrm-xalert error">Category "' + mdata.recordId + '" not found/copied.</span>';
                }
            }
            else
            {
                klay.viewbag.sysmsg = '<span class="xrm-xalert error">Whoopsie! Something went wrong; Category "' + mdata.recordId + '" not found/copied.</span>';
            }

            // And invoke the callback, passing the result of our query
            if( nk.type( callback ) === 'function' )
            { // Asynchronous
                callback( request, response, klay );
            }
            else
            { // Synchronous
                return klay.viewbag.sysmsg;
            }
        },
        schema:
        {
            /*
             * Category/Copy Action model
             */
            id: [ true, 'int', ''],
            acl: [ true, 'int', 'acl' ],
            ts: [ true, 'text', 'Created' ],
            luts: [ true, 'text', 'Last Updated' ],
            luby: [ true, 'text', 'Last Updated by' ],
            title: [ true, 'text', 'Title' ],
            description: [ true, 'text', 'Description' ],
            parent: [ true, 'int', 'Parent' ],
            enabled: [ true, 'int', 'Enabled' ]
        }
    },
    deleteView:
    {
        deleteCategory: function( request, response, callback, klay )
        {
            var mdata = null;
            if( request.posted.mdata )
            {
                mdata = JSON.parse( request.posted.mdata );
            }

            // Prep the database object
            var db = this.dbo();

            // Prep the model for the database object
            var model = klay.model.schema;
            db.reset( model );

            var whereVals =
            {
                id: [ '=', mdata.id ]
            };

            var deleted = false;

            deleted = db
            .delete( "`mmod-cms-categories`" )
            .where( whereVals )
            .execute();

            if( deleted )
            {
                if( deleted > 0 )
                {

                    klay.viewbag.sysmsg = '<span class="xrm-xalert success">Category "' + mdata.id + '" deleted successfully.</span> ';

                    db.reset( model );
                }
                else
                {
                    klay.viewbag.sysmsg = '<span class="xrm-xalert error">Category "' + mdata.id + '" not found/deleted.</span>';
                }
            }
            else
            {
                klay.viewbag.sysmsg = '<span class="xrm-alert error">Whoopsie! Something went wrong; Category "' + mdata.id + '" not found/deleted.</span>';
            }

            // And invoke the callback, passing the result of our query
            if( nk.type( callback ) === 'function' )
            { // Asynchronous
                callback( request, response, klay );
            }
            else
            { // Synchronous
                return klay.viewbag.sysmsg;
            }
        },
        schema:
        {
            /*
             * Category/Delete Action model
             */
            id: [ true, 'int', '']
        }
    }
};


// Export
module.exports = categoriesModel;