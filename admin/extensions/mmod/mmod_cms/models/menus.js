/**
 * package: mmod-cms
 * sub-package: models/menus
 * author: Richard B. Winters <a href="mailto:rik@mmogp.com">rik AT mmogp DOT com</a>
 * copyright: 2011-2015 Massively Modified, Inc.
 * license: See Terms and EULA at https://mmogp.com/about/terms
 */


// Deps
var nk = require( 'nk' ),
nk = new nk();  // When we do not pass an argument to the constructor, we
                // get only the core facilities ( .type, .extend, .each,
                // .hash, etc )


/**
 * Defines the menu model
 *
 * @since 0.0.1
 */
var menusModel =
{
    menus:
    {
        listAll: function( request, response, callback, klay )
        {
            // Prep the database object
            var db = this.dbo();

            // Rid the DBO of any models currently set.
            db.reset();

            // Get a list of all menus
            var menus = db
            .query( "select * from `mmod-cms-menus`" )
            .execute();

            if( nk.type( menus ) === 'array' && menus.length > 0 )
            {
                // Great, we have an array which should be full of menu records
                klay.viewbag.enabledTypes = [ 'No', 'No', 'Yes' ];
                for( var menu in menus )
                {
                    menus[menu].enabled = klay.viewbag.enabledTypes[menus[menu].enabled];
                }
                klay.viewbag.menus = menus;
            }
            else
            {
                console.log( 'Failed to get a list of menus...' );
                klay.viewbag.menus = {};
            }

            // And invoke the callback, passing the result of our query
            if( nk.type( callback ) === 'function' )
            { // Asynchronous
                callback( request, response, klay );
            }
            else
            { // Synchronous
                return menus;
            }
        },
        schema:
        {
            /*
             * Menu Model
             */
            id: [ true, 'int', ''],
            ts: [ true, 'text', 'Created' ],
            luts: [ true, 'text', 'Last Updated' ],
            luby: [ true, 'text', 'Last Updated by' ],
            title: [ true, 'text', 'Title' ],
            description: [ true, 'text', 'Description' ],
            enabled: [ true, 'int', 'Enabled' ],
            options: [ true, 'text', 'Options' ]
        }
    },
    listView:
    {
        schema:
        {
            /*
             * Menu Model
             */
            id: [ true, 'int', ''],
            ts: [ true, 'text', 'Created' ],
            luts: [ true, 'text', 'Last Updated' ],
            luby: [ true, 'text', 'Last Updated by' ],
            title: [ true, 'text', 'Title' ],
            description: [ true, 'text', 'Description' ],
            enabled: [ true, 'int', 'Enabled' ],
            options: [ true, 'text', 'Options' ]
        }
    },
    addView:
    {
        getMenuInputFieldOptions: function( request, response, callback, klay )
        {
            var query_good = true;

            klay.viewbag.enabledTypes = [ 'No', 'No', 'Yes' ];

            // And invoke the callback, passing the result of our query
            if( nk.type( callback ) === 'function' )
            { // Asynchronous
                callback( request, response, query_good, klay );
            }
            else
            { // Synchronous
                return query_good;
            }
        },
        addMenu: function( request, response, callback, klay )
        {
            var mdata;
            if( request.posted.mdata )
            {
                mdata = JSON.parse( request.posted.mdata );

                if( !mdata.options || mdata.options === '' )
                {
                    mdata.options = '{}';
                }
            }

            // We need to reference the user with this one.
            var user = klay.viewbag.user;

            // Prep the database object
            var db = this.dbo();

            // Prep the model for the database object
            var model = false;
            db.reset( false );

            // We store the user by Id, not by name
            var whereVals =
            {
                username: [ '=', user.username ],
                email: [ '=', user.email ]
            };

            var userId = db
            .select( "id from users" )
            .where( whereVals )
            .execute();

            // Extract the user's Id
            if( nk.type( userId ) === 'array' )
            {
                userId = userId[0].id;
            }

            model = klay.model.schema;
            delete model.id;
            db.reset( model );

            // Add our article now that we have the information to store it correctly
            if( !mdata.options || mdata.options === '' )
            {
                mdata.options = '{}';
            }

            var values =
            {
                ts: new Date().toISOString().slice(0, 19).replace('T', ' '),
                luts: new Date().toISOString().slice(0, 19).replace('T', ' '),
                luby: userId,
                title: mdata.title,
                description: mdata.description,
                enabled: mdata.enabled,
                options: mdata.options
            };

            var added = db
            .insert( "`mmod-cms-menus`" )     // Records contains the number of rows affected.
            .values( values )
            .execute();

            model = klay.model.schema
            db.reset( model );

            if( added )
            {
                if( added > 0 )
                {
                    klay.viewbag.sysmsg = '<span class="xrm-xalert success">Menu "' + mdata.title + '" added successfully.</span>';
                }
                else
                {
                    klay.viewbag.sysmsg = '<span class="xrm-xalert error">Menu "' + mdata.title + '" not saved.</span>';
                }
            }
            else
            {
                klay.viewbag.sysmsg = '<span class="xrm-xalert error">Whoopsie! Something went wrong; Menu "' + mdata.title + '" not saved.</span>';
            }

            // And invoke the callback, passing the result of our query
            if( nk.type( callback ) === 'function' )
            { // Asynchronous
                callback( request, response, klay );
            }
            else
            { // Synchronous
                return klay.viewbag.sysmsg;
            }
        },
        schema:
        {
            /*
             * Menu Model
             */
            id: [ true, 'int', ''],
            ts: [ true, 'text', 'Created' ],
            luts: [ true, 'text', 'Last Updated' ],
            luby: [ true, 'text', 'Last Updated by' ],
            title: [ true, 'text', 'Title' ],
            description: [ true, 'text', 'Description' ],
            enabled: [ true, 'int', 'Enabled' ],
            options: [ true, 'text', 'Options' ]
        }
    },
    editView:
    {
        getMenu: function( request, response, callback, klay )
        {
            var menuId = false;
            if( Object.prototype.hasOwnProperty.call( request.requrl.query, 'menuId' ) )
            {
                menuId = request.requrl.query.menuId;
            }
            else
            {
                if( Object.prototype.hasOwnProperty.call( request.requrl.query, 'recordId' ) )
                {
                    // User came here from the universal edit mechanism  on the list page
                    menuId = request.requrl.query.recordId;
                }
                else
                {
                    console.log( 'Menu/Record Id not found in query string!' );
                }
            }

            var menu = false;

            if( menuId )
            {
                // Prep the database object
                var db = this.dbo();

                // Prepare the model for the database
                var model = klay.model.schema;
                db.reset( model );

                // Get the menu
                menu = db
                .select( "`mmod-cms-menus`" )
                .where( { id: [ '=', menuId ] } )
                .execute();

                model = false;
                db.reset( false );
            }

            if( nk.type( menu ) === 'array' && menu.length > -1 )
            {
                klay.viewbag.menu = menu[0];
                klay.viewbag.menu.id = menuId;

                var enabledTypes =  [ 'No', 'No', 'Yes' ];
                klay.viewbag.enabledTypes = [ enabledTypes[klay.viewbag.menu.enabled], 'No', 'Yes' ];
            }
            else
            {
                console.log( 'Failed to get the menu...' );
                klay.viewbag.menu = {};
                klay.viewbag.menu.id = menuId;

                klay.viewbag.enabledTypes = [ 'No', 'No', 'Yes' ];
            }

            // And invoke the callback, passing the result of our query
            if( nk.type( callback ) === 'function' )
            { // Asynchronous
                callback( request, response, klay );
            }
            else
            { // Synchronous
                return;
            }
        },
        editMenu: function( request, response, callback, klay )
        {
            var mdata = null;
            if( request.posted.mdata )
            {
                // Store data so it can be manipulated more easily by other components which will
                // experience higher usage/traffic than this here article creation process (i.e.
                // the traffic of end users reading the articles). We'll only do this where applicable.
                mdata = JSON.parse( request.posted.mdata );
            }

            // We need to reference the user with this one.
            var user = klay.viewbag.user;

            // We also want to display the menu edit view after adding this here existing menu
            var menu = false;

            // Prep the database object
            var db = this.dbo();

            // Prep the model for the database object
            var model = false;
            db.reset( model );

            var whereVals =
            {
                username: [ '=', user.username ],
                email: [ '=', user.email ]
            };

            // We store the user by Id, not by name
            var userId = db
            .select( "id from users" )
            .where( whereVals )
            .execute();

            // Extract the user's Id
            if( nk.type( userId ) === 'array' )
            {
                userId = userId[0].id;
            }

            model = klay.model.schema;
            delete model.id;
            delete model.ts;
            db.reset( model );

            whereVals =
            {
                id: [ '=', mdata.id ]
            };

            // Add our article now that we have the information to store it correctly
            if( !mdata.options || mdata.options === '' )
            {
                mdata.options = '{}';
            }
            var values =
            {
                luts: new Date().toISOString().slice( 0, 19 ).replace( 'T', ' ' ),
                luby: userId,
                title: mdata.title,
                description: mdata.description,
                enabled: mdata.enabled,
                options: mdata.options
            };

            var updated = false;
            updated = db
            .update( "`mmod-cms-menus`" )
            .values( values )
            .where( whereVals )
            .execute();

            model = require( './menus' ).editView.schema;
            db.reset( model );

            if( updated && updated >= 0 )
            {
                klay.viewbag.sysmsg = '<span class="xrm-xalert success">Menu "' + mdata.id + '", "' + mdata.title + '", was updated successfully.</span>';

                menu = db
                .select( "`mmod-cms-menus`" )
                .where ( { id: [ '=', mdata.id ] } )
                .execute();

                if( nk.type( menu ) === 'array' && menu.length > -1 )
                {
                    // Great, we have an array which should be full of user records
                    klay.viewbag.menu = menu[0];

                    var enabledTypes =  [ 'No', 'No', 'Yes' ];
                    klay.viewbag.enabledTypes = [ enabledTypes[klay.viewbag.menu.enabled], 'No', 'Yes' ];
                }
                else
                {
                    console.log( 'Failed to fetch the menu...' );
                    klay.viewbag.menu = {};
                }
            }
            else
            {
                console.log( 'Something went wrong when updating the menu...');
                klay.viewbag.menu = {};
                klay.viewbag.sysmsg = '<span class="xrm-xalert error">Menu "' + mdata.id + '", "' + mdata.title + '", was not updated successfully.</span>';
            }

            model = false;
            db.reset( model );

            // Lastly, we need to set the menu id to redisplay the form properly
            klay.viewbag.menu.id = mdata.id;
            if( klay.viewbag.menu.id === '[object Object]'  )
            {
                console.log( 'There was an issue setting the menu id.')
            }

            // And invoke the callback, passing the result of our query
            if( nk.type( callback ) === 'function' )
            { // Asynchronous
                callback( request, response, klay );
            }
            else
            { // Synchronous
                return klay.viewbag.menu;
            }
        },
        schema:
        {
            /*
             * Article/edit View model
             */
            id: [ true, 'int', ''],
            luts: [ true, 'text', 'Last Updated' ],
            luby: [ true, 'text', 'Last Updated by' ],
            title: [ true, 'text', 'Title' ],
            description: [ true, 'text', 'Description' ],
            enabled: [ true, 'int', 'Enabled' ],
            options: [ true, 'text', 'Options' ]
        }
    },
    copyView:
    {
        copyMenu: function( request, response, callback, klay )
        {
            var mdata = null;
            if( request.posted.mdata )
            {
                mdata = JSON.parse( request.posted.mdata );
            }

            // We need to reference the user with this one.
            var user = klay.viewbag.user;

            // Prep the database object
            var db = this.dbo();

            // Prep the model for the database object
            var model = false;
            db.reset( model );

            var whereVals =
            {
                username: [ '=', user.username ],
                email: [ '=', user.email ]
            };

            // We store the user by Id, not by name
            var userId = db
            .select( "id from users" )
            .where( whereVals )
            .execute();

            // Extract the user's Id
            if( nk.type( userId ) === 'array' )
            {
                userId = userId[0].id;
            }

            // Prep the model for the database object
            db.reset( model );

            whereVals =
            {
                id: [ '=', mdata.recordId ]
            };

            // We're gonna select first, and make some alterations to what
            // will be inserted back into the table
            var menu = db
            .select( "* from `mmod-cms-menus`" )
            .where( whereVals )
            .execute();

            // Now lets do the deed
            var values = {};

            if( nk.type( menu ) === 'array' && menu.length > 0 )
            {
                values.ts = new Date().toISOString().slice( 0, 19 ).replace( 'T', ' ' );
                values.luts = new Date().toISOString().slice( 0, 19 ).replace( 'T', ' ' );
                values.luby = userId;
                values.title = menu[0].title + ' (Copy)';
                values.description = menu[0].description;
                values.enabled = menu[0].enabled;
                values.options = menu[0].options;
            }

            // Prep the model for the database object
            model = klay.model.schema;
            delete model.id;
            db.reset( model );

            var inserted = false;

            inserted = db
            .insert( "`mmod-cms-menus`" )
            .values( values )
            .execute();

            if( inserted )
            {
                if( inserted > 0 )
                {
                    klay.viewbag.sysmsg = '<span class="xrm-xalert success">Menu "' + mdata.recordId + '" copied successfully.</span>';
                }
                else
                {
                    klay.viewbag.sysmsg = '<span class="xrm-xalert error">Menu "' + mdata.recordId + '" not found/copied.</span>';
                }
            }
            else
            {
                klay.viewbag.sysmsg = '<span class="xrm-xalert error">Whoopsie! Something went wrong; Menu "' + mdata.recordId + '" not found/copied.</span>';
            }

            // And invoke the callback, passing the result of our query
            if( nk.type( callback ) === 'function' )
            { // Asynchronous
                callback( request, response, klay );
            }
            else
            { // Synchronous
                return klay.viewbag.sysmsg;
            }
        },
        schema:
        {
            /*
             * Article/delete Action model
             */
            id: [ true, 'int', ''],
            ts: [ true, 'text', 'Created' ],
            luts: [ true, 'text', 'Last Updated' ],
            luby: [ true, 'text', 'Last Updated by' ],
            title: [ true, 'text', 'Title' ],
            description: [ true, 'text', 'Description' ],
            enabled: [ true, 'int', 'Enabled' ],
            options: [ true, 'text', 'Options' ]
        }
    },
    deleteView:
    {
        deleteMenu: function( request, response, callback, klay )
        {
            var mdata = null;
            if( request.posted.mdata )
            {
                mdata = JSON.parse( request.posted.mdata );
            }

            // Prep the database object
            var db = this.dbo();

            // Prep the model for the database object
            var model = klay.model.schema;
            db.reset( model );

            var whereVals =
            {
                id: [ '=', mdata.id ]
            };

            var deleted = false;

            deleted = db
            .delete( "`mmod-cms-menus`" )
            .where( whereVals )
            .execute();

            if( deleted )
            {
                var piece = false;
                if( deleted > 0 )
                {

                    klay.viewbag.sysmsg = 'Menu "' + mdata.id + '" deleted successfully.';

                    db.reset( model );

                    whereVals =
                    {
                        parent: [ '=', mdata.id ]
                    };

                    var children_deleted = db
                    .delete( "`mmod-cms-menu-items`" )
                    .where( whereVals )
                    .execute();

                    if( children_deleted )
                    {
                        if( children_deleted > 0 )
                        {
                            piece = '<span class="xrm-xalert success">';
                            klay.viewbag.sysmsg = piece + klay.viewbag.sysmsg;
                            klay.viewbag.sysmsg += 'and so were its children.';
                            klay.viewbag.sysmsg += '</span>';
                        }
                        else
                        {
                            piece = '<span class="xrm-xalert error">';
                            klay.viewbag.sysmsg = piece + klay.viewbag.sysmsg;
                            klay.viewbag.sysmsg += 'but its children were not found/deleted.';
                            klay.viewbag.sysmsg += '</span>';
                        }
                    }
                    else
                    {
                        piece = '<span class="xrm-xalert error">';
                        klay.viewbag.sysmsg = piece + klay.viewbag.sysmsg;
                        klay.viewbag.sysmsg += 'but something went wrong, and its children were not found/deleted.';
                        klay.viewbag.sysmsg += '</span>';
                    }
                }
                else
                {
                    piece = '<span class="xrm-xalert error">';
                    klay.viewbag.sysmsg = piece + 'Menu "' + mdata.id + '" and its children were not found/deleted';
                    klay.viewbag.sysmsg += '</span>';
                }
            }
            else
            {
                klay.viewbag.sysmsg = '<span class="xrm-xalert error">Whoopsie! Something went wrong; Menu "' + mdata.id + '" and its children were not found/deleted.</span>';
            }

            // And invoke the callback, passing the result of our query
            if( nk.type( callback ) === 'function' )
            { // Asynchronous
                callback( request, response, klay );
            }
            else
            { // Synchronous
                return klay.viewbag.sysmsg;
            }
        },
        schema:
        {
            /*
             * Article/delete Action model
             */
            id: [ true, 'int', '']
        }
    }
};


// Export
module.exports = menusModel;