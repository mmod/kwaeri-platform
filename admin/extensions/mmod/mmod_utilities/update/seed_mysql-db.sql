CREATE TABLE IF NOT EXISTS `mmdev`.`mmod-utilities-settings`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `ts` DATETIME NULL,
  `luts` DATETIME NULL,
  `luby` INT NULL,
  `lytebox_settings` TEXT NOT NULL,
  `kwaeri_chat_settings` TEXT NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=NDBCLUSTER;

INSERT INTO `mmdev`.`mmod-utilities-settings`
( `ts`, `luts`, `luby`, `lytebox_settings`, `kwaeri_chat_settings` )
VALUES
( NOW(), NOW(), 0, '{"tags_enabled":true,"comments_enabled":true}', '{"tags_enabled":true,"comments_enabled":true}' );


CREATE TABLE `mmpro`.`mmod-utilities-settings` LIKE `mmdev`.`mmod-utilities-settings`;
INSERT INTO `mmpro`.`mmod-utilities-settings` SELECT * FROM `mmdev`.`mmod-utilities-settings`;
