/**
 * package: mmod-xrm
 * sub-package: admin/assets/mmod/js
 * version: 0.0.1
 * author:  Richard B. Winters <a href="mailto:rik@mmogp.com">rik AT mmogp DOT com</a>
 * copyright: 2011-2015 Massively Modified, Inc.
 * license: Apache, Version 2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 */


// kwaeri.extensionToolbarItems
( function( $ )
    {
        $.kwaeri.extensionToolbarItems =
        {
            dash:
            {
                tbsync: '<li><a onclick="$.mmod.view( { type: \'ext\', path: \'extensions/manage/mmod-cms/\', ui: \'settings\' act: \'updates\' } );"><span class="btn btn-success"><span class="flaticon-arrows97 mpbf"></span> Check for Updates</span></a></li>',
                tbopt:  '<li class="dropdown">' +
                            '<a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">' +
                                '<span class="btn btn-primary"><span class="flaticon-gear33 mpbf"></span> Options <span class="caret"></span></span>' +
                            '</a>' +
                            '<ul class="dropdown-menu" role="menu">' +
                                '<li><a href="#"><span class="flaticon-verify8"></span> Publish</a></li>' +
                                '<li><a href="#"><span class="flaticon-prohibition21"></span> Unpublish</a></li>' +
                                '<li><a href="#"><span class="flaticon-eye104"></span> Preview</a></li>' +
                                '<li class="divider"></li>' +
                                '<li><a onclick="$.mmod.view( { type: \'ext\', path: \'extensions/manage/mmod-cms/\', ui: \'settings\' } );"><span class="flaticon-wrench72"></span> Settings</a></li>' +
                            '</ul>' +
                        '</li>'
            },
            articles:
            {
                add:
                {
                    tbsave: '<li><a href="/extensions/manage/mmod-cms/articles/add" data-xrm="turn" data-system="ext" data-method="POST" data-params=\'{ "type": "submitForm", "formName": "addArticleForm", "resultSet": "1", "forward": "index" }\'><span class="btn btn-success"><span class="flaticon-harddisc mpbf"></span> Add</span></a></li>',
                    tbsavenew: '<li><a href="/extensions/manage/mmod-cms/articles/add" data-xrm="turn" data-system="ext" data-method="POST" data-params=\'{ "type": "submitForm", "formName": "addArticleForm", "resultSet": "1" }\'><span class="btn btn-success"><span class="flaticon-arrows96 mpbf"></span> Add &amp; New</span></a></li>',
                    tbcancel: '<li><a href="/extensions/manage/mmod-cms/articles/" data-xrm="turn" data-system="ext" class="mmedd" role="button"><span class="btn btn-warning"><span class="flaticon-cross93 mpbf"></span> Cancel</span></a></li>'
                },
                edit:
                {
                    tbsave: '<li><a href="/extensions/manage/mmod-cms/articles/edit" data-xrm="turn" data-system="ext" data-method="POST" data-params=\'{ "type": "submitForm", "formName": "editArticleForm", "resultSet": "1" }\'><span class="btn btn-success"><span class="flaticon-harddisc mpbf"></span> Update</span></a></li>',
                    tbsaveclose: '<li><a href="/extensions/manage/mmod-cms/articles/edit" data-xrm="turn" data-system="ext" data-method="POST" data-params=\'{ "type": "submitForm", "formName": "editArticleForm", "resultSet": "1", "forward": "index" }\'><span class="btn btn-success"><span class="flaticon-checked20 mpbf"></span> Update &amp; Return</span></a></li>',
                    tbcancelopt:    '<li>' +
                                        '<a href="/extensions/manage/mmod-cms/articles/" data-xrm="turn" data-system="ext" class="mmedd" role="button"><span class="btn btn-warning"><span class="flaticon-cross95 mpbf"></span> Cancel</span></a>' +
                                        '<button type="button" class="dropdown-toggle btn btn-warning" data-toggle="dropdown" aria-expanded="false">' +
                                            '<span class="caret"></span>' +
                                            '<span class="sr-only">Toggle Dropdown</span>' +
                                        '</button>' +
                                        '<ul class="dropdown-menu" role="menu">' +
                                            '<li><a href="#"><span class="flaticon-verify8"></span> Publish</a></li>' +
                                            '<li><a href="#"><span class="flaticon-prohibition21"></span> Unpublish</a></li>' +
                                            '<li><a href="#"><span class="flaticon-eye104"></span> Preview</a></li>' +
                                            '<li class="divider"></li>' +
                                            '<li><a href="/extensions/manage/mmod-cms/articles/delete" data-xrm="turn" data-system="ext" data-method="POST" data-params=\'{ "type": "submitForm", "formName": "editArticleForm", "forward": "index" }\'><span class="flaticon-recycle69"></span> Delete</a></li>' +
                                            '<li class="divider"></li>' +
                                            '<li><a href="/extensions/manage/mmod-cms/settings" data-xrm="turn" data-system="ext"><span class="flaticon-wrench72"></span> Settings</a></li>' +
                                        '</ul>' +
                                    '</li>'
                },
                tbnew: '<li role="presentation"><a href="/extensions/manage/mmod-cms/articles/add" data-xrm="turn" data-system="ext"><span class="btn btn-success"><span class="flaticon-file84 mpbf"></span> Add New</span></a></li>',
                tbcopy: '<li role="presentation"><a href="/extensions/manage/mmod-cms/articles/copy" data-xrm="turn" data-system="lbox" data-method="POST" data-params="{}" class="xrm-xcopy"><span class="btn btn-warning"><span class="flaticon-rectangular82 mpbf"></span> Copy</span></a></li>',
                tbedit: '<li role="presentation"><a href="/extensions/manage/mmod-cms/articles/edit" data-xrm="turn" data-system="lbox" data-params="{}" class="xrm-xedit"><span class="btn btn-warning"><span class="flaticon-edit44 mpbf"></span> Edit</span></a></li>',
                tbopt: '<li role="presentation">' +
                            '<a href="/extensions/manage/mmod-cms/settings" data-xrm="turn" data-system="ext" class="mmedd" role="button"><span class="btn btn-warning"><span class="flaticon-cross95 mpbf"></span> Options</span></a>' +
                            '<button type="button" class="dropdown-toggle btn btn-warning" data-toggle="dropdown" aria-expanded="false">' +
                                '<span class="caret"></span>' +
                                '<span class="sr-only">Toggle Dropdown</span>' +
                            '</button>' +
                            '<ul class="dropdown-menu" role="menu">' +
                            '<li><a href="#"><span class="flaticon-verify8"></span> Publish</a></li>' +
                            '<li><a href="#"><span class="flaticon-prohibition21"></span> Unpublish</a></li>' +
                            '<li class="divider"></li>' +
                            '<li><a href="#"><span class="flaticon-eye104"></span> Preview</a></li>' +
                            '</ul>' +
                        '</li>'
            },
            categories:
            {
                add:
                {
                    tbsave: '<li><a href="/extensions/manage/mmod-cms/categories/add" data-xrm="turn" data-system="ext" data-method="POST" data-params=\'{ "type": "submitForm", "formName": "addCategoryForm", "resultSet": "1", "forward": "index" }\'><span class="btn btn-success"><span class="flaticon-harddisc mpbf"></span> Add</span></a></li>',
                    tbsavenew: '<li><a href="/extensions/manage/mmod-cms/categories/add" data-xrm="turn" data-system="ext" data-method="POST" data-params=\'{ "type": "submitForm", "formName": "addCategoryForm", "resultSet": "1" }\'><span class="btn btn-success"><span class="flaticon-arrows96 mpbf"></span> Add &amp; New</span></a></li>',
                    tbcancel: '<li><a href="/extensions/manage/mmod-cms/categories/" data-xrm="turn" data-system="ext" class="mmedd" role="button"><span class="btn btn-warning"><span class="flaticon-cross93 mpbf"></span> Cancel</span></a></li>'
                },
                edit:
                {
                    tbsave: '<li><a href="/extensions/manage/mmod-cms/categories/edit" data-xrm="turn" data-system="ext" data-method="POST" data-params=\'{ "type": "submitForm", "formName": "editCategoryForm", "resultSet": "1" }\'><span class="btn btn-success"><span class="flaticon-harddisc mpbf"></span> Update</span></a></li>',
                    tbsaveclose: '<li><a href="/extensions/manage/mmod-cms/categories/edit" data-xrm="turn" data-system="ext" data-method="POST" data-params=\'{ "type": "submitForm", "formName": "editCategoryForm", "resultSet": "1", "forward": "index" }\'><span class="btn btn-success"><span class="flaticon-checked20 mpbf"></span> Update &amp; Return</span></a></li>',
                    tbcancelopt:    '<li>' +
                                        '<a href="/extensions/manage/mmod-cms/categories/" data-xrm="turn" data-system="ext" class="mmedd" role="button"><span class="btn btn-warning"><span class="flaticon-cross95 mpbf"></span> Cancel</span></a>' +
                                        '<button type="button" class="dropdown-toggle btn btn-warning" data-toggle="dropdown" aria-expanded="false">' +
                                            '<span class="caret"></span>' +
                                            '<span class="sr-only">Toggle Dropdown</span>' +
                                        '</button>' +
                                        '<ul class="dropdown-menu" role="menu">' +
                                            '<li><a href="/extensions/manage/mmod-cms/categories/delete" data-xrm="turn" data-system="ext" data-method="POST" data-params=\'{ "type": "submitForm", "formName": "editCategoryForm", "forward": "index" }\'><span class="flaticon-recycle69"></span> Delete</a></li>' +
                                            '<li class="divider"></li>' +
                                            '<li><a href="/extensions/manage/mmod-cms/settings" data-xrm="turn" data-system="ext"><span class="flaticon-wrench72"></span> Settings</a></li>' +
                                        '</ul>' +
                                    '</li>'
                },
                tbnew: '<li role="presentation"><a href="/extensions/manage/mmod-cms/categories/add" data-xrm="turn" data-system="ext"><span class="btn btn-success"><span class="flaticon-file84 mpbf"></span> Add New</span></a></li>',
                tbcopy: '<li role="presentation"><a href="/extensions/manage/mmod-cms/categories/copy" data-xrm="turn" data-system="lbox" data-method="POST" data-params="{}" class="xrm-xcopy"><span class="btn btn-warning"><span class="flaticon-rectangular82 mpbf"></span> Copy</span></a></li>',
                tbedit: '<li role="presentation"><a href="/extensions/manage/mmod-cms/categories/edit" data-xrm="turn" data-system="lbox" data-params="{}" class="xrm-xedit"><span class="btn btn-warning"><span class="flaticon-edit44 mpbf"></span> Edit</span></a></li>',
                tbopt: '<li role="presentation">' +
                            '<a href="/extensions/manage/mmod-cms/settings" data-xrm="turn" data-system="ext" class="mmedd" role="button"><span class="btn btn-warning"><span class="flaticon-cross95 mpbf"></span> Options</span></a>' +
                            '<button type="button" class="dropdown-toggle btn btn-warning" data-toggle="dropdown" aria-expanded="false">' +
                                '<span class="caret"></span>' +
                                '<span class="sr-only">Toggle Dropdown</span>' +
                            '</button>' +
                            '<ul class="dropdown-menu" role="menu">' +
                            '<li><a href="#"><span class="flaticon-verify8"></span> Enable</a></li>' +
                            '<li><a href="#"><span class="flaticon-prohibition21"></span> Disable</a></li>' +
                            '</ul>' +
                        '</li>'
            },
            menus:
            {
                add:
                {
                    tbsave: '<li><a href="/extensions/manage/mmod-cms/menus/add" data-xrm="turn" data-system="ext" data-method="POST" data-params=\'{ "type": "submitForm", "formName": "addMenuForm", "resultSet": "1", "forward": "index" }\'><span class="btn btn-success"><span class="flaticon-harddisc mpbf"></span> Add</span></a></li>',
                    tbsavenew: '<li><a href="/extensions/manage/mmod-cms/menus/add" data-xrm="turn" data-system="ext" data-method="POST" data-params=\'{ "type": "submitForm", "formName": "addMenuForm", "resultSet": "1" }\'><span class="btn btn-success"><span class="flaticon-arrows96 mpbf"></span> Add &amp; New</span></a></li>',
                    tbcancel: '<li><a href="/extensions/manage/mmod-cms/menus/" data-xrm="turn" data-system="ext" class="mmedd" role="button"><span class="btn btn-warning"><span class="flaticon-cross93 mpbf"></span> Cancel</span></a></li>'
                },
                edit:
                {
                    tbsave: '<li><a href="/extensions/manage/mmod-cms/menus/edit" data-xrm="turn" data-system="ext" data-method="POST" data-params=\'{ "type": "submitForm", "formName": "editMenuForm", "resultSet": "1" }\'><span class="btn btn-success"><span class="flaticon-harddisc mpbf"></span> Update</span></a></li>',
                    tbsaveclose: '<li><a href="/extensions/manage/mmod-cms/menus/edit" data-xrm="turn" data-system="ext" data-method="POST" data-params=\'{ "type": "submitForm", "formName": "editMenuForm", "resultSet": "1", "forward": "index" }\'><span class="btn btn-success"><span class="flaticon-checked20 mpbf"></span> Update &amp; Return</span></a></li>',
                    tbcancelopt:    '<li>' +
                                        '<a href="/extensions/manage/mmod-cms/menus/" data-xrm="turn" data-system="ext" class="mmedd" role="button"><span class="btn btn-warning"><span class="flaticon-cross95 mpbf"></span> Cancel</span></a>' +
                                        '<button type="button" class="dropdown-toggle btn btn-warning" data-toggle="dropdown" aria-expanded="false">' +
                                            '<span class="caret"></span>' +
                                            '<span class="sr-only">Toggle Dropdown</span>' +
                                        '</button>' +
                                        '<ul class="dropdown-menu" role="menu">' +
                                            '<li><a href="/extensions/manage/mmod-cms/menus/delete" data-xrm="turn" data-system="ext" data-method="POST" data-params=\'{ "type": "submitForm", "formName": "editMenuForm", "forward": "index" }\'><span class="flaticon-recycle69"></span> Delete</a></li>' +
                                            '<li class="divider"></li>' +
                                            '<li><a href="/extensions/manage/mmod-cms/settings" data-xrm="turn" data-system="ext"><span class="flaticon-wrench72"></span> Settings</a></li>' +
                                        '</ul>' +
                                    '</li>'
                },
                tbnew: '<li role="presentation"><a href="/extensions/manage/mmod-cms/menus/add" data-xrm="turn" data-system="ext"><span class="btn btn-success"><span class="flaticon-file84 mpbf"></span> Add New</span></a></li>',
                tbcopy: '<li role="presentation"><a href="/extensions/manage/mmod-cms/menus/copy" data-xrm="turn" data-system="lbox" data-method="POST" data-params="{}" class="xrm-xcopy"><span class="btn btn-warning"><span class="flaticon-rectangular82 mpbf"></span> Copy</span></a></li>',
                tbedit: '<li role="presentation"><a href="/extensions/manage/mmod-cms/menus/edit" data-xrm="turn" data-system="lbox" data-params="{}" class="xrm-xedit"><span class="btn btn-warning"><span class="flaticon-edit44 mpbf"></span> Edit</span></a></li>',
                tbopt: '<li role="presentation">' +
                            '<a href="/extensions/manage/mmod-cms/settings" data-xrm="turn" data-system="ext" class="mmedd" role="button"><span class="btn btn-warning"><span class="flaticon-cross95 mpbf"></span> Options</span></a>' +
                            '<button type="button" class="dropdown-toggle btn btn-warning" data-toggle="dropdown" aria-expanded="false">' +
                                '<span class="caret"></span>' +
                                '<span class="sr-only">Toggle Dropdown</span>' +
                            '</button>' +
                            '<ul class="dropdown-menu" role="menu">' +
                            '<li><a href="#"><span class="flaticon-verify8"></span> Enable</a></li>' +
                            '<li><a href="#"><span class="flaticon-prohibition21"></span> Disable</a></li>' +
                            '</ul>' +
                        '</li>'
            },
            "menu-items":
            {
                add:
                {
                    tbsave: '<li><a href="/extensions/manage/mmod-cms/menu-items/add" data-xrm="turn" data-system="ext" data-method="POST" data-params=\'{ "type": "submitForm", "formName": "addMenuItemForm", "resultSet": "1", "forward": { "action": "index", "data": {} } }\' class="xrm-child-xadd"><span class="btn btn-success"><span class="flaticon-harddisc mpbf"></span> Add</span></a></li>',
                    tbsavenew: '<li><a href="/extensions/manage/mmod-cms/menu-items/add" data-xrm="turn" data-system="ext" data-method="POST" data-params=\'{ "type": "submitForm", "formName": "addMenuItemForm", "resultSet": "1", "forward": { "action": "add", "data": {} } }\' class="xrm-child-xar"><span class="btn btn-success"><span class="flaticon-arrows96 mpbf"></span> Add &amp; New</span></a></li>',
                    tbcancel: '<li><a href="/extensions/manage/mmod-cms/menu-items/" data-xrm="turn" data-system="ext" class="mmedd" role="button"><span class="btn btn-warning"><span class="flaticon-cross93 mpbf"></span> Cancel</span></a></li>'
                },
                edit:
                {
                    tbsave: '<li><a href="/extensions/manage/mmod-cms/menu-items/edit" data-xrm="turn" data-system="ext" data-method="POST" data-params=\'{ "type": "submitForm", "formName": "editMenuItemForm", "resultSet": "1" }\'><span class="btn btn-success"><span class="flaticon-harddisc mpbf"></span> Update</span></a></li>',
                    tbsaveclose: '<li><a href="/extensions/manage/mmod-cms/menu-items/edit" data-xrm="turn" data-system="ext" data-method="POST" data-params=\'{ "type": "submitForm", "formName": "editMenuItemForm", "resultSet": "1", "forward": { "action": "index", "data": "{}" } }\' class="xrm-child-xur"><span class="btn btn-success"><span class="flaticon-checked20 mpbf"></span> Update &amp; Return</span></a></li>',
                    tbcancelopt:    '<li>' +
                                        '<a href="/extensions/manage/mmod-cms/menu-items" data-xrm="turn" data-system="ext" data-params="{}" class="mmedd xrm-child-xcancel" role="button"><span class="btn btn-warning"><span class="flaticon-cross95 mpbf"></span> Cancel</span></a>' +
                                        '<button type="button" class="dropdown-toggle btn btn-warning" data-toggle="dropdown" aria-expanded="false">' +
                                            '<span class="caret"></span>' +
                                            '<span class="sr-only">Toggle Dropdown</span>' +
                                        '</button>' +
                                        '<ul class="dropdown-menu" role="menu">' +
                                            '<li><a href="#"><span class="flaticon-verify8"></span> Publish</a></li>' +
                                            '<li><a href="#"><span class="flaticon-prohibition21"></span> Unpublish</a></li>' +
                                            '<li class="divider"></li>' +
                                            '<li><a href="/extensions/manage/mmod-cms/menu-items/delete" data-xrm="turn" data-system="ext" data-method="POST" data-params=\'{ "type": "submitForm", "formName": "editMenuItemForm", "forward": "index" }\' class="xrm-child-xdelete"><span class="flaticon-recycle69"></span> Delete</a></li>' +
                                            '<li class="divider"></li>' +
                                            '<li><a href="/extensions/manage/mmod-cms/settings" data-xrm="turn" data-system="ext"><span class="flaticon-wrench72"></span> Settings</a></li>' +
                                        '</ul>' +
                                    '</li>'
                },
                tbnew: '<li role="presentation"><a href="/extensions/manage/mmod-cms/menu-items/add" data-xrm="turn" data-system="ext" data-params="{}" class="xrm-child-xnew"><span class="btn btn-success"><span class="flaticon-file84 mpbf"></span> Add New</span></a></li>',
                tbcopy: '<li role="presentation"><a href="/extensions/manage/mmod-cms/menu-items/copy" data-xrm="turn" data-system="lbox" data-method="POST" data-params="{}" class="xrm-child-xcopy"><span class="btn btn-warning"><span class="flaticon-rectangular82 mpbf"></span> Copy</span></a></li>',
                tbedit: '<li role="presentation"><a href="/extensions/manage/mmod-cms/menu-items/edit" data-xrm="turn" data-system="lbox" data-params="{}" class="xrm-child-xedit"><span class="btn btn-warning"><span class="flaticon-edit44 mpbf"></span> Edit</span></a></li>',
                tbopt: '<li role="presentation">' +
                            '<a href="/extensions/manage/mmod-cms/settings" data-xrm="turn" data-system="ext" class="mmedd" role="button"><span class="btn btn-warning"><span class="flaticon-cross95 mpbf"></span> Options</span></a>' +
                            '<button type="button" class="dropdown-toggle btn btn-warning" data-toggle="dropdown" aria-expanded="false">' +
                                '<span class="caret"></span>' +
                                '<span class="sr-only">Toggle Dropdown</span>' +
                            '</button>' +
                            '<ul class="dropdown-menu" role="menu">' +
                            '<li><a href="#"><span class="flaticon-verify8"></span> Enable</a></li>' +
                            '<li><a href="#"><span class="flaticon-prohibition21"></span> Disable</a></li>' +
                            '</ul>' +
                        '</li>'
            },
            settings:
            {
                tbsync: '<li><a onclick="$.mmod.view( { type: \'ext\', path: \'extensions/manage/mmod-cms/\', ui: \'settings\' act: \'updates\' } );"><span class="btn btn-success"><span class="flaticon-arrows97 mpbf"></span> Check for Updates</span></a></li>',
                tbapply: '<li><a onclick="$.mmod.view( { type: \'ext\', path: \'extensions/manage/mmod-cms/\', ui: \'settings\', act: \'edit\', method: \'POST\', data: { type: \'submitForm\', formName: \'editSettingsForm\' } } );"><span class="btn btn-success"><span class="flaticon-harddisc mpbf"></span> Update Settings!</span></a></li>',
                tbopt:  '<li class="dropdown">' +
                            '<a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">' +
                                '<span class="btn btn-primary"><span class="flaticon-gear33 mpbf"></span> Options <span class="caret"></span></span>' +
                            '</a>' +
                            '<ul class="dropdown-menu" role="menu">' +
                                '<li class="divider"></li>' +
                                '<li><a onclick="$.mmod.view( { type: \'ext\', path: \'extensions/manage/mmod-cms/\', ui: \'settings\' } );"><span class="flaticon-wrench72"></span> Settings</a></li>' +
                            '</ul>' +
                        '</li>'
            }
        };
    }(jQuery)
);