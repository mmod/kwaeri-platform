/**
 * package: mmod-xrm
 * sub-package: admin/assets/mmod/js
 * version: 0.0.1
 * author:  Richard B. Winters <a href="mailto:rik@mmogp.com">rik AT mmogp DOT com</a>
 * copyright: 2011-2015 Massively Modified, Inc.
 * license: Apache, Version 2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 */