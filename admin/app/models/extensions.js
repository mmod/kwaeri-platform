/**
 * package: mmod-cms
 * sub-package: models/extensions
 * author: Richard B. Winters <a href="mailto:rik@mmogp.com">rik AT mmogp DOT com</a>
 * copyright: 2011-2015 Massively Modified, Inc.
 * license: See Terms and EULA at https://mmogp.com/about/terms
 */


// Deps
var nk = require( 'nk' ),
nk = new nk();  // When we do not pass an argument to the constructor, we
                // get only the core facilities ( .type, .extend, .each,
                // .hash, etc )


/**
 * Defines the extension models
 *
 * @since 0.0.1
 */
var extensionsModel =
{
    extension:
    {
        enable:
        {

        },
        disable:
        {

        },
        list: function( request, response, callback, klay )
        {
            // Prep the database object
            var db = this.dbo();

            // Rid the DBO of any models currently set.
            db.reset();

            // Get a list of all extensions
            var extensions = db
            .query( "select id, name as system_name, fancy_name as name, description, version, author, website, ts as installed, token, enabled from extensions;" )
            .execute();

            if( nk.type( extensions ) === 'array' )
            {
                // Great, we have an array which should be full of user records
                //console.log( extensions );
                klay.viewbag.extensions_list = extensions;
            }
            else
            {
                console.log( 'Failed to get a list of extensions mang...' );
                klay.viewbag.extensions_list = {};
            }

            // And invoke the callback, passing the result of our query
            if( nk.type( callback ) === 'function' )
            { // Asynchronous
                callback( request, response, extensions, klay );
            }
            else
            { // Synchronous
                return extensions;
            }
        },
        schema:
        {
            /*
             * Extension View Model
             */
            id: [ true, 'int', ''],
            type: [ true, 'int', 'Type' ],
            ts: [ true, 'text', 'Installed' ],
            luts: [ true, 'text', 'Last Updated' ],
            luby: [ true, 'text', 'Last Updated by' ],
            system_name: [ true, 'text', '' ], // name as system_name
            name: [ true, 'text', 'Name' ], // fancy_name as name
            description: [ true, 'text', 'Description' ],
            version: [ true, 'text', 'Version' ],
            author: [ true, 'text', 'Author' ],
            website: [ true, 'text', 'Website' ],
            token: [ true, 'text', 'token' ],
            enabled: [ true, 'int', 'Enabled' ]
        }
    },
    listView:
    {
        schema:
        {
            /*
             * Extension View Model
             */
            id: [ true, 'int', ''],
            type: [ true, 'int', 'Type' ],
            installed: [ true, 'text', 'Installed' ], // ts as installed
            luts: [ true, 'text', 'Last Updated' ],
            luby: [ true, 'text', 'Last Updated by' ],
            system_name: [ true, 'text', '' ], // name as system_name
            name: [ true, 'text', 'Name' ], // fancy_name as name
            description: [ true, 'text', 'Description' ],
            version: [ true, 'text', 'Version' ],
            author: [ true, 'text', 'Author' ],
            website: [ true, 'text', 'Website' ],
            token: [ true, 'text', 'token' ],
            enabled: [ true, 'int', 'Enabled' ]
        }
    },
    settingsView:
    {
        schema:
        {
            /*
             * Extension View Model
             */
            id: [ true, 'int', ''],
            type: [ true, 'int', 'Type' ],
            ts: [ true, 'text', 'Installed' ],
            luts: [ true, 'text', 'Last Updated' ],
            luby: [ true, 'text', 'Last Updated by' ],
            system_name: [ true, 'text', '' ], // name as system_name
            name: [ true, 'text', 'Name' ], // fancy_name as name
            description: [ true, 'text', 'Description' ],
            version: [ true, 'text', 'Version' ],
            author: [ true, 'text', 'Author' ],
            website: [ true, 'text', 'Website' ],
            token: [ true, 'text', 'token' ],
            enabled: [ true, 'int', 'Enabled' ]
        }
    },
    removeView:
    {
        removeExtension: function( request, response, callback, klay )
        {
            // Prep the database object
            var db = this.dbo();

            // Run our query. We will not compare passwords quite yet, as we
            // need the timestamp from the record as part of our salt. This is
            // ok since usernames are unique.
            //var authenticated =
            db.reset();

            // Delete Extension
            var deleted = db
            .query( "" )
            .execute();

            //console.log( authenticated );
            if( nk.type( deleted ) === 'array' )
            {
            }
            else
            {
                // User wasn't found..redirect?
                user = false;
            }

            // And invoke the callback, passing the result of our query
            if( nk.type( callback ) === 'function' )
            { // Asynchronous
                callback( request, response, deleted, klay );
            }
            else
            { // Synchronous
                return deleted;
            }
        },
        schema:
        {
            /*
             * Extension View Model
             */
            id: [ true, 'int', ''],
            type: [ true, 'int', 'Type' ],
            ts: [ true, 'text', 'Installed' ],
            luts: [ true, 'text', 'Last Updated' ],
            luby: [ true, 'text', 'Last Updated by' ],
            system_name: [ true, 'text', '' ], // name as system_name
            name: [ true, 'text', 'Name' ], // fancy_name as name
            description: [ true, 'text', 'Description' ],
            version: [ true, 'text', 'Version' ],
            author: [ true, 'text', 'Author' ],
            website: [ true, 'text', 'Website' ],
            token: [ true, 'text', 'token' ],
            enabled: [ true, 'int', 'Enabled' ]
        }
    }
};


// Export
module.exports = extensionsModel;