/**
 * package: mmod-xrm
 * sub-package: models/user
 * author: Richard B. Winters <a href="mailto:rik@mmogp.com">rik AT mmogp DOT com</a>
 * copyright: 2011-2015 Massively Modified, Inc.
 * license: See Terms and EULA at https://mmogp.com/about/terms
 */


// Deps
var nk = require( 'nk' ),
nk = new nk();  // When we do not pass an argument to the constructor, we
                // get only the core facilities ( .type, .extend, .each,
                // .hash, etc )


/**
 * Defines the context model
 *
 * @since 0.0.1
 */
var userModel =
{
    user:
    {
        list: function( request, response, callback, klay )
        {
            // Prep the database object
            var db = this.dbo();

            // Rid the DBO of any models currently set.
            var model = false;
            db.reset( model );

            // Get a list of all users
            var users = db
            .select
            (
                "users.id as id, users.type as type, users.ts as ts, users.username as username, contexts.company as context, acls.name as acl, users.first as first, " +
                "users.middle as middle, users.last as last, contexts.email as email from users"
            )
            .join( "contexts on users.id=contexts.owner and users.context=contexts.id" )
            .join( "acls on users.acl=acls.id" )
            .execute();

            console.log( 'users: ' );
            console.log( users );

            db.reset( model );

            if( nk.type( users ) === 'array' && users.length > -1 )
            {
                // Great, we have an array which should be full of user records
                klay.viewbag.users = users;
            }
            else
            {
                console.log( 'Failed to get a list of users...' );
                klay.viewbag.users = {};
            }

            klay.viewbag.userTypes = [ 'Registered', 'Registered', 'Administrator' ];

            for( var i = 0; i < klay.viewbag.users.length; i++ )
            {
                // User types
                klay.viewbag.users[i].type = klay.viewbag.userTypes[klay.viewbag.users[i].type];

                // Contexts
                if( !klay.viewbag.users[i].context )
                {
                    klay.viewbag.users[i].context = 'Default';
                }
            }

            // And invoke the callback, passing the result of our query
            if( nk.type( callback ) === 'function' )
            { // Asynchronous
                callback( request, response, klay );
            }
            else
            { // Synchronous
                return users;
            }
        },
        schema:
        {
            /*
             * User View Model
             */
            // company: [ true, 'text', 'Record Id' ], // We can extend the user system as we see fit
            id: [ true, 'int', 'User Id'],
            type: [ true, 'int', 'Account Type' ],
            acl: [ true, 'int', 'User ACL' ],
            cacl: [ true, 'int', 'Context ACL' ],
            ts: [ true, 'text', 'Member Since' ],
            username: [ true, 'text', 'Username' ],
            email: [ true, 'text', 'Email' ],
            context: [ true, 'int', 'Default Context' ],
            company: [ false, 'int', 'Company' ],
            first: [ true, 'text', 'First Name' ],
            last: [ true, 'text', 'Last Name' ]
        }
    },
    listView:
    {
        schema:
        {
            /*
             * User/list View model
             */
            id: [ true, 'int', ''],
            type: [ true, 'int', 'Type' ],
            acl: [ true, 'int', 'ACL' ],
            ts: [ true, 'text', 'Since' ],
            luts: [ true, 'text', 'Updated' ],
            luby: [ true, 'int', 'Updated by' ],
            username: [ true, 'text', 'Username' ],
            password: [ true, 'text', 'Password' ],
            first: [ true, 'text', 'First' ],
            middle: [ true, 'text', 'Middle' ],
            last: [ true, 'text', 'Last' ],
            email: [ true, 'text', 'Email' ],
            context: [ true, 'int', 'Context' ]
        }
    },
    addView:
    {
        getUserInputFieldOptions: function( request, response, callback, klay )
        {
            var query_good = true;

            // Prep the database object
            var db = this.dbo();

            // Prep the model for the database object
            var model = false;
            db.reset( model );

            klay.viewbag.userTypes = [ 'Registered', 'Registered', 'Administrator' ];

            db.reset( model );

            var acls = false;
            acls = db
            .select( "* from acls" )
            .execute();

            db.reset( model );

            if( nk.type( acls ) === 'array' && acls.length > -1 )
            {
                var tmp = [];
                for( var i = 0; i < acls.length; i++ )
                {
                    tmp[acls[i].id] = acls[i].name;
                }

                // Set the 'selected' value
                tmp[0] = tmp[1];

                klay.viewbag.userACLs = tmp;
            }
            else
            {
                console.log( 'Failed to get the acls...' );
                klay.viewbag.userACLs = {};
            }

            // And invoke the callback, passing the result of our query
            if( nk.type( callback ) === 'function' )
            { // Asynchronous
                callback( request, response, klay );
            }
            else
            { // Synchronous
                return query_good;
            }
        },
        addArticle: function( request, response, callback, klay )
        {
            var mdata;
            if( request.posted.mdata )
            {
                // Store data so it can be manipulated more easily by other components which will
                // experience higher usage/traffic than this here article creation process (i.e.
                // the traffic of end users reading the articles). We'll only do this where applicable.
                mdata = JSON.parse( request.posted.mdata );
                if( !mdata.tags || mdata.tags === '' )
                {
                    mdata.tags = '[]';
                }
                else
                {
                    mdata.tags = JSON.stringify( nk.split( mdata.tags ) );
                }
                if( !mdata.keywords || mdata.keywords === '' )
                {
                    mdata.keywords = '[]';
                }
                else
                {
                    mdata.keywords = JSON.stringify( nk.split( mdata.keywords ) );
                }
                if( !mdata.options || mdata.options === '' )
                {
                    mdata.options = '{"allow_comments":false,"social_forwarding":{"enabled":false}}';
                }
            }

            // We need to reference the user with this one.
            var user = klay.viewbag.user;

            // We also want to display the article edit view after adding this here new article
            var article = false;

            // Prep the database object
            var db = this.dbo();

            // Prep the model for the database object
            var model = false;
            db.reset( false );

            // We store the user by Id, not by name
            var whereVals =
            {
                username: [ '=', user.username ],
                email: [ '=', user.email ]
            };

            var userId = db
            .select( "id from users" )
            .where( whereVals )
            .execute();

            // Extract the user's Id
            if( nk.type( userId ) === 'array' )
            {
                userId = userId[0].id;
            }

            model = klay.model.schema;
            delete model.id;
            db.reset( model );

            // Add our article now that we have the information to store it correctly
            var values =
            {
                type: mdata.type,
                published: mdata.published,
                acl: mdata.acl,
                ts: new Date().toISOString().slice(0, 19).replace('T', ' '),
                owner: userId,
                luts: new Date().toISOString().slice(0, 19).replace('T', ' '),
                luby: userId,
                category: mdata.category,
                tags: mdata.tags,
                title: mdata.title,
                alias: mdata.alias,
                description: mdata.description,
                content: mdata.content,
                keywords: mdata.keywords,
                options: mdata.options
            };

            var added = db
            .insert( 'mmod_cms_articles' )     // Records contains the number of rows affected.
            .values( values )
            .execute();

            model = klay.model.schema
            db.reset( model );

            if( added )
            {
                if( added > 0 )
                {
                    klay.viewbag.sysmsg = '<span class="xrm-xalert success">Article "' + mdata.title + '" added successfully.</span>';
                }
                else
                {
                    klay.viewbag.sysmsg = '<span class="xrm-xalert error">Article "' + mdata.title + '" not saved.</span>';
                }
            }
            else
            {
                klay.viewbag.sysmsg = '<span class="xrm-xalert error">Whoopsie! Something went wrong; Article "' + mdata.title + '" not saved.</span>';
            }

            // And invoke the callback, passing the result of our query
            if( nk.type( callback ) === 'function' )
            { // Asynchronous
                callback( request, response, klay );
            }
            else
            { // Synchronous
                return klay.viewbag.sysmsg;
            }
        },
        schema:
        {
            /*
             * Users/add View model
             */
            id: [ true, 'int', ''],
            type: [ true, 'int', 'Type' ],
            acl: [ true, 'int', 'ACL' ],
            username: [ true, 'text', 'Username' ],
            email: [ true, 'text', 'Email' ],
            first: [ true, 'text', 'First' ],
            middle: [ true, 'text', 'Middle' ],
            last: [ true, 'text', 'Last' ],
            add1: [ true, 'text', 'Address 1' ],
            add2: [ true, 'text', 'Address 2' ],
            city: [ true, 'text', 'City' ],
            state: [ true, 'text', 'State' ],
            zip: [ true, 'text', 'Zip/Postal' ],
            country: [ true, 'text', 'Country' ],
            phone: [ true, 'text', 'Phone' ],
            fax: [ true, 'text', 'Fax' ]
        }
    },
    editView:
    {
        getUser: function( request, response, callback, klay )
        {
            var userId = false;
            if( Object.prototype.hasOwnProperty.call( request.requrl.query, 'userId' ) )
            {
                userId = request.requrl.query.userId;
            }
            else
            {
                if( Object.prototype.hasOwnProperty.call( request.requrl.query, 'recordId' ) )
                {
                    // User came here from the universal edit mechanism  on the list page
                    userId = request.requrl.query.recordId;
                }
                else
                {
                    console.log( 'User/Record ID not found in query string!' );
                }
            }

            var user = false;

            if( userId )
            {
                // Prep the database object
                var db = this.dbo();

                // Prepare the model for the database
                var model = klay.model.schema;
                db.reset( model );

                // Get the user
                user = db
                .select
                (
                    "users"
                )
                .where( { 'users.id': [ '=', userId ] } )
                .execute();

                model = false;
                db.reset( false );

                var companies = false;
                companies = db
                .select( "* from companies" )
                .where( { owner: [ '=', user[0].context ] } )
                .execute();

                model = false;
                db.reset( false );

                var contexts = false;
                contexts = db
                .select( "* from contexts" )
                .where( { owner: [ '=', userId ] } )
                .execute();

                model = false;
                db.reset( model );

                var acls = false;
                acls = db
                .select( "* from acls" )
                .execute();

                db.reset( model );
            }

            if( nk.type( user ) === 'array' && user.length > -1 )
            {
                // Great, we have an array which should be full of user records

                klay.viewbag.userRecord = user[0];
                klay.viewbag.userRecord.id = userId;
            }
            else
            {
                console.log( 'Failed to get the user...' );
                klay.viewbag.userRecord = {};
                klay.viewbag.userRecord.id = userId;
            }

            klay.viewbag.userTypes = [ 'Registered', 'Registered', 'Administrator' ];
            klay.viewbag.userTypes[0] = klay.viewbag.userTypes[klay.viewbag.userRecord.type];

            if( nk.type( companies ) === 'array' && companies.length > -1 )
            {
                var tmp = [];
                for( var i = 0; i < companies.length; i++ )
                {
                    if( !companies[i].name )
                    {
                        tmp[companies[i].id] = 'None';
                    }
                    else
                    {
                        tmp[companies[i].id] = companies[i].name;
                    }
                }

                // Set the 'selected' value
                tmp[0] = tmp[contexts[klay.viewbag.userRecord.context].company];

                klay.viewbag.userCompanies = tmp;
            }
            else
            {
                console.log( 'Failed to get the user companies...' );
                klay.viewbag.userCompanies = {};
            }

            if( nk.type( contexts ) === 'array' && contexts.length > -1 )
            {
                var tmp = [];
                for( var i = 0; i < contexts.length; i++ )
                {
                    if( !contexts[i].company )
                    {
                        tmp[contexts[i].id] = 'Default';
                    }
                    else
                    {
                        if( klay.viewbag.userCompanies[contexts[i].company] )
                        {
                            tmp[contexts[i].id] = klay.viewbag.userCompanies[contexts[i].company];
                        }
                        else
                        {
                            tmp[contexts[i].id] = 'Issue!';
                        }
                    }
                }

                // Set the 'selected' value
                tmp[0] = tmp[klay.viewbag.userRecord.context];

                klay.viewbag.userContexts = tmp;
            }
            else
            {
                console.log( 'Failed to get the user contexts...' );
                klay.viewbag.userContexts = {};
            }

            if( nk.type( acls ) === 'array' && acls.length > -1 )
            {
                var tmp = [];
                for( var i = 0; i < acls.length; i++ )
                {
                    tmp[acls[i].id] = acls[i].name;
                }

                // Set the 'selected' value
                tmp[0] = tmp[klay.viewbag.userRecord.acl];

                klay.viewbag.userACLs = tmp;
            }
            else
            {
                console.log( 'Failed to get the acls...' );
                klay.viewbag.userACLs = {};
            }

            // And invoke the callback, passing the result of our query
            if( nk.type( callback ) === 'function' )
            { // Asynchronous
                callback( request, response, klay );
            }
            else
            { // Synchronous
                return;
            }
        },
        editUser: function( request, response, callback, klay )
        {
            var mdata = null;
            if( request.posted.mdata )
            {
                mdata = JSON.parse( request.posted.mdata );
            }
            console.log( 'mdata: ' );
            console.log( mdata );

            // We need to reference the user with this one.
            var user = klay.viewbag.user;

            // We also want to display the user edit view after updating this here existing user
            var userRecord = false;

            // Prep the database object
            var db = this.dbo();

            // Prep the model for the database object
            var model = false;
            db.reset( model );

            var whereVals =
            {
                username: [ '=', user.username ],
                email: [ '=', user.email ]
            };

            // We store the user by Id, not by name
            var userId = db
            .select( "id from users" )
            .where( whereVals )
            .execute();

            // Extract the user's Id
            if( nk.type( userId ) === 'array' )
            {
                userId = userId[0].id;
            }

            model =
            {
                type: [ true, 'int', 'Type' ],
                acl: [ true, 'int', 'ACL' ],
                luts: [ true, 'text', 'Updated' ],
                luby: [ true, 'int', 'Updated by' ],
                first: [ true, 'text', 'First' ],
                middle: [ true, 'text', 'Middle' ],
                last: [ true, 'text', 'Last' ],
                email: [ true, 'text', 'Email' ],
                context: [ true, 'int', 'Context' ]
            };
            db.reset( model );

            whereVals =
            {
                id: [ '=', mdata.id ]
            };

            // Add our article now that we have the information to store it correctly
            var values = {};
            values.type = mdata.type;
            values.acl = mdata.acl;
            values.luts = new Date().toISOString().slice( 0, 19 ).replace( 'T', ' ' );
            values.luby = userId;
            values.first = mdata.first;
            values.middle = mdata.middle;
            values.last = mdata.last;
            values.email = mdata.email;
            values.context = mdata.context;

            var updated = false;
            updated = db
            .update( 'users' )
            .values( values )
            .where( whereVals )
            .execute();

            if( updated && updated >= 0 )
            {
                klay.viewbag.sysmsg = '<span class="xrm-xalert success">User "' + mdata.id + '", "' + mdata.first + ' ' + mdata.last + '", was updated successfully.</span>';

                model = klay.model.schema;
                db.reset( model );

                // Get the user
                userRecord = db
                .select
                (
                    "users"
                )
                .where( { 'id': [ '=', userId ] } )
                .execute();

                if( nk.type( userRecord ) === 'array' && userRecord.length > -1 )
                {
                    // Great, we have an array which should be full of user records
                    klay.viewbag.userRecord = userRecord[0];
                    klay.viewbag.userRecord.id = userId;
                }
                else
                {
                    console.log( 'Failed to fetch the user record...' );
                    klay.viewbag.userRecord = {};
                    klay.viewbag.userRecord.id = userId;
                }
            }
            else
            {
                console.log( 'Something went wrong when updating the user record...');
                klay.viewbag.userRecord = {};
                klay.viewbag.userRecord.id = userId;
                klay.viewbag.sysmsg = '<span class="xrm-xalert error">User record "' + mdata.id + '", "' + mdata.first + ' ' + mdata.last + '", was not updated successfully.</span>';
            }

            model = false;
            db.reset( false );

            var companies = false;
            companies = db
            .select( "* from companies" )
            .where( { owner: [ '=', klay.viewbag.userRecord.context ] } )
            .execute();

            model = false;
            db.reset( false );

            var contexts = false;
            contexts = db
            .select( "* from contexts" )
            .where( { owner: [ '=', userId ] } )
            .execute();

            model = false;
            db.reset( model );

            var acls = false;
            acls = db
            .select( "* from acls" )
            .execute();

            db.reset( model );

            klay.viewbag.userTypes = [ 'Registered', 'Registered', 'Administrator' ];
            klay.viewbag.userTypes[0] = klay.viewbag.userTypes[klay.viewbag.userRecord.type];

            if( nk.type( companies ) === 'array' && companies.length > -1 )
            {
                var tmp = [];
                for( var i = 0; i < companies.length; i++ )
                {
                    if( !companies[i].name )
                    {
                        tmp[companies[i].id] = 'None';
                    }
                    else
                    {
                        tmp[companies[i].id] = companies[i].name;
                    }
                }

                // Set the 'selected' value
                tmp[0] = tmp[contexts[klay.viewbag.userRecord.context].company];

                klay.viewbag.userCompanies = tmp;
            }
            else
            {
                console.log( 'Failed to get the user companies...' );
                klay.viewbag.userCompanies = {};
            }

            if( nk.type( contexts ) === 'array' && contexts.length > -1 )
            {
                var tmp = [];
                for( var i = 0; i < contexts.length; i++ )
                {
                    if( !contexts[i].company )
                    {
                        tmp[contexts[i].id] = 'Default';
                    }
                    else
                    {
                        if( klay.viewbag.userCompanies[contexts[i].company] )
                        {
                            tmp[contexts[i].id] = klay.viewbag.userCompanies[contexts[i].company];
                        }
                        else
                        {
                            tmp[contexts[i].id] = 'Issue!';
                        }
                    }
                }

                // Set the 'selected' value
                tmp[0] = tmp[klay.viewbag.userRecord.context];

                klay.viewbag.userContexts = tmp;
            }
            else
            {
                console.log( 'Failed to get the user contexts...' );
                klay.viewbag.userContexts = {};
            }

            if( nk.type( acls ) === 'array' && acls.length > -1 )
            {
                var tmp = [];
                for( var i = 0; i < acls.length; i++ )
                {
                    tmp[acls[i].id] = acls[i].name;
                }

                // Set the 'selected' value
                tmp[0] = tmp[klay.viewbag.userRecord.acl];

                klay.viewbag.userACLs = tmp;
            }
            else
            {
                console.log( 'Failed to get the acls...' );
                klay.viewbag.userACLs = {};
            }

            // And invoke the callback, passing the result of our query
            if( nk.type( callback ) === 'function' )
            { // Asynchronous
                callback( request, response, klay );
            }
            else
            { // Synchronous
                return klay.viewbag.userRecord;
            }
        },
        schema:
        {
            /*
             * User/edit View model
             */
            id: [ true, 'int', ''],
            type: [ true, 'int', 'Type' ],
            acl: [ true, 'int', 'ACL' ],
            ts: [ true, 'text', 'Since' ],
            luts: [ true, 'text', 'Updated' ],
            luby: [ true, 'int', 'Updated by' ],
            username: [ true, 'text', 'Username' ],
            password: [ true, 'text', 'Password' ],
            first: [ true, 'text', 'First' ],
            middle: [ true, 'text', 'Middle' ],
            last: [ true, 'text', 'Last' ],
            email: [ true, 'text', 'Email' ],
            context: [ true, 'int', 'Context' ]
        }
    },
    deleteView:
    {
        deleteUser: function( request, response, callback, klay )
        {
            var mdata = null;
            if( request.posted.mdata )
            {
                mdata = JSON.parse( request.posted.mdata );
            }

            // Prep the database object
            var db = this.dbo();

            // Prep the model for the database object
            var model = klay.model.schema;
            db.reset( model );

            var whereVals =
            {
                id: [ '=', mdata.id ]
            };

            var deleted = false;

            deleted = db
            .delete( 'mmod_cms_articles' )
            .where( whereVals )
            .execute();

            if( deleted )
            {
                if( deleted > 0 )
                {
                    klay.viewbag.sysmsg = '<span class="xrm-xalert success">Article "' + mdata.id + '" deleted successfully.</span>';
                }
                else
                {
                    klay.viewbag.sysmsg = '<span class="xrm-xalert error">Article "' + mdata.id + '" not found/deleted.</span>';
                }
            }
            else
            {
                klay.viewbag.sysmsg = '<span class="xrm-xalert error">Whoopsie! Something went wrong; Article(s) "' + mdata.id + '" not found/deleted.</span>';
            }

            // And invoke the callback, passing the result of our query
            if( nk.type( callback ) === 'function' )
            { // Asynchronous
                callback( request, response, klay );
            }
            else
            { // Synchronous
                return klay.viewbag.sysmsg;
            }
        },
        schema:
        {
            /*
             * User/delete Action model
             */
            id: [ true, 'int', '']
        }
    }
};


// Export
module.exports = userModel;