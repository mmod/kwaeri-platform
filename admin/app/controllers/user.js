/**
 * package: mmod-xrm
 * sub-package: controllers/user
 * author: Richard B. Winters <a href="mailto:rik@mmogp.com">rik AT mmogp DOT com</a>
 * copyright: 2011-2015 Massively Modified, Inc.
 * license: See Terms and EULA at https://mmogp.com/about/terms
 */


/**
 * Constructor
 *
 * @since 0.0.1
 */
function userController()
{
}


// HTTP GET /user
userController.prototype.index = function( request, response )
{
    var na = this;

    var userModel = require( '../models/user' ).user,
    model = this.modl.set( userModel );

    // Prep the viewbag
    this.klay.viewbag.title = 'MMod XRM: Manage Users';
    this.klay.viewbag.pagetitle = 'Manage Users';

    this.klay.model = require( '../models/user' ).listView;

    // Here we define the callback for our list method
    var callback = function( req, res, tk )
    {
        na.rendr( req, res );
    };

    model.list( request, response, callback, this.klay );
};


//HTTP GET /user/add
userController.prototype.add = function( request, response )
{
    var na = this;

    var usersModel = require( '../models/user' ).addView,
    model = this.modl.set( usersModel );

    this.klay.model = usersModel;
    this.klay.viewbag.pagetitle = 'Add a New User';

    // Here we define the callback for our method
    var callback = function( req, res, tk )
    {

        na.rendr( req, res );
    };

    model.getUserInputFieldOptions( request, response, callback, this.klay );
};


//HTTP POST /user/add
userController.prototype.addPost = function( request, response )
{
    var na = this;

    var articlesModel = require( '../models/articles' ).addView,
    model = this.modl.set( articlesModel );

    this.klay.model = articlesModel;
    this.klay.viewbag.pagetitle = 'Add a New Article';
    this.klay.viewbag.article = { id: '', type: '', acl: '', category: '', tags: '', title: '', alias: '', description: '', content: '', keywords: '', options: '' };

    // Here we define the callback for our list method
    var callback = function( req, res, tk )
    {
        na.rendr( req, res );
    };

    model.addArticle( request, response, callback, this.klay );
};


//HTTP GET /user/edit
userController.prototype.edit = function( request, response )
{
    var na = this;

    var usersModel = require( '../models/user' ).editView,
    model = this.modl.set( usersModel );

    this.klay.model = usersModel;

    // We just need to display a message here
    this.klay.viewbag.pagetitle = 'Users';

    // Here we define the callback for our list method
    var callback = function( req, res, tk )
    {
        na.rendr( req, res );
    };

    model.getUser( request, response, callback, this.klay );
};


//HTTP POST /user/edit
userController.prototype.editPost = function( request, response )
{
    var na = this;

    var usersModel = require( '../models/user' ).editView,
    model = this.modl.set( usersModel );

    this.klay.model = usersModel;

    // We just need to display a message here
    this.klay.viewbag.pagetitle = 'Users';

     // Here we define the callback for our list method
     var callback = function( req, res, tk )
     {
        na.rendr( req, res );
    };

    model.editUser( request, response, callback, this.klay );
};


//HTTP POST /user/delete
userController.prototype.deletePost = function( request, response )
{
    var na = this;

    var usersModel = require( '../models/user' ).deleteView,
    model = this.modl.set( usersModel );

    this.klay.model = usersModel;

    // We just need to display a message here
    this.klay.viewbag.pagetitle = 'Users';

    // Here we define the callback for our method
    var callback = function( req, res, tk )
    {
        na.rendr( req, res );
    };

    model.deleteUser( request, response, callback, this.klay );
};


// Export
module.exports = exports = userController;