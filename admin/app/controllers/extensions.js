/**
 * package: mmod-xrm
 * sub-package: controllers/extensions
 * author: Richard B. Winters <a href="mailto:rik@mmogp.com">rik AT mmogp DOT com</a>
 * copyright: 2011-2015 Massively Modified, Inc.
 * license: See Terms and EULA at https://mmogp.com/about/terms
 */


/**
 * Constructor
 *
 * @since 0.0.1
 */
function extensionsController()
{
}


// HTTP GET /extensions
extensionsController.prototype.index = function( request, response )
{
    var na = this;

    // Here we're going to want to use our database provider, when we do so
    // we'll define a callback to send along with our query
    // to support an implicit asynchronicity.
    var extensionsModel = require( '../models/extensions' ).extension,
    model = this.modl.set( extensionsModel );

    // Prep the viewbag
    this.klay.viewbag.title = 'MMod XRM: Manage Extensions';
    this.klay.viewbag.pagetitle = 'Manage Extensions';

    this.klay.model = require( '../models/extensions' ).listView;

    // Here we define the callback for our list method
    var callback = function( req, res, extensions, tk )
    {
        if( extensions )
        {
            if( !extensions.length > 0 )
            {
                console.log( 'We were unable to get a list of extensions.' );

                na.rendr( req, res );
            }
            else
            {
                //console.log( 'We were able to get a list of extensions.' );
                //console.log( extensions );
                na.klay.viewbag.extensions_list = extensions;

                na.rendr( req, res );
            }
        }
        else
        {
            console.log( "Extensions not returned" );
            na.klay.viewbag.extensions_list = [ { id: '', name: '', description: '', version: '', author: '', website: '', installed: '', enabled: '' } ];

            na.rendr( req, res );
        }
    };

    // And here we invoke the model's list method. I'm sure you can see
    // the changes you would need to make in this controller method to make things
    // synchronous instead (i.e. remove code body from callback, have it run after
    // model executes, but have model return its value to a variable within this method's
    // scope like var authenticated = mode.authenticate... callback can be left undefined.)
    model.list( request, response, callback, this.klay );
};


// HTTP GET /extensions/manage
extensionsController.prototype.manage = function( request, response )
{
    var reqmeth;
    if( request.method === 'POST' )
    {
        reqmeth = 'Post';
    }
    else
    {
        reqmeth = '';
    }

    // Get the path
    var path = request.requrl.pathname;

    // Remove leading slash if present, we check for one using the original path in the switch below to handle default url processing by the controller
    var tpath = path;
    if( tpath.charAt( 0 ) === '/' )
    {
        tpath = tpath.substr( 1 );
    }

    // And get the path parts
    var parts = tpath.split( '/' );

    // Check that we got parts
    if( parts.length > 0 )
    {
        // Great we had parts, now lets check whether there was 3 of them
        if( !parts.length >= 3 )
        {
            // This tells us an extension was not provided and so we can't possibly load it
            //parts[1] = 'dash';
        }
    }
    else
    {
        //parts[0] = tpath;
        //parts[1] = 'dash';
        // Impossible, something must have gone wrong.
    }

    // Here we attempt to load whatever extension is requested by name
    // if there is an error we display the 404 page.
    try
    {
        // And get the path parts
        var eparts = parts[2].split( '-' );
        if( !eparts.length > 1 )
        {
            // Not a valid extension namespace
            console.log( "Error: not a valid extension namespace" );
        }

        if( !parts[3] )
        {
            parts[3] = 'dash';
        }

        var etype = require( this.dextensions + '/' + eparts[0] + '/' + eparts[0] + "_" + eparts[1] + '/controllers/' + parts[3] ),
            dextension = new etype();

        dextension.isExtension = true;
        dextension.isModule = false;
        dextension.extData =
        {
            vendor: eparts[0],
            extension: eparts[0] + '_' + eparts[1]
        }
        dextension.dextensions = this.dextensions;
        dextension.config = this.config;
        dextension.modl = this.modl;
        dextension.pottr = this.pottr;
        dextension.promoter = this.promoter;
        dextension.setUserAuth = this.setUserAuth;

        // The requested action determines the view, ensure the view action specified exists and that its a function, otherwise
        // we'll set Index as the action/view - and if that's not found then a great big 404 will display :)
        if( toString.call( dextension[parts[4] + reqmeth] ) !== '[object Function]' )
        {
            parts[4] = 'index';
        }

        // Tac ?layout=0 on to the url in order to avoid wrapping the response with the layout/template
        if( Object.prototype.hasOwnProperty.call( request.requrl.query, 'layout' ) )
        {
            if( request.requrl.query.layout == 0 )
            {
                dextension.klay =
                {
                    controller: parts[3],
                    view: parts[4],
                    layoutOverride: true,
                    viewbag: this.klay.viewbag
                };
            }
        }
        else
        {
            dextension.klay =
            {
                controller: parts[3],
                view: parts[4],
                viewbag: this.klay.viewbag
            };
        }

        dextension.rendr = this.rendr;

        //console.log( dextension );
        //console.log( 'Invoking method: ' + parts[4] + reqmeth + ': ' + parts[0] + ', ' + parts[1] + ', ' + parts[2] + ', ' + parts[3] );
        // Require the controller, and use the action term within the parts array to invoke the proper controller method
        dextension[parts[4] + reqmeth]( request, response );
        //console.log( 'done invoking method: ' + parts[4] + reqmeth );
    }
    catch( error )
    {
        // And log the error ofc
        console.error( 'Extension error: ' + error + ' Derived path: ' + path + 'line: ', /\(file:[\w\d/.-]+:([\d]+)/.exec( error.stack ) );

        // If the controller can't be loaded for some reason, handle the exception by showing a 404
        require( './../../../node_modules/nk/library/controller/404' ).get( request, response );
    }
};


// Export
module.exports = exports = extensionsController;