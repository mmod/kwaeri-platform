/**
 * package: mmod-xrm
 * sub-package: controllers/system
 * author: Richard B. Winters <a href="mailto:rik@mmogp.com">rik AT mmogp DOT com</a>
 * copyright: 2011-2015 Massively Modified, Inc.
 * license: See Terms and EULA at https://mmogp.com/about/terms
 */


/**
 * Constructor
 *
 * @since 0.0.1
 */
function systemController()
{
}

// HTTP GET /system
systemController.prototype.index = function( request, response )
{
    // We just need to display a message here
    this.klay.viewbag.title = 'MMod XRM: System';
    this.klay.viewbag.pagetitle = 'System...nada.';

    this.rendr( request, response );
};


// HTTP GET /system/preferences
systemController.prototype.preferences = function( request, response )
{
    // We just need to display fields for a login here
    this.klay.viewbag.title = 'MMod XRM: System Preferences';
    this.klay.viewbag.pagetitle = 'System Preferences';

    this.rendr( request, response );
};


//HTTP GET /system/information
systemController.prototype.information = function( request, response )
{
 // We just need to display fields for a login here
 this.klay.viewbag.title = 'MMod XRM: System Information';
 this.klay.viewbag.pagetitle = 'System Information';

 this.rendr( request, response );
};


// Export
module.exports = exports = systemController;