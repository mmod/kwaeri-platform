/**
 * package: mmod-xrm
 * sub-package: controllers/theme
 * author: Richard B. Winters <a href="mailto:rik@mmogp.com">rik AT mmogp DOT com</a>
 * copyright: 2011-2015 Massively Modified, Inc.
 * license: See Terms and EULA at https://mmogp.com/about/terms
 */


/**
 * Constructor
 *
 * @since 0.0.1
 */
function themeController()
{
}


// HTTP GET /theme
themeController.prototype.index = function( request, response )
{
    // We just need to display a message here
    this.klay.viewbag.pagetitle = 'Theme the Application';

    this.rendr( request, response );
};


// Export
module.exports = exports = themeController;