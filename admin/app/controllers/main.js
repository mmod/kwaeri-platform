/**
 * package: mmod-xrm
 * sub-package: controllers/main
 * author: Richard B. Winters <a href="mailto:rik@mmogp.com">rik AT mmogp DOT com</a>
 * copyright: 2011-2015 Massively Modified, Inc.
 * license: See Terms and EULA at https://mmogp.com/about/terms
 */


/**
 * Constructor
 *
 * @since 0.0.1
 */
function mainController()
{
}


// HTTP GET /
mainController.prototype.index = function( request, response )
{
    if( !request.isAuthenticated )
    {
        response.redirect( '/account/login' );
        return;
    }

    // We need to collect all the data we need for rendering a layout response;
    // this includes a list of extensions, and any and all static content.
    //
    // The admin application will hardly ever have a non-ajax display, but if so
    // further nested data can be gathered via whatever controller is necessary.
    //
    // Regardless of anything, it is here we gather module data for the layout


    // We just need to display a welcome here for now
    this.klay.viewbag.title = 'MMod';
    this.klay.viewbag.pagetitle = 'The MMOD Online Gathering Place';
    this.klay.viewbag.testvar =
    [
        { 'a': '<b>Themes</b>', 'b': '<p>Our work in progress; a state-of-the-art platform featuring a multi-threaded non-blocking/blocking I/O.  Coming soon.</p><p><a href="#" class="btn btn-lg btn-primary" role="button">Learn more</a></p>.', 'c': 'oh' },
        { 'a': '<b>Menus</b>', 'b': '<p>A concept, and small taste of the application framework experience baked into the up-and-coming Kwaeri platform; for Node.js.</p><p><a href="#" class="btn btn-lg btn-primary" role="button">Check it out</a></p>', 'c': 'ho' },
        { 'a': '<b>Content</b>', 'b': '<p>A concept which will help us shape the future of our data integration tools for the kwaeri platform.</p><p><a href="#" class="btn btn-lg btn-primary" role="button">Learn more</a></p><p><a href="#" class="btn btn-default" role="button">On the Node</a></p>', 'c': 'yea' },
        { 'a': '<b>Extensions</b>', 'b': '<p>Our work in progress; a state-of-the-art platform featuring a multi-threaded non-blocking/blocking I/O.  Coming soon.</p><p><a href="#" class="btn btn-lg btn-primary" role="button">Learn more</a></p>.', 'c': 'oh' }
    ];

    // For testing
    this.klay.modules = {};
    this.klay.modules.sysutil = true;
    this.klay.modules.menu = 'Yes';
    this.klay.modules.content = 1;
    this.klay.modules.footer = 'Ofc';

    this.rendr( request, response );
};


// Export
module.exports = exports = mainController;