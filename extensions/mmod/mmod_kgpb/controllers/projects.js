/**
 * package: mmod-kgpb
 * sub-package: controllers/projects
 * author: Richard B. Winters <a href="mailto:rik@mmogp.com">rik AT mmogp DOT com</a>
 * copyright: 2011-2015 Massively Modified, Inc.
 * license: See Terms and EULA at https://mmogp.com/about/terms
 */


/**
 * Constructor
 *
 * @since 0.0.1
 */
function projectsController()
{
}


/**
 * Method for rendering module content
 *
 * @param request
 * @param response
 *
 * @return string The module content
 *
 * @since 0.0.1
 */
projectsController.prototype.renderModuleContent = function( request, response )
{
    // Prep the viewbag
    this.klay.viewbag.projectsSettings = JSON.parse( this.moduleParams );

    // Here we're going to want to use our database provider, when we do so
    // we'll define a callback to send along with our query
    // to support an implicit asynchronicity.
    if( this.klay.viewbag.hasOwnProperty( 'projectsSettings' ) )
    {
        if( this.klay.viewbag.projectsSettings.hasOwnProperty( 'project_options' ) )
        {
            var projectType = this.klay.viewbag.projectsSettings.project_options.type;

            switch( projectType )
            {
                case 1:
                {
                    //this.klay.controller = "lytebox";
                    this.klay.view = "project";
                }break;
            }
            console.log( 'after the project switch...' );
            console.log( this.klay.view );
            console.log( projectType );
        }
        else
        {
            console.log( 'project_options not found...' );
            //this.klay.view = "utility";
            this.klay.viewbag.projects = [ "" ];
        }
    }
    else
    {
        console.log( 'projectsSettings not found in the viewbag...' );
        //this.klay.view = "utility";
        this.klay.viewbag.projects = [ "" ];
    }

    this.rendr( request, response );
};


// Export
module.exports = exports = projectsController;
