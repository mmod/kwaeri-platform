/**
 * package: mmod-cms
 * sub-package: controllers/menu
 * author: Richard B. Winters <a href="mailto:rik@mmogp.com">rik AT mmogp DOT com</a>
 * copyright: 2011-2015 Massively Modified, Inc.
 * license: See Terms and EULA at https://mmogp.com/about/terms
 */


/**
 * Constructor
 *
 * @since 0.0.1
 */
function menuController()
{
}


/**
 * Method for rendering module content
 *
 * @param request
 * @param response
 *
 * @return string The module content
 *
 * @since 0.0.1
 */
menuController.prototype.renderModuleContent = function( request, response, klay )
{
    console.log( 'in renderModuleContent()...' );
    var na = this;

    // Here we're going to want to use our database provider, when we do so
    // we'll define a callback to send along with our query
    // to support an implicit asynchronicity.
    var menuModel = require( '../models/menu' ).menu,
        model = this.modl.set( menuModel );

    console.log( 'was it the model?' );

    // Prep the viewbag
    this.klay.viewbag.menuSettings = JSON.parse( this.moduleParams );
    //console.log( 'menuSettings: ' );
    //console.log( this.klay.viewbag.menuSettings );

    // Here we define the callback for our list method
    var callback = function( req, res, tk )
    {
        if( tk.viewbag.menuItems )
        {
            if( !tk.viewbag.menuItems.length > 0 )
            {
                console.log( 'We were unable to get the menu module content.' );
                na.klay.viewbag.menuitems = [];
                na.klay.viewbag.authddmenuitems = [];
                na.klay.viewbag.ddmenuitems = [];
                na.klay.viewbag.authlinks = [];
                na.klay.viewbag.links = [];

                na.rendr( req, res );
            }
            else
            {
                console.log( 'We were able to get a list of menu items: ' );
                //console.log( tk.viewbag.menuItems );
                na.klay.viewbag.menuitems = tk.viewbag.menuItems;
                na.klay.viewbag.authddmenuitems = na.klay.viewbag.menuSettings.authDropdownMenu;
                na.klay.viewbag.ddmenuitems = na.klay.viewbag.menuSettings.dropdownMenu;
                na.klay.viewbag.authlinks = na.klay.viewbag.menuSettings.authLinks;
                na.klay.viewbag.links = na.klay.viewbag.menuSettings.links;

                na.rendr( req, res );
            }
        }
        else
        {
            console.log( "Menu content not returned" );
            na.klay.viewbag.menuitems = [];
            na.klay.viewbag.authddmenuitems = [];
            na.klay.viewbag.ddmenuitems = [];
            na.klay.viewbag.authlinks = [];
            na.klay.viewbag.links = [];

            na.rendr( req, res );
        }
    };

    // And here we invoke the model's list method. I'm sure you can see
    // the changes you would need to make in this controller method to make things
    // synchronous instead (i.e. remove code body from callback, have it run after
    // model executes, but have model return its value to a variable within this method's
    // scope like var authenticated = mode.authenticate... callback can be left undefined.)
    model.getMenu( request, response, callback, this.klay );
};


// Export
module.exports = exports = menuController;
