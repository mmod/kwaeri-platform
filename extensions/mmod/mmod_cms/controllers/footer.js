/**
 * package: mmod-cms
 * sub-package: controllers/footer
 * author: Richard B. Winters <a href="mailto:rik@mmogp.com">rik AT mmogp DOT com</a>
 * copyright: 2011-2015 Massively Modified, Inc.
 * license: See Terms and EULA at https://mmogp.com/about/terms
 */


/**
 * Constructor
 *
 * @since 0.0.1
 */
function footerController()
{
}


/**
 * Method for rendering module content
 *
 * @param request
 * @param response
 *
 * @return string The module content
 *
 * @since 0.0.1
 */
footerController.prototype.renderModuleContent = function( request, response )
{
    // Prep the viewbag
    this.klay.viewbag.footerSettings = JSON.parse( this.moduleParams );

    // Here we're going to want to use our database provider, when we do so
    // we'll define a callback to send along with our query
    // to support an implicit asynchronicity.
    //var footerModel = require( '../models/footer' ).footer,
    //    model = this.modl.set( footerModel );
    if( this.klay.viewbag.hasOwnProperty( 'footerSettings' ) )
    {
        if( this.klay.viewbag.footerSettings.hasOwnProperty( 'copyrights' ) )
        {
            this.klay.viewbag.footercopyrights = this.klay.viewbag.footerSettings.copyrights;
            for( var copyright in this.klay.viewbag.footercopyrights )
            {
                if( this.klay.viewbag.footercopyrights[copyright].date )
                {
                    var date = new Date();
                    this.klay.viewbag.footercopyrights[copyright].year = date.getFullYear();
                }
                else
                {
                    this.klay.viewbag.footercopyrights[copyright].year = "";
                }

                if( this.klay.viewbag.footercopyrights[copyright].range )
                {
                    this.klay.viewbag.footercopyrights[copyright].range = this.klay.viewbag.footercopyrights[copyright].dateStart;
                }
                else
                {
                    this.klay.viewbag.footercopyrights[copyright].range = "";
                }
            }
        }
        else
        {
            this.klay.viewbag.footercopyrights = [ "" ];
        }

        if( this.klay.viewbag.footerSettings.hasOwnProperty( 'links' ) )
        {
            this.klay.viewbag.footerlinks = this.klay.viewbag.footerSettings.links;
        }
        else
        {
            this.klay.viewbag.footerlinks = [ "" ];
        }

        if( this.klay.viewbag.footerSettings.hasOwnProperty( 'icons' ) )
        {
            this.klay.viewbag.footericons = this.klay.viewbag.footerSettings.icons;
        }
        else
        {
            this.klay.viewbag.footericons = [ "" ];
        }

        if( this.klay.viewbag.footerSettings.hasOwnProperty( 'socials' ) )
        {
            this.klay.viewbag.footersocials = this.klay.viewbag.footerSettings.socials;
        }
        else
        {
            this.klay.viewbag.footersocials = [ "" ];
        }
    }
    else
    {
        this.klay.viewbag.footercopyrights = [ "" ];
        this.klay.viewbag.footerlinks = [ "" ];
        this.klay.viewbag.footericons = [ "" ];
        this.klay.viewbag.footersocials = [ "" ];
    }

    this.rendr( request, response );
};


// Export
module.exports = exports = footerController;
