/**
 * package: mmod-cms
 * sub-package: controllers/view
 * author: Richard B. Winters <a href="mailto:rik@mmogp.com">rik AT mmogp DOT com</a>
 * copyright: 2011-2015 Massively Modified, Inc.
 * license: See Terms and EULA at https://mmogp.com/about/terms
 */


// Deps
var nk = require( 'nk' ),
nk = new nk();  // When we do not pass an argument to the constructor, we
                // get only the core facilities ( .type, .extend, .each,
                // .hash, etc )


/**
 * Constructor
 *
 * @since 0.0.1
 */
function viewController()
{
}


viewController.prototype.index = function( request, response )
{

    // Get the path
    var path = request.requrl.pathname;

    // Remove leading slash if present, we check for one using the original path in the switch below to handle default url processing by the controller
    var tpath = path;
    if( tpath.charAt( 0 ) === '/' )
    {
        tpath = tpath.substr( 1 );
    }

    // And get the path parts
    var parts = tpath.split( '/' );
    var action = "view";

    // Check that we got parts
    if( parts.length > 2 )
    {
        // This tells us an action was provided, let's set it
        action = parts[2];
    }

    //console.log( 'PARTS[2]: ' + action );

    switch( action )
    {
        case 'view':
        {
            var viewModel = require( '../models/view' ).view,
            model = this.modl.set( viewModel );

            this.klay.model = require( '../models/view' ).view;

            // Prep the viewbag
            this.klay.viewbag.viewSettings = this.extData.params[this.extData.params.length - 1];

            // We just need to display a message here
            this.klay.viewbag.title = 'MMOD';
            this.klay.viewbag.pagetitle = '';

            this.klay.view = "view";

            var na = this;

            // Here we define the callback for our list method
            var callback = function( req, res, tk )
            {
                //console.log( tk.viewbag.view );
                if( tk.viewbag.view )
                {
                    na.klay.viewbag.view = tk.viewbag.view;
                }

                na.rendr( req, res );
            };

            model.view( request, response, callback, this.klay );
        }break;

        case 'add':
        {

        }break;

        case 'edit':
        {

        }break;

        case 'delete':
        {

        }break;
    }
};


// Export
module.exports = exports = viewController;
