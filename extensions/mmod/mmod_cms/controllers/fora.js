/**
 * package: mmod-cms
 * sub-package: controllers/blog
 * author: Richard B. Winters <a href="mailto:rik@mmogp.com">rik AT mmogp DOT com</a>
 * copyright: 2011-2015 Massively Modified, Inc.
 * license: See Terms and EULA at https://mmogp.com/about/terms
 */


// Deps
var nk = require( 'nk' ),
nk = new nk();  // When we do not pass an argument to the constructor, we
                // get only the core facilities ( .type, .extend, .each,
                // .hash, etc )


/**
 * Constructor
 *
 * @since 0.0.1
 */
function foraController()
{
}


foraController.prototype.index = function( request, response )
{
    var na = this;

    var foraModel = require( '../models/fora' ).fora,
    model = this.modl.set( foraModel );

    this.klay.model = require( '../models/fora' ).fora;

    // We just need to display a message here
    this.klay.viewbag.title = 'MMOD';
    this.klay.viewbag.pagetitle = 'Fora';

    // Here we define the callback for our list method
    var callback = function( req, res, tk )
    {
        if( tk.viewbag.foraentries && !nk.isEmptyObject( tk.viewbag.foraentries ) )
        {
            na.klay.viewbag.foraentries = tk.viewbag.foraentries;
        }

        na.rendr( req, res );
    };

    model.list( request, response, callback, this.klay );
};


foraController.prototype.blog = function( request, response )
{

    // Get the path
    var path = request.requrl.pathname;

    // Remove leading slash if present, we check for one using the original path in the switch below to handle default url processing by the controller
    var tpath = path;
    if( tpath.charAt( 0 ) === '/' )
    {
        tpath = tpath.substr( 1 );
    }

    // And get the path parts
    var parts = tpath.split( '/' );
    var action = "";

    // Check that we got parts
    if( parts.length > 2 )
    {
        // This tells us an action was provided, let's set it
        action = parts[2];
    }
    else
    {
        var blogId = false;
        if( Object.prototype.hasOwnProperty.call( request.requrl.query, 'blogId' ) )
        {
            blogId = request.requrl.query.blogId;
            if( action === "" )
            {
                action = "view"
            }
        }
        else
        {
            if( Object.prototype.hasOwnProperty.call( request.requrl.query, 'recordId' ) )
            {
                // User came here from the universal edit mechanism  on the list page
                blogId = request.requrl.query.recordId;
                if( action === "" )
                {
                    action = "view"
                }
            }
            else
            {
                console.log( 'Fora Blog/Record ID not found in query string!' );
            }
        }
    }

    //console.log( 'PARTS[2]: ' + action );

    switch( action )
    {
        case 'view':
        {
            var foraModel = require( '../models/fora' ).blog,
            model = this.modl.set( foraModel );

            this.klay.model = require( '../models/fora' ).blog;

            // We just need to display a message here
            this.klay.viewbag.title = 'MMOD';
            this.klay.viewbag.pagetitle = 'Fora';

            this.klay.view = "view-blog";

            var na = this;

            // Here we define the callback for our list method
            var callback = function( req, res, tk )
            {
                if( tk.viewbag.blogentry && !nk.isEmptyObject( tk.viewbag.blogentry ) )
                {
                    na.klay.viewbag.blogentry = tk.viewbag.blogentry;
                }

                na.rendr( req, res );
            };

            model.view( request, response, callback, this.klay );
        }break;

        case 'add':
        {

        }break;

        case 'edit':
        {

        }break;

        case 'delete':
        {

        }break;

        default:
        {
            var na = this;

            var foraModel = require( '../models/fora' ).fora,
            model = this.modl.set( foraModel );

            this.klay.model = require( '../models/fora' ).fora;

            // We just need to display a message here
            this.klay.viewbag.title = 'MMOD';
            this.klay.viewbag.pagetitle = 'Fora';

            this.klay.view = "index";

            // Here we define the callback for our list method
            var callback = function( req, res, tk )
            {
                if( tk.viewbag.foraentries && !nk.isEmptyObject( tk.viewbag.foraentries ) )
                {
                    na.klay.viewbag.foraentries = tk.viewbag.foraentries;
                }

                na.rendr( req, res );
            };

            model.list( request, response, callback, this.klay );
        }break;
    }
};


/**
 * Method for rendering module content
 *
 * @param request
 * @param response
 *
 * @return string The module content
 *
 * @since 0.0.1
 */
foraController.prototype.renderModuleContent = function( request, response, klay )
{
    var na = this;

    // Here we're going to want to use our database provider, when we do so
    // we'll define a callback to send along with our query
    // to support an implicit asynchronicity.
    var menuModel = require( '../models/menu' ).menu,
        model = this.modl.set( menuModel );

    console.log( 'was it the model?' );

    // Prep the viewbag
    this.klay.viewbag.menuSettings = JSON.parse( this.moduleParams );
    //console.log( 'menuSettings: ' );
    //console.log( this.klay.viewbag.menuSettings );

    // Here we define the callback for our list method
    var callback = function( req, res, tk )
    {
        if( tk.viewbag.menuItems )
        {
            if( !tk.viewbag.menuItems.length > 0 )
            {
                console.log( 'We were unable to get the menu module content.' );
                na.klay.viewbag.menuitems = [];
                na.klay.viewbag.authddmenuitems = [];
                na.klay.viewbag.ddmenuitems = [];
                na.klay.viewbag.authlinks = [];
                na.klay.viewbag.links = [];


                na.rendr( req, res );
            }
            else
            {
                console.log( 'We were able to get a list of menu items: ' );
                //console.log( tk.viewbag.menuItems );
                na.klay.viewbag.menuitems = tk.viewbag.menuItems;
                na.klay.viewbag.authddmenuitems = na.klay.viewbag.menuSettings.authDropdownMenu;
                na.klay.viewbag.ddmenuitems = na.klay.viewbag.menuSettings.dropdownMenu;
                na.klay.viewbag.authlinks = na.klay.viewbag.menuSettings.authLinks;
                na.klay.viewbag.links = na.klay.viewbag.menuSettings.links;

                na.rendr( req, res );
            }
        }
        else
        {
            console.log( "Menu content not returned" );
            na.klay.viewbag.menuitems = [];
            na.klay.viewbag.authddmenuitems = [];
            na.klay.viewbag.ddmenuitems = [];
            na.klay.viewbag.authlinks = [];
            na.klay.viewbag.links = [];

            na.rendr( req, res );
        }
    };

    // And here we invoke the model's list method. I'm sure you can see
    // the changes you would need to make in this controller method to make things
    // synchronous instead (i.e. remove code body from callback, have it run after
    // model executes, but have model return its value to a variable within this method's
    // scope like var authenticated = mode.authenticate... callback can be left undefined.)
    model.getMenu( request, response, callback, this.klay );
};


// Export
module.exports = exports = foraController;
