/**
 * package: mmod-cms
 * sub-package: models/menu
 * author: Richard B. Winters <a href="mailto:rik@mmogp.com">rik AT mmogp DOT com</a>
 * copyright: 2011-2015 Massively Modified, Inc.
 * license: See Terms and EULA at https://mmogp.com/about/terms
 */


// Deps
var nk = require( 'nk' ),
nk = new nk();  // When we do not pass an argument to the constructor, we
                // get only the core facilities ( .type, .extend, .each,
                // .hash, etc )


/**
 * Defines the menu model
 *
 * @since 0.0.1
 */
var menuModel =
{
    menu:
    {
        getMenu: function( request, response, callback, klay )
        {
            //console.log( 'Were in the model!' );
            // Prep the database object
            var db = this.dbo();

            // Rid the DBO of any models currently set.
            db.reset();

            // Get a list of all menus
            var menuItems = db
            .query( "select * from `mmod-cms-menu-items` " +
                    "where menu='" + klay.viewbag.menuSettings.menu + "' and enabled='2' and parent='0';" )
            .execute();

            console.log( 'Was it the query?' );
            //console.log( 'Menu Items: ' );
            //console.log( menuItems );

            if( nk.type( menuItems ) === 'array' && menuItems.length > 0 )
            {
                // Great, we have an array which should be full of menu records
                klay.viewbag.enabledTypes = [ 'No', 'No', 'Yes' ];
                for( var menuItem in menuItems )
                {
                    menuItems[menuItem].enabled = klay.viewbag.enabledTypes[menuItems[menuItem].enabled];
                    menuItems[menuItem].isview = false;

                    var menuItemParts = menuItems[menuItem].href;
                    if( menuItemParts.charAt( 0 ) === '/' )
                    {
                        menuItemParts = menuItemParts.substr( 1 );
                    }

                    menuItemParts = menuItemParts.split( '/' );
                    if( nk.type( menuItemParts ) === 'array' && menuItemParts.length > 0 )
                    {
                        if( menuItemParts[0] === 'view')
                        {
                            menuItems[menuItem].isview = true;
                        }
                        else
                        {
                            menuItems[menuItem].isview = false;
                        }
                    }
                }

                klay.viewbag.menuItems = menuItems;
            }
            else
            {
                console.log( 'Failed to get the menu...' );
                klay.viewbag.menuItems = {};
            }

            // And invoke the callback, passing the result of our query
            if( nk.type( callback ) === 'function' )
            { // Asynchronous
                callback( request, response, klay );
            }
            else
            { // Synchronous
                return menuItems;
            }
        },
        schema:
        {
            /*
             * Menu Model
             */
            id: [ true, 'int', ''],
            ts: [ true, 'text', 'Created' ],
            luts: [ true, 'text', 'Last Updated' ],
            luby: [ true, 'text', 'Last Updated by' ],
            title: [ true, 'text', 'Title' ],
            description: [ true, 'text', 'Description' ],
            enabled: [ true, 'int', 'Enabled' ],
            options: [ true, 'text', 'Options' ]
        }
    }
};


// Export
module.exports = menuModel;