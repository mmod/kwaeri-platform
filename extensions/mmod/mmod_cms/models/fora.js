/**
 * package: mmod-cms
 * sub-package: models/fora
 * author: Richard B. Winters <a href="mailto:rik@mmogp.com">rik AT mmogp DOT com</a>
 * copyright: 2011-2015 Massively Modified, Inc.
 * license: See Terms and EULA at https://mmogp.com/about/terms
 */


// Deps
var nk = require( 'nk' ),
nk = new nk();  // When we do not pass an argument to the constructor, we
                // get only the core facilities ( .type, .extend, .each,
                // .hash, etc )


/**
 * Defines the blog model
 *
 * @since 0.0.1
 */
var foraModel =
{
    fora:
    {
        list: function( request, response, callback, klay )
        {
            // Prep the database object
            var db = this.dbo();

            // Rid the DBO of any stored data.
            var model = klay.model.schema;
            db.reset( model );

            var whereVals =
            {
                type: [ '=', 2 ],
                published: [ '=', 1 ]
            };

            // Get a list of all articles using a prepared statement
            var fora = db
            .select( "`mmod-cms-articles`" )
            .where( whereVals )
            .execute();

            if( nk.type( fora ) === 'array' )
            {
                // Great, we have an array which should be full of user records
                for( var blog in fora )
                {
                    // Get the author's name
                    db.reset( false );

                    var author = db
                    .query( "select first, middle, last from users where id='" + fora[blog].owner + "';" )
                    .execute();

                    if( author && !nk.isEmptyObject( author ) )
                    {
                        fora[blog].author = author[0].first;

                        if( author[0].middle && author[0].middle != "" )
                        {
                            fora[blog].author = fora[blog].author + " " + author[0].middle;
                        }

                        fora[blog].author = fora[blog].author + " " + author[0].last;
                    }

                    // Parse tags for proper rendering
                    var tmp = '';
                    var tags = JSON.parse( fora[blog].tags );
                    for( var tag in tags )
                    {
                        tmp += '<span class="badge tag">' + tags[tag] + '</span> ';
                    }
                    fora[blog].tags = tmp;
                }

                klay.viewbag.foraentries = fora;
            }
            else
            {
                console.log( 'Failed to get a list of fora blogs...' );
                klay.viewbag.foraentries = {};
            }

            // And invoke the callback, passing the result of our query
            if( nk.type( callback ) === 'function' )
            { // Asynchronous
                callback( request, response, klay );
            }
            else
            { // Synchronous
                return fora;
            }
        },
        schema:
        {
            /*
             * Fora Blog Model
             */
            id: [ true, 'int', ''],
            type: [ true, 'int', 'Type' ],
            published: [ true, 'int', 'Published' ],
            acl: [ true, 'int', 'ACL' ],
            ts: [ true, 'text', 'Created' ],
            owner: [ true, 'int', 'Owner' ],
            luts: [ true, 'text', 'Last Updated' ],
            luby: [ true, 'text', 'Last Updated by' ],
            category: [ true, 'int', 'Category' ],
            tags: [true, 'text', 'Tags' ],
            title: [ true, 'text', 'Title' ],
            alias: [ true, 'text', 'Alias' ],
            description: [ true, 'text', 'Description' ],
            content: [ true, 'text', 'Article Content' ],
            keywords: [ true, 'text', 'Keywords' ],
            options: [ true, 'text', 'Options' ]
        }
    },
    blog:
    {
        view: function( request, response, callback, klay )
        {
            var blogId = false;
            if( Object.prototype.hasOwnProperty.call( request.requrl.query, 'blogId' ) )
            {
                blogId = request.requrl.query.blogId;
            }
            else
            {
                if( Object.prototype.hasOwnProperty.call( request.requrl.query, 'recordId' ) )
                {
                    // User came here from the universal edit mechanism  on the list page
                    blogId = request.requrl.query.recordId;
                }
                else
                {
                    console.log( 'Fora Blog/Record ID not found in query string!' );
                }
            }

            var blogEntry = false;

            // Prep the database object
            var db = this.dbo();

            // Rid the DBO of any stored data.
            var model = klay.model.schema;
            db.reset( model );

            var whereVals =
            {
                id: [ '=', blogId ],
                type: [ '=', 2 ],
                published: [ '=', 1 ]
            };

            // Get the fora blog entry using a prepared statement
            blogEntry = db
            .select( "`mmod-cms-articles`" )
            .where( whereVals )
            .execute();

            if( nk.type( blogEntry ) === 'array' )
            {
                // Great, we have an array which should have the fora item
                // Get the author's name
                db.reset( false );

                var author = db
                .query( "select first, middle, last from users where id='" + blogEntry[0].owner + "';" )
                .execute();

                if( author && !nk.isEmptyObject( author ) )
                {
                    blogEntry[0].author = author[0].first;

                    if( author[0].middle && author[0].middle != "" )
                    {
                        blogEntry[0].author = blogEntry[0].author + " " + author[0].middle;
                    }

                    blogEntry[0].author = blogEntry[0].author + " " + author[0].last;
                }

                // Format timestamps for proper rendering
                var timestamp = '';
                var mysqlstampparts = blogEntry[0].ts.split( " " );
                var mysqlstamp = mysqlstampparts[0].split( "-" );
                if( nk.type( mysqlstamp ) === 'array' && mysqlstamp.length == 3 )
                {
                    if( mysqlstamp[1].charAt( 0 ) === '0' )
                    {
                        timestamp = mysqlstamp[1].substr( 1 );
                    }
                    else
                    {
                        timestamp = mysqlstamp[1];
                    }

                    timestamp = timestamp + '/';

                    if( mysqlstamp[2].charAt( 0 ) === '0' )
                    {
                        timestamp += mysqlstamp[2].substr( 1 );
                    }
                    else
                    {
                        timestamp += mysqlstamp[2];
                    }

                    timestamp = timestamp + '/';

                    if( mysqlstamp[0].charAt( 0 ) === '0' )
                    {
                        timestamp += mysqlstamp[0].substr( 1 );
                    }
                    else
                    {
                        timestamp += mysqlstamp[0];
                    }
                }

                blogEntry[0].ts = timestamp;

                var lutimestamp = timestamp;
                var mysqllustampparts = blogEntry[0].luts.split( " " );
                var mysqllustamp = mysqllustampparts[0].split( "-" );
                if( nk.type( mysqllustamp ) === 'array' && mysqllustamp.length == 3 )
                {
                    if( mysqllustamp[1].charAt( 0 ) === '0' )
                    {
                        lutimestamp = mysqllustamp[1].substr( 1 );
                    }
                    else
                    {
                        lutimestamp = mysqllustamp[1];
                    }

                    lutimestamp = lutimestamp + '/';

                    if( mysqllustamp[2].charAt( 0 ) === '0' )
                    {
                        lutimestamp += mysqllustamp[2].substr( 1 );
                    }
                    else
                    {
                        lutimestamp += mysqllustamp[2];
                    }

                    lutimestamp = lutimestamp + '/';

                    if( mysqllustamp[0].charAt( 0 ) === '0' )
                    {
                        lutimestamp += mysqllustamp[0].substr( 1 );
                    }
                    else
                    {
                        lutimestamp += mysqllustamp[0];
                    }
                }

                blogEntry[0].luts = lutimestamp;

                // Parse tags for proper rendering
                var tmp = '';
                var tags = JSON.parse( blogEntry[0].tags );
                for( var tag in tags )
                {
                    tmp += '<span class="badge tag">' + tags[tag] + '</span> ';
                }
                blogEntry[0].tags = tmp;

                klay.viewbag.blogentry = blogEntry;
            }
            else
            {
                console.log( 'Failed to get the fora blog entry...' );
                klay.viewbag.blogentry = {};
            }

            // And invoke the callback, passing the result of our query
            if( nk.type( callback ) === 'function' )
            { // Asynchronous
                callback( request, response, klay );
            }
            else
            { // Synchronous
                return blogEntry;
            }
        },
        schema:
        {
            /*
             * Fora Blog Model
             */
            id: [ true, 'int', ''],
            type: [ true, 'int', 'Type' ],
            published: [ true, 'int', 'Published' ],
            acl: [ true, 'int', 'ACL' ],
            ts: [ true, 'text', 'Created' ],
            owner: [ true, 'int', 'Owner' ],
            luts: [ true, 'text', 'Last Updated' ],
            luby: [ true, 'text', 'Last Updated by' ],
            category: [ true, 'int', 'Category' ],
            tags: [true, 'text', 'Tags' ],
            title: [ true, 'text', 'Title' ],
            alias: [ true, 'text', 'Alias' ],
            description: [ true, 'text', 'Description' ],
            content: [ true, 'text', 'Article Content' ],
            keywords: [ true, 'text', 'Keywords' ],
            options: [ true, 'text', 'Options' ]
        }
    }
};


// Export
module.exports = foraModel;