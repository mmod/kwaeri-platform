/**
 * package: mmod-cms
 * sub-package: models/menu-item-types
 * author: Richard B. Winters <a href="mailto:rik@mmogp.com">rik AT mmogp DOT com</a>
 * copyright: 2011-2015 Massively Modified, Inc.
 * license: See Terms and EULA at https://mmogp.com/about/terms
 */


// Deps
var nk = require( 'nk' ),
nk = new nk();  // When we do not pass an argument to the constructor, we
                // get only the core facilities ( .type, .extend, .each,
                // .hash, etc )


/**
 * Defines the menu-item-types model
 *
 * @since 0.0.1
 */
var menuItemTypesModel =
{
    menuItemTypes:
    {
        schema:
        {
            /*
             * Menu-Item-Type Model
             */
            id: [ true, 'int', ''],
            acl: [ true, 'int', 'acl' ],
            ts: [ true, 'text', 'Created' ],
            luts: [ true, 'text', 'Last Updated' ],
            luby: [ true, 'text', 'Last Updated by' ],
            title: [ true, 'text', 'Title' ],
            description: [ true, 'text', 'Description' ],
            parent: [ true, 'int', 'Parent' ],
            enabled: [ true, 'int', 'Enabled' ],
            options: [ true, 'text', 'Options' ]
        }
    }
};


// Export
module.exports = menuItemTypesModel;