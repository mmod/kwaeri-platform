/**
 * package: mmod-cms
 * sub-package: models/menu-items
 * author: Richard B. Winters <a href="mailto:rik@mmogp.com">rik AT mmogp DOT com</a>
 * copyright: 2011-2015 Massively Modified, Inc.
 * license: See Terms and EULA at https://mmogp.com/about/terms
 */


// Deps
var nk = require( 'nk' ),
nk = new nk();  // When we do not pass an argument to the constructor, we
                // get only the core facilities ( .type, .extend, .each,
                // .hash, etc )


/**
 * Defines the menu-items model
 *
 * @since 0.0.1
 */
var menusItemsModel =
{
    menuItems:
    {
        listAll: function( request, response, callback, klay )
        {
            var menuId = false;

            if( Object.prototype.hasOwnProperty.call( request.requrl.query, 'menuId' ) )
            {
                menuId = request.requrl.query.menuId;
            }
            else
            {
                if( Object.prototype.hasOwnProperty.call( request.requrl.query, 'parentId' ) )
                {
                    // User came here from the universal edit mechanism  on the list page
                    menuId = request.requrl.query.parentId;
                }
                else
                {
                    console.log( 'Menu/Parent Id not found in query string!' );
                }
            }

            var menuItems = false;

            if( menuId )
            {
                // Prep the database object
                var db = this.dbo();

                // Prepare the model for the database
                var model = klay.model.schema;
                db.reset( model );

                // Get a list of all menu items for the menu
                menuItems = db
                .select( "mmod_cms_menu_items" )
                .where( { menu: [ '=', menuId ] } )
                .execute();

                model = false;
                db.reset( false );

                var menuItemTypes = false;
                menuItemTypes = db
                .select( "* from mmod_cms_menu_item_types" )
                .execute();

                model = require( './menus' ).menus.schema;
                db.reset( model );

                var menu = false;
                menu = db
                .select( "mmod_cms_menus" )
                .where( { id: [ '=', menuId ] } )
                .execute();

                model = false;
                db.reset( model );

                var acls = false;
                acls = db
                .select( "* from acls" )
                .execute();

                db.reset( model );
            }

            if( nk.type( menuItems ) === 'array' && menuItems.length > -1 )
            {
                // Great, we have an array which should be full of menu-item records
                var tmp = [];
                tmp.push( "None" );
                for( var i = 0; i < menuItems.length; i++ )
                {
                    tmp[menuItems[i].id] = menuItems[i].title;
                }

                klay.viewbag.menuItemParents = tmp;
                klay.viewbag.menuItems = menuItems;
                klay.viewbag.menuId = menuId;
            }
            else
            {
                console.log( 'Failed to get the menuItems...' );
                klay.viewbag.menuItemParents = {};
                klay.viewbag.menuItems = {};
                klay.viewbag.menuId = menuId;
            }

            klay.viewbag.enabledTypes = [ 'No', 'No', 'Yes' ];

            if( nk.type( menuItemTypes ) === 'array' && menuItemTypes.length > -1 )
            {
                var tmp = [];
                for( var i = 0; i < menuItemTypes.length; i++ )
                {
                    tmp[menuItemTypes[i].id] = menuItemTypes[i].title;
                }

                tmp[0] = tmp[1];

                klay.viewbag.menuItemTypes = tmp;
            }
            else
            {
                console.log( 'Failed to get the menuItemTypes...' );
                klay.viewbag.menuItemTypes = {};
            }

            if( nk.type( acls ) === 'array' && acls.length > -1 )
            {
                var tmp = [];
                for( var i = 0; i < acls.length; i++ )
                {
                    tmp[acls[i].id] = acls[i].name;
                }

                tmp[0] = tmp[1];

                klay.viewbag.menuItemACLs = tmp;
            }
            else
            {
                console.log( 'Failed to get the acls...' );
                klay.viewbag.menuItemACLs = {};
            }

            // Let's prep those records for what were going to use them for
            for( menu_item in klay.viewbag.menuItems )
            {
                klay.viewbag.menuItems[menu_item].type = klay.viewbag.menuItemTypes[klay.viewbag.menuItems[menu_item].type];
                klay.viewbag.menuItems[menu_item].enabled = klay.viewbag.enabledTypes[klay.viewbag.menuItems[menu_item].enabled];
                klay.viewbag.menuItems[menu_item].acl = klay.viewbag.menuItemACLs[klay.viewbag.menuItems[menu_item].acl];
                klay.viewbag.menuItems[menu_item].parent = klay.viewbag.menuItemParents[klay.viewbag.menuItems[menu_item].parent];
            }

            // And invoke the callback, passing the result of our query
            if( nk.type( callback ) === 'function' )
            { // Asynchronous
                callback( request, response, klay );
            }
            else
            { // Synchronous
                return;
            }
        },
        schema:
        {
            /*
             * Menu-Item Model
             */
            id: [ true, 'int', ''],
            type: [ true, 'int', 'Type' ],
            acl: [ true, 'int', 'acl' ],
            ts: [ true, 'text', 'Created' ],
            luts: [ true, 'text', 'Last Updated' ],
            luby: [ true, 'text', 'Last Updated by' ],
            title: [ true, 'text', 'Title' ],
            description: [ true, 'text', 'Description' ],
            menu: [ true, 'int', 'Menu' ],
            parent: [ true, 'int', 'Parent' ],
            enabled: [ true, 'int', 'Enabled' ],
            options: [ true, 'text', 'Options' ]
        }
    },
    listView:
    {
        schema:
        {
            /*
             * Menu-Item Model
             */
            id: [ true, 'int', ''],
            type: [ true, 'int', 'Type' ],
            acl: [ true, 'int', 'acl' ],
            ts: [ true, 'text', 'Created' ],
            luts: [ true, 'text', 'Last Updated' ],
            luby: [ true, 'text', 'Last Updated by' ],
            title: [ true, 'text', 'Title' ],
            description: [ true, 'text', 'Description' ],
            menu: [ true, 'int', 'Menu' ],
            parent: [ true, 'int', 'Parent' ],
            enabled: [ true, 'int', 'Enabled' ],
            options: [ true, 'text', 'Options' ]
        }
    },
    addView:
    {
        getMenuItemInputFieldOptions: function( request, response, callback, klay )
        {
            var menuId = false;

            if( Object.prototype.hasOwnProperty.call( request.requrl.query, 'menuId' ) )
            {
                menuId = request.requrl.query.menuId;
            }
            else
            {
                if( Object.prototype.hasOwnProperty.call( request.requrl.query, 'parentId' ) )
                {
                    // User came here from the universal add mechanism  on the list page
                    menuId = request.requrl.query.parentId;
                }
                else
                {
                    console.log( 'Menu/Parent Id not found in query string!' );
                }
            }

            var query_good = true;

            // Prep the database object
            var db = this.dbo();

            // Prep the model for the database object
            var model = false;

            klay.viewbag.enabledTypes = [ 'No', 'No', 'Yes' ];

            model = require( './menu-item-types' ).menuItemTypes.schema;
            db.reset( model );

            var menuItemTypes = db
            .select( "mmod_cms_menu_item_types" )
            .execute();

            if( nk.type( menuItemTypes ) === 'array' && menuItemTypes.length > -1 )
            {
                var tmp = [];
                for( var i = 0; i < menuItemTypes.length; i++ )
                {
                    tmp[menuItemTypes[i].id] = menuItemTypes[i].title;
                }

                tmp[0] = tmp[1];

                klay.viewbag.menuItemTypes = tmp;
            }
            else
            {
                console.log( 'Failed to get the menu-item types...' );
                klay.viewbag.menuItemTypes = {};
            }

            model = false;
            db.reset( model );

            var acls = db
            .select( "* from acls" )
            .execute();

            if( nk.type( acls ) === 'array' && acls.length > -1 )
            {
                var tmp = [];
                for( var i = 0; i < acls.length; i++ )
                {
                    tmp[acls[i].id] = acls[i].name;
                }

                tmp[0] = tmp[1];
                klay.viewbag.menuItemACLs = tmp;
            }
            else
            {
                console.log( 'Failed to get the acls...' );
                klay.viewbag.menuItemACLs = {};
            }

            model = false;
            db.reset( model );

            var parents = false;
            parents = db
            .select( "* from mmod_cms_menu_items" )
            .where( { menu: [ '=', menuId ] } )
            .execute();

            if( nk.type( parents ) === 'array' && parents.length > -1  )
            {
                var tmp = [];
                tmp[1] = 'None';
                for( var i = 0; i < parents.length; i++ )
                {
                    tmp[(parents[i].id + 1)] = parents[i].title;
                }

                tmp[0] = tmp[1];

                klay.viewbag.menuItemParents = tmp;
            }
            else
            {
                query_good = false;
                console.log( 'Failed to get a list of eligible parents...' );
                klay.viewbag.menuItemParents = {};
            }

            db.reset( model );

            klay.viewbag.menuId = menuId;

            // And invoke the callback, passing the result of our query
            if( nk.type( callback ) === 'function' )
            { // Asynchronous
                callback( request, response, query_good, klay );
            }
            else
            { // Synchronous
                return query_good;
            }
        },
        addMenuItem: function( request, response, callback, klay )
        {
            var mdata = null;
            if( request.posted.mdata )
            {
                mdata = JSON.parse( request.posted.mdata );
            }

            var menuId = mdata.parentId || false;

            // We need to reference the user with this one.
            var user = klay.viewbag.user;

            // Prep the database object
            var db = this.dbo();

            // Prep the model for the database object
            var model = false;
            db.reset( false );

            // We store the user by Id, not by name
            var whereVals =
            {
                username: [ '=', user.username ],
                email: [ '=', user.email ]
            };

            var userId = db
            .select( "id from users" )
            .where( whereVals )
            .execute();

            // Extract the user's Id
            if( nk.type( userId ) === 'array' )
            {
                userId = userId[0].id;
            }

            model = klay.model.schema;
            delete model.id;
            db.reset( model );

            // Add our menu item now that we have the information to store it correctly
            var values =
            {
                type: mdata.type,
                acl: mdata.acl,
                ts: new Date().toISOString().slice(0, 19).replace('T', ' '),
                luts: new Date().toISOString().slice(0, 19).replace('T', ' '),
                luby: userId,
                title: mdata.title,
                description: mdata.description,
                menu: menuId,
                parent: mdata.parent - 1,
                enabled: mdata.enabled,
                options: JSON.stringify( { content_type: "" } )
            };

            var added = db
            .insert( 'mmod_cms_menu_items' )     // Records contains the number of rows affected.
            .values( values )
            .execute();

            model = klay.model.schema
            db.reset( model );

            if( added )
            {
                if( added > 0 )
                {
                    klay.viewbag.sysmsg = '<span class="xrm-xalert success">Menu Item "' + mdata.title + '" added successfully.</span>';
                }
                else
                {
                    klay.viewbag.sysmsg = '<span class="xrm-xalert error">Menu Item "' + mdata.title + '" not saved.</span>';
                }
            }
            else
            {
                klay.viewbag.sysmsg = '<span class="xrm-xalert error">Whoopsie! Something went wrong; Menu Item "' + mdata.title + '" not saved.</span>';
            }

            // And invoke the callback, passing the result of our query
            if( nk.type( callback ) === 'function' )
            { // Asynchronous
                callback( request, response, klay );
            }
            else
            { // Synchronous
                return klay.viewbag.sysmsg;
            }
        },
        schema:
        {
            /*
             * Menu Item Model
             */
            id: [ true, 'int', ''],
            type: [ true, 'int', 'Type' ],
            acl: [ true, 'int', 'acl' ],
            ts: [ true, 'text', 'Created' ],
            luts: [ true, 'text', 'Last Updated' ],
            luby: [ true, 'text', 'Last Updated by' ],
            title: [ true, 'text', 'Title' ],
            description: [ true, 'text', 'Description' ],
            menu: [ true, 'int', 'Menu' ],
            parent: [ true, 'int', 'Parent' ],
            enabled: [ true, 'int', 'Enabled' ],
            options: [ true, 'text', 'Options' ]
        }
    },
    editView:
    {
        getMenuItem: function( request, response, callback, klay )
        {
            var menuId = false;
            if( Object.prototype.hasOwnProperty.call( request.requrl.query, 'menuId' ) )
            {
                menuId = request.requrl.query.menuId;
            }
            else
            {
                if( Object.prototype.hasOwnProperty.call( request.requrl.query, 'parentId' ) )
                {
                    // User came here from the universal edit mechanism  on the list page
                    menuId = request.requrl.query.parentId;
                }
                else
                {
                    console.log( 'Parent/Record Id not found in query string!' );
                }
            }

            var menuItemId = false;
            if( Object.prototype.hasOwnProperty.call( request.requrl.query, 'menuItemId' ) )
            {
                menuItemId = request.requrl.query.menuItemId;
            }
            else
            {
                if( Object.prototype.hasOwnProperty.call( request.requrl.query, 'recordId' ) )
                {
                    // User came here from the universal edit mechanism  on the list page
                    menuItemId = request.requrl.query.recordId;
                }
                else
                {
                    console.log( 'Menu Item/Record Id not found in query string!' );
                }
            }

            var menuItem = false;
            var menuItemTypes = false;

            if( menuItemId )
            {
                // Prep the database object
                var db = this.dbo();

                // Prepare the model for the database
                var model = klay.model.schema;
                db.reset( model );

                // Get the menu
                menuItem = db
                .select( "mmod_cms_menu_items" )
                .where( { id: [ '=', menuItemId ] } )
                .execute();
            }

            if( nk.type( menuItem ) === 'array' && menuItem.length > -1 )
            {
                klay.viewbag.menuId = menuId || menuitem[0].menu;
                klay.viewbag.menuItem = menuItem[0];
                klay.viewbag.menuItem.id = menuItemId;
            }
            else
            {
                console.log( 'Failed to get the menu-item...' );
                klay.viewbag.menuId = menuId;
                klay.viewbag.menuItem = {};
                klay.viewbag.menuItem.id = menuItemId;
            }

            var enabledTypes = [ 'No', 'No', 'Yes' ];
            klay.viewbag.enabledTypes = [ enabledTypes[klay.viewbag.menuItem.enabled], 'No', 'Yes' ];

            model = require( './menu-item-types' ).menuItemTypes.schema;
            db.reset( model );

            menuItemTypes = db
            .select( "mmod_cms_menu_item_types" )
            .execute();

            if( nk.type( menuItemTypes ) === 'array' && menuItemTypes.length > -1 )
            {
                var tmp = [];
                for( var i = 0; i < menuItemTypes.length; i++ )
                {
                    tmp[menuItemTypes[i].id] = menuItemTypes[i].title;
                }

                tmp[0] = tmp[klay.viewbag.menuItem.type];

                klay.viewbag.menuItemTypes = tmp;
            }
            else
            {
                console.log( 'Failed to get the menu-item types...' );
                klay.viewbag.menuItemTypes = {};
            }

            model = false;
            db.reset( model );

            var acls = db
            .select( "* from acls" )
            .execute();

            if( nk.type( acls ) === 'array' && acls.length > -1 )
            {
                var tmp = [];
                for( var i = 0; i < acls.length; i++ )
                {
                    tmp[acls[i].id] = acls[i].name;
                }

                tmp[0] = tmp[klay.viewbag.menuItem.acl];
                klay.viewbag.menuItemACLs = tmp;
            }
            else
            {
                console.log( 'Failed to get the acls...' );
                klay.viewbag.menuItemACLs = {};
            }

            model = false;
            db.reset( model );

            var parents = false;
            parents = db
            .select( "* from mmod_cms_menu_items" )
            .where( { menu: [ '=', menuId || klay.viewbag.menuItem.menu ] } )
            .execute();

            if( nk.type( parents ) === 'array' )
            {
                var tmp = [];
                tmp[1] = 'None';
                for( var i = 0; i < parents.length; i++ )
                {
                    tmp[(parents[i].id + 1)] = parents[i].title;
                }

                tmp[0] = tmp[(klay.viewbag.menuItem.parent + 1)];

                klay.viewbag.menuItemParents = tmp;
            }
            else
            {
                query_good = false;
                console.log( 'Failed to get a list of eligible parents...' );
                klay.viewbag.menuItemParents = {};
            }

            // And invoke the callback, passing the result of our query
            if( nk.type( callback ) === 'function' )
            { // Asynchronous
                callback( request, response, klay );
            }
            else
            { // Synchronous
                return;
            }
        },
        editMenuItem: function( request, response, callback, klay )
        {
            var mdata = null;
            if( request.posted.mdata )
            {
                mdata = JSON.parse( request.posted.mdata );
            }

            // We need to reference the user with this one.
            var user = klay.viewbag.user;

            // We also want to display the menu edit view after adding this here existing menu
            var menuItem = false;
            var menuItemTypes = false;

            // Prep the database object
            var db = this.dbo();

            // Prep the model for the database object
            var model = false;
            db.reset( model );

            var whereVals =
            {
                username: [ '=', user.username ],
                email: [ '=', user.email ]
            };

            // We store the user by Id, not by name
            var userId = db
            .select( "id from users" )
            .where( whereVals )
            .execute();

            // Extract the user's Id
            if( nk.type( userId ) === 'array' )
            {
                userId = userId[0].id;
            }

            model = klay.model.schema;
            delete model.id;
            delete model.ts;
            delete options;
            db.reset( model );

            whereVals =
            {
                id: [ '=', mdata.id ],
                menu: [ '=', mdata.parentId ]
            };

            // Update our menu-item now that we have the information to store it correctly
            var values =
            {
                type: mdata.type,
                acl: mdata.acl,
                luts: new Date().toISOString().slice( 0, 19 ).replace( 'T', ' ' ),
                luby: userId,
                title: mdata.title,
                description: mdata.description,
                menu: mdata.parentId,
                parent: mdata.parent - 1,
                enabled: mdata.enabled
                //options: mdata.options
            };

            var updated = false;
            updated = db
            .update( 'mmod_cms_menu_items' )
            .values( values )
            .where( whereVals )
            .execute();

            model = require( './menu-items' ).editView.schema;
            db.reset( model );

            if( updated && updated >= 0 )
            {
                klay.viewbag.sysmsg = '<span class="xrm-xalert success">Menu Item "' + mdata.id + '", "' + mdata.title + '", was updated successfully.</span>';

                menuItem = db
                .select( "mmod_cms_menu_items" )
                .where ( { id: [ '=', mdata.id ] } )
                .execute();

                if( nk.type( menuItem ) === 'array' && menuItem.length > -1 )
                {
                    klay.viewbag.menuId = mdata.parentId || menuitem[0].menu;
                    klay.viewbag.menuItem = menuItem[0];
                    klay.viewbag.menuItem.id = mdata.id;
                }
                else
                {
                    console.log( 'Failed to get the menu-item...' );
                    klay.viewbag.menuId = mdata.parentId;
                    klay.viewbag.menuItem = {};
                    klay.viewbag.menuItem.id = mdata.id;
                }
            }
            else
            {
                console.log( 'Something went wrong when updating the menu-item...');
                klay.viewbag.menuItem = {};
                klay.viewbag.sysmsg = '<span class="xrm-xalert error">Menu Item "' + mdata.id + '", "' + mdata.title + '", was not updated successfully.</span>';
            }

            var enabledTypes = [ 'No', 'No', 'Yes' ];
            klay.viewbag.enabledTypes = [ enabledTypes[klay.viewbag.menuItem.enabled], 'No', 'Yes' ];

            model = require( './menu-item-types' ).menuItemTypes.schema;
            db.reset( model );

            menuItemTypes = db
            .select( "mmod_cms_menu_item_types" )
            .execute();

            if( nk.type( menuItemTypes ) === 'array' && menuItemTypes.length > -1 )
            {
                var tmp = [];
                for( var i = 0; i < menuItemTypes.length; i++ )
                {
                    tmp[menuItemTypes[i].id] = menuItemTypes[i].title;
                }

                tmp[0] = tmp[klay.viewbag.menuItem.type];

                klay.viewbag.menuItemTypes = tmp;
            }
            else
            {
                console.log( 'Failed to get the menu-item types...' );
                klay.viewbag.menuItemTypes = {};
            }

            model = false;
            db.reset( model );

            var acls = db
            .select( "* from acls" )
            .execute();

            if( nk.type( acls ) === 'array' && acls.length > -1 )
            {
                var tmp = [];
                for( var i = 0; i < acls.length; i++ )
                {
                    tmp[acls[i].id] = acls[i].name;
                }

                tmp[0] = tmp[klay.viewbag.menuItem.acl];
                klay.viewbag.menuItemACLs = tmp;
            }
            else
            {
                console.log( 'Failed to get the acls...' );
                klay.viewbag.menuItemACLs = {};
            }

            model = false;
            db.reset( model );

            var parents = false;
            parents = db
            .select( "* from mmod_cms_menu_items" )
            .where( { menu: [ '=', mdata.recordId || klay.viewbag.menuItem.menu ] } )
            .execute();

            if( nk.type( parents ) === 'array' )
            {
                var tmp = [];
                tmp[1] = 'None';
                for( var i = 0; i < parents.length; i++ )
                {
                    tmp[(parents[i].id + 1)] = parents[i].title;
                }

                tmp[0] = tmp[(klay.viewbag.menuItem.parent + 1)];

                klay.viewbag.menuItemParents = tmp;
            }
            else
            {
                console.log( 'Failed to get a list of eligible parents...' );
                klay.viewbag.menuItemParents = {};
            }

            model = false;
            db.reset( model );

            // Lastly, we need to set the menu id to redisplay the form properly
            klay.viewbag.menuItem.id = mdata.id;
            if( klay.viewbag.menuItem.id === '[object Object]'  )
            {
                console.log( 'There was an issue setting the menu-item id.')
            }
            klay.viewbag.menuId = mdata.parentId;
            if( klay.viewbag.menuId === '[object Object]'  )
            {
                console.log( 'There was an issue setting the menu id.')
            }

            // And invoke the callback, passing the result of our query
            if( nk.type( callback ) === 'function' )
            { // Asynchronous
                callback( request, response, klay );
            }
            else
            { // Synchronous
                return klay.viewbag.menu;
            }
        },
        schema:
        {
            /*
             * Menu-Item/edit View/Action model
             */
            id: [ true, 'int', ''],
            type: [ true, 'int', 'Type' ],
            acl: [ true, 'int', 'acl' ],
            ts: [ true, 'text', 'Created' ],
            luts: [ true, 'text', 'Last Updated' ],
            luby: [ true, 'text', 'Last Updated by' ],
            title: [ true, 'text', 'Title' ],
            description: [ true, 'text', 'Description' ],
            menu: [ true, 'int', 'Menu' ],
            parent: [ true, 'int', 'Parent' ],
            enabled: [ true, 'int', 'Enabled' ],
            options: [ true, 'text', 'Options' ]
        }
    },
    copyView:
    {
        copyMenuItem: function( request, response, callback, klay )
        {
            var mdata = null;
            if( request.posted.mdata )
            {
                mdata = JSON.parse( request.posted.mdata );
            }

            // We need to reference the user with this one.
            var user = klay.viewbag.user;

            // Prep the database object
            var db = this.dbo();

            // Prep the model for the database object
            var model = false;
            db.reset( model );

            var whereVals =
            {
                username: [ '=', user.username ],
                email: [ '=', user.email ]
            };

            // We store the user by Id, not by name
            var userId = db
            .select( "id from users" )
            .where( whereVals )
            .execute();

            // Extract the user's Id
            if( nk.type( userId ) === 'array' )
            {
                userId = userId[0].id;
            }

            // Prep the model for the database object
            db.reset( model );

            whereVals =
            {
                id: [ '=', mdata.recordId ]
            };

            // We're gonna select first, and make some alterations to what
            // will be inserted back into the table
            var menuItem = db
            .select( '* from mmod_cms_menu_items' )
            .where( whereVals )
            .execute();

            // Now lets do the deed
            var values = {};

            if( nk.type( menuItem ) === 'array' && menuItem.length > 0 )
            {
                values.type = menuItem[0].type;
                values.acl = menuItem[0].acl;
                values.ts = new Date().toISOString().slice( 0, 19 ).replace( 'T', ' ' );
                values.luts = new Date().toISOString().slice( 0, 19 ).replace( 'T', ' ' );
                values.luby = userId;
                values.title = menuItem[0].title + ' (Copy)';
                values.description = menuItem[0].description;
                values.menu = menuItem[0].menu;
                values.parent = menuItem[0].parent;
                values.enabled = menuItem[0].enabled;
                values.options = menuItem[0].options;
            }

            // Prep the model for the database object
            model = klay.model.schema;
            delete model.id;
            db.reset( model );

            var inserted = false;

            inserted = db
            .insert( 'mmod_cms_menu_items' )
            .values( values )
            .execute();

            if( inserted )
            {
                if( inserted > 0 )
                {
                    klay.viewbag.sysmsg = '<span class="xrm-xalert success">Menu Item "' + mdata.recordId + '" copied successfully.</span>';
                }
                else
                {
                    klay.viewbag.sysmsg = '<span class="xrm-xalert error">Menu Item "' + mdata.recordId + '" not found/copied.</span>';
                }
            }
            else
            {
                klay.viewbag.sysmsg = '<span class="xrm-xalert error">Whoopsie! Something went wrong; Menu Item "' + mdata.recordId + '" not found/copied.</span>';
            }

            // And invoke the callback, passing the result of our query
            if( nk.type( callback ) === 'function' )
            { // Asynchronous
                callback( request, response, klay );
            }
            else
            { // Synchronous
                return klay.viewbag.sysmsg;
            }
        },
        schema:
        {
            /*
             * Menu/copy Action model
             */
            id: [ true, 'int', ''],
            type: [ true, 'int', 'Type' ],
            acl: [ true, 'int', 'acl' ],
            ts: [ true, 'text', 'Created' ],
            luts: [ true, 'text', 'Last Updated' ],
            luby: [ true, 'text', 'Last Updated by' ],
            title: [ true, 'text', 'Title' ],
            description: [ true, 'text', 'Description' ],
            menu: [ true, 'int', 'Menu' ],
            parent: [ true, 'int', 'Parent' ],
            enabled: [ true, 'int', 'Enabled' ],
            options: [ true, 'text', 'Options' ]
        }
    },
    deleteView:
    {
        deleteMenuItem: function( request, response, callback, klay )
        {
            var mdata = null;
            if( request.posted.mdata )
            {
                mdata = JSON.parse( request.posted.mdata );
            }

            // Prep the database object
            var db = this.dbo();

            // Prep the model for the database object
            var model = klay.model.schema;
            db.reset( model );

            var whereVals =
            {
                id: [ '=', mdata.id ],
                menu: [ '=', mdata.parentId ]
            };

            var deleted = false;

            deleted = db
            .delete( 'mmod_cms_menu_items' )
            .where( whereVals )
            .execute();

            if( deleted )
            {
                if( deleted > 0 )
                {

                    klay.viewbag.sysmsg = '<span class="xrm-xalert success">Menu Item "' + mdata.id + '" deleted successfully.</span> ';

                    db.reset( model );
                }
                else
                {
                    klay.viewbag.sysmsg = '<span class="xrm-xalert error">Menu Item "' + mdata.id + '" not found/deleted.</span>';
                }
            }
            else
            {
                klay.viewbag.sysmsg = '<span class="xrm-alert error">Whoopsie! Something went wrong; Menu Item "' + mdata.id + '" not found/deleted.</span>';
            }

            // And invoke the callback, passing the result of our query
            if( nk.type( callback ) === 'function' )
            { // Asynchronous
                callback( request, response, klay );
            }
            else
            { // Synchronous
                return klay.viewbag.sysmsg;
            }
        },
        schema:
        {
            /*
             * Menu-Item/delete Action model
             */
            id: [ true, 'int', '']
        }
    }
};


// Export
module.exports = menusItemsModel;