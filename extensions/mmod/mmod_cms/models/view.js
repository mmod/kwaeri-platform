/**
 * package: mmod-cms
 * sub-package: models/view
 * author: Richard B. Winters <a href="mailto:rik@mmogp.com">rik AT mmogp DOT com</a>
 * copyright: 2011-2015 Massively Modified, Inc.
 * license: See Terms and EULA at https://mmogp.com/about/terms
 */


// Deps
var nk = require( 'nk' ),
nk = new nk();  // When we do not pass an argument to the constructor, we
                // get only the core facilities ( .type, .extend, .each,
                // .hash, etc )


/**
 * Defines the view model
 *
 * @since 0.0.1
 */
var viewModel =
{
    view:
    {
        view: function( request, response, callback, klay )
        {
            var viewId = false;
            if( Object.prototype.hasOwnProperty.call( request.requrl.query, 'viewId' ) )
            {
                viewId = request.requrl.query.viewId;
            }
            else
            {
                if( Object.prototype.hasOwnProperty.call( request.requrl.query, 'recordId' ) )
                {
                    // User came here from the universal edit mechanism  on the list page
                    viewId = request.requrl.query.recordId;
                }
                else
                {
                    console.log( 'View View/Record ID not found in query string!' );
                    console.log( request.posted );
                    if( request.posted )
                    {
                        viewId = request.posted.viewId;
                        console.log( 'Posted: ' );
                        console.log( request.posted );
                    }
                    else
                    {
                        var parsed = JSON.parse( klay.viewbag.viewSettings.options );
                        viewId = parsed.article_alias;
                        console.log( 'In options?: ' + viewId );
                    }
                }
            }

            var viewRecord = false;

            // Prep the database object
            var db = this.dbo();

            // Rid the DBO of any stored data.
            var model = klay.model.schema;
            db.reset( model );

            var whereVals =
            {
                alias: [ '=', viewId ],
                type: [ '=', 1 ],
                published: [ '=', 1 ]
            };

            // Get the article record using a prepared statement
            viewRecord = db
            .select( "`mmod-cms-articles`" )
            .where( whereVals )
            .execute();


            console.log( viewRecord );
            console.log( nk.type( viewRecord ) );

            if( nk.type( viewRecord ) === 'array' && viewRecord.length > 0 )
            {
                // Great, we have an array which should have the view item
                // Get the author's name
                db.reset( false );

                var author = db
                .query( "select first, middle, last from users where id='" + viewRecord[0].owner + "';" )
                .execute();

                if( author && !nk.isEmptyObject( author ) )
                {
                    viewRecord[0].author = viewRecord[0].first;

                    if( author[0].middle && author[0].middle != "" )
                    {
                        viewRecord[0].author = viewRecord[0].author + " " + author[0].middle;
                    }

                    viewRecord[0].author = viewRecord[0].author + " " + author[0].last;
                }

                // Format timestamps for proper rendering
                var timestamp = '';
                var mysqlstampparts = viewRecord[0].ts.split( " " );
                var mysqlstamp = mysqlstampparts[0].split( "-" );
                if( nk.type( mysqlstamp ) === 'array' && mysqlstamp.length == 3 )
                {
                    if( mysqlstamp[1].charAt( 0 ) === '0' )
                    {
                        timestamp = mysqlstamp[1].substr( 1 );
                    }
                    else
                    {
                        timestamp = mysqlstamp[1];
                    }

                    timestamp = timestamp + '/';

                    if( mysqlstamp[2].charAt( 0 ) === '0' )
                    {
                        timestamp += mysqlstamp[2].substr( 1 );
                    }
                    else
                    {
                        timestamp += mysqlstamp[2];
                    }

                    timestamp = timestamp + '/';

                    if( mysqlstamp[0].charAt( 0 ) === '0' )
                    {
                        timestamp += mysqlstamp[0].substr( 1 );
                    }
                    else
                    {
                        timestamp += mysqlstamp[0];
                    }
                }

                viewRecord[0].ts = timestamp;

                var lutimestamp = timestamp;
                var mysqllustampparts = viewRecord[0].luts.split( " " );
                var mysqllustamp = mysqllustampparts[0].split( "-" );
                if( nk.type( mysqllustamp ) === 'array' && mysqllustamp.length == 3 )
                {
                    if( mysqllustamp[1].charAt( 0 ) === '0' )
                    {
                        lutimestamp = mysqllustamp[1].substr( 1 );
                    }
                    else
                    {
                        lutimestamp = mysqllustamp[1];
                    }

                    lutimestamp = lutimestamp + '/';

                    if( mysqllustamp[2].charAt( 0 ) === '0' )
                    {
                        lutimestamp += mysqllustamp[2].substr( 1 );
                    }
                    else
                    {
                        lutimestamp += mysqllustamp[2];
                    }

                    lutimestamp = lutimestamp + '/';

                    if( mysqllustamp[0].charAt( 0 ) === '0' )
                    {
                        lutimestamp += mysqllustamp[0].substr( 1 );
                    }
                    else
                    {
                        lutimestamp += mysqllustamp[0];
                    }
                }

                viewRecord[0].luts = lutimestamp;

                // Parse tags for proper rendering
                var tmp = '';
                var tags = JSON.parse( viewRecord[0].tags );
                for( var tag in tags )
                {
                    tmp += '<span class="badge tag">' + tags[tag] + '</span> ';
                }
                viewRecord[0].tags = tmp;

                klay.viewbag.view = viewRecord;
            }
            else
            {
                console.log( 'Failed to get the view record...' );
                klay.viewbag.view = {};
            }

            // And invoke the callback, passing the result of our query
            if( nk.type( callback ) === 'function' )
            { // Asynchronous
                callback( request, response, klay );
            }
            else
            { // Synchronous
                return viewRecord[0];
            }
        },
        schema:
        {
            /*
             * View Model
             */
            id: [ true, 'int', ''],
            type: [ true, 'int', 'Type' ],
            published: [ true, 'int', 'Published' ],
            acl: [ true, 'int', 'ACL' ],
            ts: [ true, 'text', 'Created' ],
            owner: [ true, 'int', 'Owner' ],
            luts: [ true, 'text', 'Last Updated' ],
            luby: [ true, 'text', 'Last Updated by' ],
            category: [ true, 'int', 'Category' ],
            tags: [true, 'text', 'Tags' ],
            title: [ true, 'text', 'Title' ],
            alias: [ true, 'text', 'Alias' ],
            description: [ true, 'text', 'Description' ],
            content: [ true, 'text', 'Article Content' ],
            keywords: [ true, 'text', 'Keywords' ],
            options: [ true, 'text', 'Options' ]
        }
    }
};


// Export
module.exports = viewModel;