/**
 * package: mmod-cms
 * sub-package: controllers/utility
 * author: Richard B. Winters <a href="mailto:rik@mmogp.com">rik AT mmogp DOT com</a>
 * copyright: 2011-2015 Massively Modified, Inc.
 * license: See Terms and EULA at https://mmogp.com/about/terms
 */


/**
 * Constructor
 *
 * @since 0.0.1
 */
function utilityController()
{
}


/**
 * Method for rendering module content
 *
 * @param request
 * @param response
 *
 * @return string The module content
 *
 * @since 0.0.1
 */
utilityController.prototype.renderModuleContent = function( request, response )
{
    // Prep the viewbag
    this.klay.viewbag.utilitySettings = JSON.parse( this.moduleParams );

    // Here we're going to want to use our database provider, when we do so
    // we'll define a callback to send along with our query
    // to support an implicit asynchronicity.
    if( this.klay.viewbag.hasOwnProperty( 'utilitySettings' ) )
    {
        if( this.klay.viewbag.utilitySettings.hasOwnProperty( 'utility_options' ) )
        {
            var utiltype = this.klay.viewbag.utilitySettings.utility_options.type;

            switch( utiltype )
            {
                case 1:
                {
                    //this.klay.controller = "lytebox";
                    this.klay.view = "lytebox";
                }break;

                case 2:
                {
                    //this.klay.controller = "kwaerichat";
                    this.klay.view = "kwaerichat";
                }break;
            }
            console.log( 'after the utility switch...' );
            console.log( this.klay.view );
            console.log( utiltype );
        }
        else
        {
            console.log( 'utility_options not found...' );
            //this.klay.view = "utility";
            this.klay.viewbag.utility = [ "" ];
        }
    }
    else
    {
        console.log( 'utilitySettings not found in the viewbag...' );
        //this.klay.view = "utility";
        this.klay.viewbag.utility = [ "" ];
    }

    this.rendr( request, response );
};


// Export
module.exports = exports = utilityController;
