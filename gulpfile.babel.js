/*-----------------------------------------------------------------------------
 * @package:    Kwaeri platform
 * @author:     Richard B Winters
 * @copyright:  2015-2018 Massively Modified, Inc.
 * @license:    Apache-2.0
 * @version:    0.1.0
 *---------------------------------------------------------------------------*/


 // Absolutely declare this always.
 'use strict';


// INCLUDES
import gulp from 'gulp';                            // Obviously :)
//import gts from 'gulp-typescript';                  // For compiling typescript, requires the typescript module also.
import sass from 'gulp-sass';                       // Again, obviously!
import sassGlob from 'gulp-sass-glob';              // For special globbing support?
import watch from 'gulp-watch';                     // For Auto-Compilation/Transpilation upon save
import sourcemaps from 'gulp-sourcemaps';           // For generating JavaScript source maps
import webpack from 'webpack';                      // For bundling... ; ;
import webpackStream from 'webpack-stream';         // So gulp can manipulate the webpack bundling process
import uglifyes from 'uglify-es';                   // To minify es6+ JavaScript syntax
import composer from 'gulp-uglify/composer';        // Allows gulp-uglify to use an alternate uglify module (default is uglify-js)
import plumber from 'gulp-plumber';                 // For helping gulp manipulate webpack
import cssnano from 'gulp-cssnano';                 // To minify CSS
import rename from 'gulp-rename';                   // For adding suffixes/renaming files (we use it to name minified js files)
import imagemin from 'gulp-imagemin';             // For compressing images and media items
import cache from 'gulp-cache';                   // For caching compressed imagesS
import del from 'del';                              // Used by our clean process, for the most part -
import bump from 'gulp-bump-version';               // Used for version increment automation
import runsequence from 'run-sequence';             // For running tasks sequentially


// GLOBALS
let minify = composer( uglifyes, console );         // Replacing the uglify processor with one which supports es6+
//let gtsProject = gts.createProject( 'tsconfig.json' );  // A preliminary for transpiling typescript using microsoft's compiler

const args =                                        // Allows us to accept arguments to our gulpfile
(
    argList =>
    {
        let args = {}, i, option, thisOption, currentOption;

        for( i = 0; i < argList.length; i++ )
        {
            thisOption = argList[i].trim();

            option = thisOption.replace( /^\-+/, '' );

            if( option === thisOption )
            {
                // argument value
                if( currentOption )
                {
                    args[currentOption] = option;
                }

                currentOption = null;
            }
            else
            {
                currentOption = option;
                args[currentOption] = true;
            }
        }

        return args;
    }
)( process.argv );


// Check if we supplied arguments for updating the version of the project and its files
let bumpVersion         = args['bump-version'],
    toVersion           = args['version'],
    byType              = args['type'],
    bumpOptions         = ( bumpVersion ) ? { } : { type: 'patch' },
    projectBumpOptions  = ( bumpVersion ) ? { } : { type: 'patch', key: '"version": "' };

// If version was provided, overwrite the options accordingly
if( toVersion )
{
    bumpOptions = { version: toVersion };
    projectBumpOptions = bumpOptions;
    projectBumpOptions.key = '"version": "';
}

// If type was provided, overwrite the options accordingly
if( byType )
{
    bumpOptions = { type: byType };
    projectBumpOptions = bumpOptions;
    projectBumpOptions.key = '"version": "';
}


// Bump the version in our projects files (called manually, or through the build-release task)
gulp.task
(
    'bump-file-versions',
    () =>
    {
        console.log( ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' ) + 'Modifying version strings in project files...' );

        // Otherwise, by providing --bump-version only, you will increment the patch revision

        // Start with all the files using the typical key, then hit out package.json: 'dist/src/**/*.js',
        return gulp.src
        (
            [
                '**/*.ts',
                '**/*.js',
                '**/*.scss',
                '!dist/js/kwaeri-ux{,/**}',
                '!node_modules{,/**}'
            ],
            { base: './' }
        )
        .pipe( bump( bumpOptions ) )
        .pipe( gulp.dest( '' ) );
    }
);


// Bump the version in our in our package.json file
gulp.task
(
    'bump-project-version',
    () =>
    {
        console.log( ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' ) + 'Modifying version string in package.json...' );

        // Target out package.json:
        return gulp.src
        (
            'package.json',
            { base: './' }
        )
        .pipe( bump( projectBumpOptions ) )
        .pipe( gulp.dest( '' ) );
    }
);


/* Compile Typescript files
gulp.task
(
    'compile-typescript',
    () =>
    {
        console.log( ' ', ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' ) + 'Compiling Typescript sources into JavaScript...', ' ' );

        return gtsProject.src() // This line pulls the 'src' from the tsconfig.json file
        .pipe( gtsProject() )
        .js.pipe( gulp.dest( 'dist/js/src' ) );
    }
);


// Compile Typescript files
gulp.task
(
    'generate-typescript-declarations',
    () =>
    {
        console.log( ' ', ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' ) + 'Generating Typescript declarations...', ' ' );

        return gtsProject.src() // This line pulls the 'src' from the tsconfig.json file
        .pipe( gtsProject() )
        .dts.pipe( gulp.dest( 'dist/js/src' ) );
    }
);
*/

// Compiles admin SCSS files
gulp.task
(
    'compile-admin-scss',
    () =>
    {
        console.log( ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' )  + 'Compiling admin SCSS source fies for ' + ( ( args.dev ) ? 'development' : 'production' ) + '...' );

        if( args.dev )
        {
            return gulp.src( 'admin/assets/mmod/scss/kp-admin.scss' )
            .pipe(plumber())
            .pipe( sourcemaps.init() )
            .pipe( sassGlob() )
            .pipe
            (
                sass( { outputStyle: 'compressed', includePaths: ['node_modules'] } ).on( 'error', sass.logError )
            )
            .pipe( sourcemaps.write( '.' ) )  // Remove '.' for inline scss sourcemaps
            .pipe( gulp.dest( 'admin/assets/mmod/public/css' ) );
        }

        return gulp.src( 'admin/assets/mmod/scss/kp-admin.scss' )
        .pipe(plumber())
        .pipe( sassGlob() )
        .pipe
        (
            sass( { outputStyle: 'compressed', includePaths: ['node_modules'] } ).on( 'error', sass.logError )
        )
        .pipe( gulp.dest( 'admin/assets/mmod/public/css' ) );
    }
);

// Compiles Client SCSS files
gulp.task
(
    'compile-client-scss',
    () =>
    {
        console.log( ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' )  + 'Compiling client SCSS source fies for ' + ( ( args.dev ) ? 'development' : 'production' ) + '...' );

        if( args.dev )
        {
            return gulp.src( 'assets/mmod/scss/kp-client.scss' )
            .pipe(plumber())
            .pipe( sourcemaps.init() )
            .pipe( sassGlob() )
            .pipe
            (
                sass( { outputStyle: 'compressed', includePaths: ['node_modules'] } ).on( 'error', sass.logError )
            )
            .pipe( sourcemaps.write( '.' ) )  // Remove '.' for inline scss sourcemaps
            .pipe( gulp.dest( 'assets/mmod/public/css' ) );
        }

        return gulp.src( 'assets/mmod/scss/kp-client.scss' )
        .pipe(plumber())
        .pipe( sassGlob() )
        .pipe
        (
            sass( { outputStyle: 'compressed', includePaths: ['node_modules'] } ).on( 'error', sass.logError )
        )
        .pipe( gulp.dest( 'assets/mmod/public/css' ) );
    }
);


// Watches for changes in TS & SCSS files
gulp.task
(
    'watch',
    () =>
    {
        console.log( ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' )  + 'Watching for changes to SCSS source...' );

        gulp.watch( 'src/**/*.ts', ['compile-typescript'] );
        gulp.watch( 'scss/**/*.scss', ['compile-sass'] );       // We'll watch all scss files for changes, although we only build
                                                                // with the main scss file as a source target.
    }
);


// Bundle JS files with Webpack for use in a browser (admin)
gulp.task
(
    'transpile-admin-library',
    () =>
    {
        console.log( ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' ) + 'Transpiling and Bundling Admin MDC & Kwaeri JavaScript using Webpack...' );

        gulp.src
        (
            'admin/assets/mmod/js/library.js'
        )
        .pipe( plumber() )  // Prevent stop on error
        .pipe( webpackStream( ( ( args.dev ) ? require( './webpack/webpack.dev.config' ) : require( './webpack/webpack.prod.config' ) ) ), webpack )
        .pipe( gulp.dest( 'admin/assets/mmod/public/js/kwaeri-platform' ) );
    }
);


// Bundle JS files with Webpack for use in a browser (client)
gulp.task
(
    'transpile-client-library',
    () =>
    {
        console.log( ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' ) + 'Transpiling and Bundling Client MDC & Kwaeri JavaScript using Webpack...' );

        gulp.src
        (
            [ 'assets/mmod/js/library.js' ]
        )
        .pipe( plumber() )  // Prevent stop on error
        .pipe( webpackStream( ( ( args.dev ) ? require( './webpack/webpack.dev.config' ) : require( './webpack/webpack.prod.config' ) ) ), webpack )
        .pipe( gulp.dest( 'assets/mmod/public/js/kwaeri-platform' ) );
    }
);


// Optimzes admin JS files
gulp.task
(
    'optimize-admin-js',
    () =>
    {
        console.log( ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' ) + 'Minifying admin JavaScript source files...' );

        //return gulp.src( [ 'dist/js/src/kwdt.js', 'dist/js/test/test.js' ] )
        return gulp.src( [ 'admin/assets/mmod/public/js/**/*.js', '!admin/assets/mmod/public/js/kwaeri-platform/*.js' ] )
        .pipe( sourcemaps.init() )
        .pipe( minify() )
        .on('error', function (err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
        .pipe( rename( { suffix: '.min' } ) )
        .pipe( sourcemaps.write( '.', { includeContent: false, sourceRoot: '' } ) )
        .pipe( gulp.dest( 'admin/assets/mmod/public/js' ) );
    }
);


// Copy precompiled javascript to admin public assets directory
gulp.task
(
    'publish-precompiled-admin-js',
    () =>
    {
        console.log( ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' ) + 'Moving precompiled admin JavaScript sources to the public directory...' );

        return gulp.src( [ 'admin/assets/mmod/js/*.min.js', 'admin/assets/mmod/js/prism.js' ] )
        .pipe( gulp.dest( 'admin/assets/mmod/public/js' ) );
    }
);


// Copy precompiled javascript to public assets directory
gulp.task
(
    'publish-precompiled-client-js',
    () =>
    {
        console.log( ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' ) + 'Moving precompiled client JavaScript sources to the public directory...' );

        return gulp.src( [ 'assets/mmod/js/*.min.js', 'assets/mmod/js/prism.js' ] )
        .pipe( gulp.dest( 'assets/mmod/public/js' ) );
    }
);


// Optimzes client JS files
gulp.task
(
    'optimize-client-js',
    () =>
    {
        console.log( ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' ) + 'Minifying client JavaScript source files...' );

        //return gulp.src( [ 'dist/js/src/kwdt.js', 'dist/js/test/test.js' ] )
        return gulp.src( [ 'assets/mmod/public/js/**/*.js', '!assets/mmod/public/js/kwaeri-platform/*.js' ] )
        .pipe( sourcemaps.init() )
        .pipe( minify() )
        .on('error', function (err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
        .pipe( rename( { suffix: '.min' } ) )
        .pipe( sourcemaps.write( '.', { includeContent: false, sourceRoot: '' } ) )
        .pipe( gulp.dest( 'assets/mmod/public/js' ) );
    }
);


// Optimzes admin CSS files
gulp.task
(
    'optimize-admin-css',
    () =>
    {
        console.log( ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' ) + 'Minifying admin CSS source files...' );

        if( args.dev )
        {
            return gulp.src( 'admin/assets/mmod/public/css/*.css' )
            .pipe( sourcemaps.init() )
            .pipe( cssnano() )
            .pipe( sourcemaps.write( '.' ) )
            .pipe( rename( { suffix: '.min' } ) )
            .pipe( gulp.dest( 'admin/assets/mmod/public/css' ) );
        }

        return gulp.src( 'admin/assets/mmod/public/css/*.css' )
        .pipe( cssnano() )
        .pipe( rename( { suffix: '.min' } ) )
        .pipe( gulp.dest( 'admin/assets/mmod/public/css' ) );
    }
);


// Optimzes client CSS files
gulp.task
(
    'optimize-client-css',
    () =>
    {
        console.log( ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' ) + 'Minifying client CSS source files...' );

        if( args.dev )
        {
            return gulp.src( 'assets/mmod/public/css/*.css' )
            .pipe( sourcemaps.init() )
            .pipe( cssnano() )
            .pipe( sourcemaps.write( '.' ) )
            .pipe( rename( { suffix: '.min' } ) )
            .pipe( gulp.dest( 'assets/mmod/public/css' ) );
        }

        return gulp.src( 'assets/mmod/public/css/*.css' )
        .pipe( cssnano() )
        .pipe( rename( { suffix: '.min' } ) )
        .pipe( gulp.dest( 'assets/mmod/public/css' ) );
    }
);


// Optimize admin image files
gulp.task
(
    'optimize-admin-images',
    () =>
    {
        console.log( ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' ) + 'Minifying admin image files...' );

        return gulp.src( 'admin/assets/mmod/images/**/*.+(png|jpg|jpeg|gif|svg)' )
        .pipe
        (
            cache
            (
                imagemin
                (
                    {
                        // Setting interlaced to true
                        interlaced: true
                    }
                )
            )
        )
        .pipe( gulp.dest( 'admin/assets/mmod/public/images' ) )
    }
);


// Optimize client image files
gulp.task
(
    'optimize-client-images',
    () =>
    {
        console.log( ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' ) + 'Minifying client image files...' );

        return gulp.src( 'assets/mmod/images/**/*.+(png|jpg|jpeg|gif|svg)' )
        .pipe
        (
            cache
            (
                imagemin
                (
                    {
                        // Setting interlaced to true
                        interlaced: true
                    }
                )
            )
        )
        .pipe( gulp.dest( 'assets/mmod/public/images' ) )
    }
);



// Copy fonts to admin public assets directory
gulp.task
(
    'publish-admin-fonts',
    () =>
    {
        console.log( ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' ) + 'Importing kwaeri user experience fonts to the admin application...' );

        return gulp.src( [ 'node_modules/@kwaeri/user-experience/fonts/**/*.woff2' ] )
        .pipe( gulp.dest( 'admin/assets/mmod/public/fonts' ) );
    }
);



// Copy fonts to client public assets directory
gulp.task
(
    'publish-client-fonts',
    () =>
    {
        console.log( ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' ) + 'Importing kwaeri user experience fonts to the client application...' );

        return gulp.src( [ 'node_modules/@kwaeri/user-experience/fonts/**/*.woff2' ] )
        .pipe( gulp.dest( 'assets/mmod/public/fonts' ) );
    }
);


// Cleans the admin public assets directory (removes it).
gulp.task
(
    'clean:admin-assets',
    () =>
    {
        console.log( ' ', ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' )  + 'Cleaning public assets directory (admin)...', ' ' );

        let deleteList = ( !args.alt ) ? 'admin/assets/mmod/public' : 'admin/assets/mmod/public';
        return del.sync
        (
            deleteList,
            { force: true }
        );
    }
);


// Cleans the client public  directory (removes it).
gulp.task
(
    'clean:client-assets',
    () =>
    {
        console.log( ' ', ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' )  + 'Cleaning public assets directory (client)...', ' ' );

        let deleteList = ( !args.alt ) ? 'assets/mmod/public' : 'assets/mmod/public';
        return del.sync
        (
            deleteList,
            { force: true }
        );
    }
);


// Clears image cache(s).
gulp.task
(
    'clear:image-cache',
     callback =>
    {
        console.log( ' ', ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' )  + 'Clearing optimized image cache...', ' ' );

        return cache.clearAll( callback );
    }
);


// Clears admin and client assets.
gulp.task
(
    'clean:build',
     callback =>
    {
        runsequence
        (
            'clean:admin-assets',
            'clean:client-assets',
            callback
        );
    }
);


// Clears admin and client assets. as well as clears the cache
gulp.task
(
    'clean:all',
     callback =>
    {
        runsequence
        (
            'clean:admin-assets',
            'clean:client-assets',
            'clear:image-cache',
            callback
        );
    }
);


// Compiles & optimizes the admin scss source files
gulp.task
(
    'build-admin-styles',
    callback =>
    {
        runsequence
        (
            'compile-admin-scss',
            'optimize-admin-css',
            callback
        );
    }
);


// Compiles & optimizes the client scss source files
gulp.task
(
    'build-client-styles',
    callback =>
    {
        runsequence
        (
            'compile-client-scss',
            'optimize-client-css',
            callback
        );
    }
);


// Cleans the dist directory (removes it).
gulp.task
(
    'build-release',
    callback =>
    {
        runsequence
        (
            'clean:admin-assets',
            'clean:client-assets',
            'bump-project-version',
            'bump-file-versions',
            'publish-admin-fonts',
            'publish-client-fonts',
            'compile-admin-scss',
            'compile-client-scss',
            'optimize-admin-css',
            'optimize-client-css',
            //'compile-typescript',
            //'generate-typescript-declarations',
            'transpile-admin-library',
            'transpile-client-library',
            'optimize-admin-js',
            'optimize-client-js',
            callback
        );
    }
);


// Cleans the dist directory (removes it).
gulp.task
(
    'prep-admin',
    callback =>
    {
        runsequence
        (
            'clean:admin-assets',
            'compile-admin-scss',
            'optimize-admin-css',
            //'compile-typescript',
            //'generate-typescript-declarations',
            'transpile-admin-library',
            'optimize-admin-js',
            'publish-precompiled-admin-js',
            'optimize-admin-images',
            'publish-admin-fonts',
            callback
        );
    }
);


// Cleans the dist directory (removes it).
gulp.task
(
    'prep-client',
    callback =>
    {
        runsequence
        (
            'clean:client-assets',
            'compile-client-scss',
            'optimize-client-css',
            //'compile-typescript',
            //'generate-typescript-declarations',
            'transpile-client-library',
            'optimize-client-js',
            'publish-precompiled-client-js',
            'optimize-client-images',
            'publish-client-fonts',
            callback
        );
    }
);


// Cleans the dist directory (removes it).
gulp.task
(
    'default',
    callback =>
    {
        runsequence
        (
            'clean:admin-assets',
            'clean:client-assets',
            'compile-admin-scss',
            'compile-client-scss',
            'optimize-admin-css',
            'optimize-client-css',
            //'compile-typescript',
            //'generate-typescript-declarations',
            'transpile-admin-library',
            'transpile-client-library',
            'optimize-admin-js',
            'optimize-client-js',
            'publish-precompiled-admin-js',
            'publish-precompiled-client-js',
            'publish-admin-fonts',
            'publish-client-fonts',
            callback
        );
    }
);


