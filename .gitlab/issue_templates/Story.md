>>>
 A story is used to elaborately explain as many details as possible about a particular feature or facility that is to be implemented in a product. Ensure that you get into the nitty-gritty, as sub-tasks will be created based on the stories details, and the more details you have the more easily you - or others - will be able to ensure that all required sub-tasks will be created (and subsequently, that all pertinent features are implemented). For example (be sure to remove blockquotes):
>>>

## Synopsis

As a [developer|user], I expect that [...]

## Details

The following will be required in order to fully implement [...]:

[...task/bullet list here...]


**Check list:**

Ensure you check these off as you approach the submission of the issue:

- [ ] I [*was|was not*] possible to attach a mock-up or a link to a video with an example, etc.
- [ ] I *have* labeled the issue properly as a `story`.
- [ ] I *have* deleted the template instructions at the top of this issue.
- [ ] I *have* deleted the example at the bottom of this issue.




*DELETE THIS NOTE AND THE BELOW STORY EXPLANAITON AND EXAMPLE (INCLUDING THE HEADINGS) BEFORE SUBMITTING YOUR ISSUE!*

> Once you have properly filled out the story's details, ensure to appropriately assign it to a milestone, label it, assign it to yourself or leave the assignee open - and if sub-tasks will be required, ensure you immediately begin adding them and linking them appropriately.


>>>
## Stories Explained

A story is used to elaborately explain as many details as possible about a particular feature or facility that is to be implemented in a product. Ensure that you get into the nitty-gritty, as sub-tasks will be created based on the stories details, and the more details you have the more easily you - or others - will be able to ensure that all required sub-tasks will be created (and subsequently, that all pertinent features are implemented). For example (be sure to remove blockquotes):

## Example

As a developer, I expect that kwaeri-platform would include an extension system for extending the functionality of the platform. There should be a new menu item in the administrative application which loads a default page for managing extensions. Included in the default management page should be options to install new extensions, remove existing extensions, or to click into an existing extension and make changes to a particular extensions configuration.

As extension systems tend to provide not only mangement of particular data within an administrative application, but also modules for displaying such related data on a front-facing application, I expect that a module system will also designed and implemented as a sub-task of this story.

Subsequently, the existing layout system should be upgraded to a full-fledged theme system which supports modules and which provides module-positions. This I expect will also be a sub-task (or set of) for this story.

The following will be required in order to fully implement an extension system into the kwaeri-platform application:

* A controller which will act as a base route `/extensions` for the implementation.
  * The controller should act as an extended base controller, providing everything needed to an extensions controller.
  * The controller needs to possess the appropriate properties for implementing the management interfaces as well
* A database table for storing data relevant to extensions
  * Names, paths, enabled/disabled flags
* Database tables for allowing extension provided modules
  * There should be a table for module-types. This table will record the name, enabled/disabled flag, as well as tie it to the extension which provides it
  * There should be a table for modules. This table will record all the data relevant to the module, including params that the front-facing module implementation requires for its proper rendering/functionality. Other pertinent columns would include positions, enabled/disabled, name, type/provider for querying proper rendering routines from the appropriate provider, etc.
* Modifications to the layout system to enact a full-fledged theme system.
  * The theme system should incorporate positions
* Modifications to the nk framework for supporting extensions.
  * Update logic where required so that the theme system properly incorporates modules, their positions, and that the renderer is capable of incorporating all of the pertinent pieces to do its job properly.
>>>