>>>
When naming your feature issue, ensure that you are as clear as possible while using as few words as possible. Leverage the description for the more encompassing bits...

Some examples:

* The title: Social sharing
  * The details: As an end user, when I am writing blogs in the CMS extension, I would like to be able to configure automatically posting to my linked social accounts such as Facebook or Twitter..
* The title: Platform migrations via kpm
  * The details: As a developer, when I am working on the kwaeri-platform project, I would like to be able to easily apply database updates to my local installation using the kwaeri-platform manager (kpm)
>>>

## Feature Details

As an [*end user|developer*] I would like [*to be able to...*]


**Check list:**

Ensure you check these off as you approach the submission of the issue:

- [ ] I [*was|was not*] possible to attach a mock-up or a link to a video with an example of such a feature, etc.
- [ ] I *have* labeled the feature properly as a `feature`.
- [ ] I *have* deleted the template instructions at the top of this issue.


#### NOTICE

*DO NOT assign the issue to anyone, nor set a priority or milestone. These actions are for team members only.*

*DO NOT make the feature  confidential. It defeats the purpose as we expect to avoid duplicate feature issues or request for features already being implemented.*