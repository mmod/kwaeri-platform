>>>
When naming your issue, ensure that you are as clear as possible while using as few words as possible. Leverage the description for the more encompassing bits...

Some examples:

* The title: Navigation from extensions view results in a whoopsie
  * The details: When clicking on a link within the extensions management page, the entire browser is redirected to the /extensions/manage subdomain, rather than properly navigating via AJAX..
* The title: Attempting admin login yields a login request loop
  * The details: When attempting to login to the administrative site - whether upon visiting the application, or after a session timeout -  a login request loop ensues until such time as the front-facing application is refreshed
>>>

## Issue Details

* [...]


**Check list:**

Ensure you check these off as you approach the submission of the issue:

- [ ] I [*was|was not*] possible to attach a mock-up or a link to a video with an example of such an issue, etc.
- [ ] I *have* labeled the issue properly as a `issue`.
- [ ] I *have* deleted the template instructions at the top of this issue.


#### NOTICE

*DO NOT assign the issue to anyone, nor set a priority or milestone. These actions are for team members only.*

*DO NOT make the issue confidential. It defeats the purpose as we expect to duplicate issue reports.*