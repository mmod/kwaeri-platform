> A resolution merge request is one that is specifically made in order to contribute to or resolve an issue. Ensure that you link to the issue that the merge request contributes to or resolves, and if any developer on the team should be notified or is responsible (assigned to the task) then make sure you @mention them For example.

This merge request [*resolves or contributes*] to [*issue|story|bug|feature request or enhancement*] #[XX] [*name and desc of issue*]

Here is a summary of what has been done:

* Fixed [*xxx*]
* Added [*xxx*]
* Removed [*xxx*]
* Updated [*xxx*]

[*do an @mention here of any user assigned or who should be notified of task completion*]