/**
 * @package:   mmod-xrm
 * @author:    Richard B. Winters <a href='mailto:rik@mmogp.com'>Rik At MMOGP</a>
 * @copyright: 2011-2015 Massively Modified, Inc.
 * @license:   Apache, Version 2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 */


// Deps
var nk = require( "nk" ),
    config = require( "./config" );

var app = new nk( config );


// Start our App
app.init( { xrm: true } );